# ![logo](src/main/webapp/img/web/meadme_logo.png)
## Z平台介绍
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Z平台是开源免费的Java Web快速开发平台。通过Z平台集成开发环境，以低代码、动态配置的方式能够快速开发出各类Web管理系统。Z平台框架组成所用到的各种功能组件与框架，都是开源免费的，不涉及到版权问题，商业与非商业项目都可以使用。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**官方网站：**<http://www.zframeworks.com/>    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**演示平台：**<http://www.zframeworks.com/u_s_l>        

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**适合开发的项目：**    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.各类企业ERP系统    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.APP后端接口服务    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.各类任务处理平台    

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Z平台的优势：**     
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;承诺Z平台永久开源免费

## 下载中心
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Z平台-集成版:** 请到Z平台交流QQ群【685040231】,群共享文件夹中下载。       

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Z平台-源代码:** [Gitee仓库](https://gitee.com/zj1983/zz)              

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**文件存储组件:** 请到Z平台交流QQ群【685040231】,群共享文件夹中下载。          



## 搭建环境服务
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如果您想使用Z平台建设项目，但对开发或运行环境搭建不太擅长，您可以加我微信与我联系，购买服务，我将通过QQ远程协助的方式，辅助您快速完成开发与运行环境搭建工作。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**服务费：** 100元/次


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**我的微信：**   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![wx](src/main/webapp/img/web/wxewm.jpg)


## 版本说明
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Z平台-集成版:**  该版本包含Z平台最新版程序、MySQL、Tomcat、JDK等所有运行环境应用到的组件。并集成了控制台小程序。方便对于Java环境配置过程不熟的同学，能够快速使用与体验Z平台。减少在环境配置方面浪费时间。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Z平台-源代码:**  该版本只包含未编译的Z平台源代码与数据库初始化文件。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**文件存储组件:**  与Z平台配套的文件管理服务组件，管理所有上传到Z平台中的文件，与Z平台Web工程的文件分别管理。



## 使用教程
[1.基本概念介绍](https://gitee.com/zj1983/zz/blob/master/docs/1.%E5%9F%BA%E6%9C%AC%E6%A6%82%E5%BF%B5%E4%BB%8B%E7%BB%8D.doc)   
[2.开发环境搭建](https://gitee.com/zj1983/zz/blob/master/docs/2.%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA.doc)   
[3.Z平台集成版使用教程](https://gitee.com/zj1983/zz/blob/master/docs/3.Z%E5%B9%B3%E5%8F%B0%E9%9B%86%E6%88%90%E7%89%88%E4%BD%BF%E7%94%A8%E6%95%99%E7%A8%8B.doc)   
[4表单开发](https://gitee.com/zj1983/zz/blob/master/docs/4%E8%A1%A8%E5%8D%95%E5%BC%80%E5%8F%91.docx)   
[5.报表开发](https://gitee.com/zj1983/zz/blob/master/docs/5.%E6%8A%A5%E8%A1%A8%E5%BC%80%E5%8F%91.doc)   
[6.接口开发](https://gitee.com/zj1983/zz/blob/master/docs/6.%E6%8E%A5%E5%8F%A3%E5%BC%80%E5%8F%91.docx)   
[7.自动任务开发](https://gitee.com/zj1983/zz/blob/master/docs/7.%E8%87%AA%E5%8A%A8%E4%BB%BB%E5%8A%A1%E5%BC%80%E5%8F%91.docx)   
[8.工作流开发](https://gitee.com/zj1983/zz/blob/master/docs/8.%E5%B7%A5%E4%BD%9C%E6%B5%81%E5%BC%80%E5%8F%91.docx)   
[9.【报表】【表单】权限分配](https://blog.csdn.net/qq_38056435/article/details/101268762)   
[10.平台运行流程介绍](https://blog.csdn.net/qq_38056435/article/details/101432208)   
[11.源码包结构介绍](https://blog.csdn.net/qq_38056435/article/details/101433536)   
[12.核心类介绍](https://blog.csdn.net/qq_38056435/article/details/101435324)   
[13.常用JAVA工具类介绍](https://blog.csdn.net/qq_38056435/article/details/101436311)       
[14.调整表单中字段控件的布局](https://blog.csdn.net/qq_38056435/article/details/122918861)        
[15.表单增加自定义CSS样式](https://blog.csdn.net/qq_38056435/article/details/122920077)        
## 技巧文章
[Z平台充当数据交换平台应用方案](https://blog.csdn.net/qq_38056435/article/details/109765619)   
[通过Z平台读取其它数据库的方法](https://blog.csdn.net/qq_38056435/article/details/106210900)   
[Z平台-移动版](https://blog.csdn.net/qq_38056435/article/details/106070264)   
[通过Excel模板导入数据到表单](https://blog.csdn.net/qq_38056435/article/details/105656851)   
[Z平台启动错误（The server time zone value ‘?й???????‘ is unrecognized or represents ....）](https://blog.csdn.net/qq_38056435/article/details/104946035)   
[使用低版本MySQL数据库执行Z平台数据初始化](https://blog.csdn.net/qq_38056435/article/details/104529769)   
[Tomcat运行环境中直接开发自定义程序接口](https://blog.csdn.net/qq_38056435/article/details/104302025)   
[快速重写标准按钮处理方法](https://blog.csdn.net/qq_38056435/article/details/103957897)   
[多数据库连接配置](https://blog.csdn.net/qq_38056435/article/details/103591383)   
[改变表单列表页面显示样式为“卡片组”](https://blog.csdn.net/qq_38056435/article/details/103491053)   
[常用前端JS代码与JS方法](https://blog.csdn.net/qq_38056435/article/details/103236659)   
[安装与部署【文件存储服务】](https://blog.csdn.net/qq_38056435/article/details/103117399)   
[如何获取表记录的主键](https://blog.csdn.net/qq_38056435/article/details/103049156)   
[总账功能与明细账功能使用讲解](https://blog.csdn.net/qq_38056435/article/details/103047469)   
[设置表单数据隔离模式](https://blog.csdn.net/qq_38056435/article/details/102817131)   
[发送手机验证码接口调用](https://blog.csdn.net/qq_38056435/article/details/102776854)   
[重写表单默认保存方法教程](https://blog.csdn.net/qq_38056435/article/details/102736737)   
[自定义功能按钮开发](https://blog.csdn.net/qq_38056435/article/details/102714981)   
[根据关键词自动抓取百度搜索记录并发送到指定邮箱](https://blog.csdn.net/qq_38056435/article/details/102623519)   
[设置表单常用查询条件，方便日常查询工作](https://blog.csdn.net/qq_38056435/article/details/102372977)
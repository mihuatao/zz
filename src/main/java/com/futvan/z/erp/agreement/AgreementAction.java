package com.futvan.z.erp.agreement;
import com.futvan.z.erp.cms_info.cms_info;
import com.futvan.z.framework.core.z;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.erp.agreement.AgreementService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AgreementAction extends SuperAction{
	@Autowired
	private AgreementService agreementService;


	/**
	 * 	打开指定协议
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/openAgreement")
	public ModelAndView open_cms(String number) throws Exception {
		ModelAndView  model = new ModelAndView("erp/agreement/agreement_info");
		z_agreement agreement = new z_agreement();
		if(z.isNotNull(number)) {
			z_agreement aq = new z_agreement();
			aq.setNumber(number);
			List<z_agreement> list = sqlSession.selectList("z_agreement_select",aq);
			if(z.isNotNull(list) && list.size()>0){
				agreement = list.get(0);
			}else{
				agreement.setInfo("根据["+number+"]未获取到协议内容");
			}
		}else {
			agreement.setInfo("参数：number不能为空");
		}
		model.addObject("agreement", agreement);
		return model;
	}
}

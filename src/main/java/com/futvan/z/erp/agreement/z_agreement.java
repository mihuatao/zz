package com.futvan.z.erp.agreement;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_agreement extends SuperBean{
	//协议文本
	private String info;

	//文档
	private String file_url;

	/**
	* get协议文本
	* @return info
	*/
	public String getInfo() {
		return info;
  	}

	/**
	* set协议文本
	* @return info
	*/
	public void setInfo(String info) {
		this.info = info;
 	}

	/**
	* get文档
	* @return file_url
	*/
	public String getFile_url() {
		return file_url;
  	}

	/**
	* set文档
	* @return file_url
	*/
	public void setFile_url(String file_url) {
		this.file_url = file_url;
 	}

}

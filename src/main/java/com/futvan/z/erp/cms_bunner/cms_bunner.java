package com.futvan.z.erp.cms_bunner;

import com.futvan.z.framework.core.SuperBean;
public class cms_bunner extends SuperBean{
	//轮播图组
	private String bunner_group;

	//轮播图片
	private String img;

	//标题
	private String title;

	//链接
	private String url;

	/**
	* get轮播图组
	* @return bunner_group
	*/
	public String getBunner_group() {
		return bunner_group;
  	}

	/**
	* set轮播图组
	* @return bunner_group
	*/
	public void setBunner_group(String bunner_group) {
		this.bunner_group = bunner_group;
 	}

	/**
	* get轮播图片
	* @return img
	*/
	public String getImg() {
		return img;
  	}

	/**
	* set轮播图片
	* @return img
	*/
	public void setImg(String img) {
		this.img = img;
 	}

	/**
	* get标题
	* @return title
	*/
	public String getTitle() {
		return title;
  	}

	/**
	* set标题
	* @return title
	*/
	public void setTitle(String title) {
		this.title = title;
 	}

	/**
	* get链接
	* @return url
	*/
	public String getUrl() {
		return url;
  	}

	/**
	* set链接
	* @return url
	*/
	public void setUrl(String url) {
		this.url = url;
 	}

}

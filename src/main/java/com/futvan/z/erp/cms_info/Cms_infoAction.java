package com.futvan.z.erp.cms_info;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
@Controller
public class Cms_infoAction extends SuperAction{
	@Autowired
	private Cms_infoService cms_infoService;
	
	
	/**
	 * 	打开指定CMS页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/open_cms")
	public ModelAndView open_cms(String number) throws Exception {
		ModelAndView  model = new ModelAndView("erp/cms_info/cms_info");
		cms_info c = null;
		if(z.isNotNull(number)) {
			c = z.cmsForNumber.get(number);
			if(z.isNull(c)) {
				c = new cms_info();
				c.setInfo("参数：根据number未能获取到指定信息");
			}
		}else {
			c = new cms_info();
			c.setInfo("参数：number不能为空");
		}
		model.addObject("cms", c);
		return model;
	}

	
	/**
	 * 根据栏目编号获取CMS信息列表信息
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getCmsListForColumnNumber")
	public @ResponseBody Result getCmsListForColumnNumber(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		Result authority = z.isServiceAuthority(bean,request);
		if(Code.SUCCESS.equals(authority.getCode())){
			String Number = bean.get("Number");
			List<cms_info> list = z.cmsListForColumnNumber.get(Number);
			if(z.isNotNull(list) && list.size()>0) {
				result.setCode(Code.SUCCESS);
				result.setMsg(Number);
				result.setData(list);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据Number未找到信息");
			}
		}else {
			result = authority;
		}
		return result;
	}
	
	/**
	 * 根据栏目Zid编号获取CMS信息列表信息
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getCmsListForColumnZid")
	public @ResponseBody Result getCmsListForColumnZid(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		Result authority = z.isServiceAuthority(bean,request);
		if(Code.SUCCESS.equals(authority.getCode())){
			String zid = bean.get("zid");
			List<cms_info> list = z.cmsList.get(zid);
			if(z.isNotNull(list) && list.size()>0) {
				result.setCode(Code.SUCCESS);
				result.setMsg(zid);
				result.setData(list);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据Zid未找到信息");
			}
		}else {
			result = authority;
		}
		return result;
	}
	
	/**
	 * 根据CMS编号获取CMS信息
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getCmsForNumber")
	public @ResponseBody Result getCmsForNumber(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		Result authority = z.isServiceAuthority(bean,request);
		if(Code.SUCCESS.equals(authority.getCode())){
			String number = bean.get("number");
			cms_info c = z.cmsForNumber.get(number);
			if(z.isNotNull(c)) {
				result.setCode(Code.SUCCESS);
				result.setMsg(number);
				result.setData(c);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据编号未找到信息");
			}
		}else {
			result = authority;
		}
		return result;
	}
	
	/**
	 * 根据CMS主键获取CMS信息
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getCmsForZid")
	public @ResponseBody Result getCmsForZid(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		Result authority = z.isServiceAuthority(bean,request);
		if(Code.SUCCESS.equals(authority.getCode())){
			String zid = bean.get("zid");
			cms_info c = z.cms.get(zid);
			if(z.isNotNull(c)) {
				result.setCode(Code.SUCCESS);
				result.setMsg(zid);
				result.setData(c);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据Zid未找到信息");
			}
		}else {
			result = authority;
		}
		return result;
	}
}

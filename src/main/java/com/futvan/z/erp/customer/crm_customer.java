package com.futvan.z.erp.customer;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class crm_customer extends SuperBean{
	//推荐人
	private String parentid;

	//是否启用
	private String is_enabled;

	//小组编号
	private String group_num;

	//昵称
	private String nickname;

	//性别
	private String sex;

	//身份证号
	private String id_number;

	//生日
	private String birthday;

	//用户名
	private String username;

	//手机号
	private String tel;

	//邮箱
	private String email;

	//级别
	private String level;

	//网体节点序号
	private String node;

	//登陆密码
	private String password_login;

	//支付密码
	private String password_pay;

	//微信OpenId
	private String wechat_openid;

	//照片
	private String photo;

	//学历
	private String education;

	//全积分支付次数
	private String all_points_pay_count;

	//积分支付比例
	private String points_pay_ratio;

	//预付货款支付比较
	private String acc2_pay_ratio;

	//代收货款支付比例
	private String acc3_pay_ratio;

	//是否开通预付
	private String is_ope_aadvance;

	//预付货款达标时间
	private String acc2_targettime;

	//注册地区
	private String areaid;

	//经销商创建时间
	private String level1_createtime;

	//客户地址
	private List<crm_customer_address> crm_customer_address_list;

	/**
	* get推荐人
	* @return parentid
	*/
	public String getParentid() {
		return parentid;
  	}

	/**
	* set推荐人
	* @return parentid
	*/
	public void setParentid(String parentid) {
		this.parentid = parentid;
 	}

	/**
	* get是否启用
	* @return is_enabled
	*/
	public String getIs_enabled() {
		return is_enabled;
  	}

	/**
	* set是否启用
	* @return is_enabled
	*/
	public void setIs_enabled(String is_enabled) {
		this.is_enabled = is_enabled;
 	}

	/**
	* get小组编号
	* @return group_num
	*/
	public String getGroup_num() {
		return group_num;
  	}

	/**
	* set小组编号
	* @return group_num
	*/
	public void setGroup_num(String group_num) {
		this.group_num = group_num;
 	}

	/**
	* get昵称
	* @return nickname
	*/
	public String getNickname() {
		return nickname;
  	}

	/**
	* set昵称
	* @return nickname
	*/
	public void setNickname(String nickname) {
		this.nickname = nickname;
 	}

	/**
	* get性别
	* @return sex
	*/
	public String getSex() {
		return sex;
  	}

	/**
	* set性别
	* @return sex
	*/
	public void setSex(String sex) {
		this.sex = sex;
 	}

	/**
	* get身份证号
	* @return id_number
	*/
	public String getId_number() {
		return id_number;
  	}

	/**
	* set身份证号
	* @return id_number
	*/
	public void setId_number(String id_number) {
		this.id_number = id_number;
 	}

	/**
	* get生日
	* @return birthday
	*/
	public String getBirthday() {
		return birthday;
  	}

	/**
	* set生日
	* @return birthday
	*/
	public void setBirthday(String birthday) {
		this.birthday = birthday;
 	}

	/**
	* get用户名
	* @return username
	*/
	public String getUsername() {
		return username;
  	}

	/**
	* set用户名
	* @return username
	*/
	public void setUsername(String username) {
		this.username = username;
 	}

	/**
	* get手机号
	* @return tel
	*/
	public String getTel() {
		return tel;
  	}

	/**
	* set手机号
	* @return tel
	*/
	public void setTel(String tel) {
		this.tel = tel;
 	}

	/**
	* get邮箱
	* @return email
	*/
	public String getEmail() {
		return email;
  	}

	/**
	* set邮箱
	* @return email
	*/
	public void setEmail(String email) {
		this.email = email;
 	}

	/**
	* get级别
	* @return level
	*/
	public String getLevel() {
		return level;
  	}

	/**
	* set级别
	* @return level
	*/
	public void setLevel(String level) {
		this.level = level;
 	}

	/**
	* get网体节点序号
	* @return node
	*/
	public String getNode() {
		return node;
  	}

	/**
	* set网体节点序号
	* @return node
	*/
	public void setNode(String node) {
		this.node = node;
 	}

	/**
	* get登陆密码
	* @return password_login
	*/
	public String getPassword_login() {
		return password_login;
  	}

	/**
	* set登陆密码
	* @return password_login
	*/
	public void setPassword_login(String password_login) {
		this.password_login = password_login;
 	}

	/**
	* get支付密码
	* @return password_pay
	*/
	public String getPassword_pay() {
		return password_pay;
  	}

	/**
	* set支付密码
	* @return password_pay
	*/
	public void setPassword_pay(String password_pay) {
		this.password_pay = password_pay;
 	}

	/**
	* get微信OpenId
	* @return wechat_openid
	*/
	public String getWechat_openid() {
		return wechat_openid;
  	}

	/**
	* set微信OpenId
	* @return wechat_openid
	*/
	public void setWechat_openid(String wechat_openid) {
		this.wechat_openid = wechat_openid;
 	}

	/**
	* get照片
	* @return photo
	*/
	public String getPhoto() {
		return photo;
  	}

	/**
	* set照片
	* @return photo
	*/
	public void setPhoto(String photo) {
		this.photo = photo;
 	}

	/**
	* get学历
	* @return education
	*/
	public String getEducation() {
		return education;
  	}

	/**
	* set学历
	* @return education
	*/
	public void setEducation(String education) {
		this.education = education;
 	}

	/**
	* get全积分支付次数
	* @return all_points_pay_count
	*/
	public String getAll_points_pay_count() {
		return all_points_pay_count;
  	}

	/**
	* set全积分支付次数
	* @return all_points_pay_count
	*/
	public void setAll_points_pay_count(String all_points_pay_count) {
		this.all_points_pay_count = all_points_pay_count;
 	}

	/**
	* get积分支付比例
	* @return points_pay_ratio
	*/
	public String getPoints_pay_ratio() {
		return points_pay_ratio;
  	}

	/**
	* set积分支付比例
	* @return points_pay_ratio
	*/
	public void setPoints_pay_ratio(String points_pay_ratio) {
		this.points_pay_ratio = points_pay_ratio;
 	}

	/**
	* get预付货款支付比较
	* @return acc2_pay_ratio
	*/
	public String getAcc2_pay_ratio() {
		return acc2_pay_ratio;
  	}

	/**
	* set预付货款支付比较
	* @return acc2_pay_ratio
	*/
	public void setAcc2_pay_ratio(String acc2_pay_ratio) {
		this.acc2_pay_ratio = acc2_pay_ratio;
 	}

	/**
	* get代收货款支付比例
	* @return acc3_pay_ratio
	*/
	public String getAcc3_pay_ratio() {
		return acc3_pay_ratio;
  	}

	/**
	* set代收货款支付比例
	* @return acc3_pay_ratio
	*/
	public void setAcc3_pay_ratio(String acc3_pay_ratio) {
		this.acc3_pay_ratio = acc3_pay_ratio;
 	}

	/**
	* get是否开通预付
	* @return is_ope_aadvance
	*/
	public String getIs_ope_aadvance() {
		return is_ope_aadvance;
  	}

	/**
	* set是否开通预付
	* @return is_ope_aadvance
	*/
	public void setIs_ope_aadvance(String is_ope_aadvance) {
		this.is_ope_aadvance = is_ope_aadvance;
 	}

	/**
	* get预付货款达标时间
	* @return acc2_targettime
	*/
	public String getAcc2_targettime() {
		return acc2_targettime;
  	}

	/**
	* set预付货款达标时间
	* @return acc2_targettime
	*/
	public void setAcc2_targettime(String acc2_targettime) {
		this.acc2_targettime = acc2_targettime;
 	}

	/**
	* get注册地区
	* @return areaid
	*/
	public String getAreaid() {
		return areaid;
  	}

	/**
	* set注册地区
	* @return areaid
	*/
	public void setAreaid(String areaid) {
		this.areaid = areaid;
 	}

	/**
	* get经销商创建时间
	* @return level1_createtime
	*/
	public String getLevel1_createtime() {
		return level1_createtime;
  	}

	/**
	* set经销商创建时间
	* @return level1_createtime
	*/
	public void setLevel1_createtime(String level1_createtime) {
		this.level1_createtime = level1_createtime;
 	}

	/**
	* get客户地址
	* @return 客户地址
	*/
	public List<crm_customer_address> getCrm_customer_address_list() {
		return crm_customer_address_list;
  	}

	/**
	* set客户地址
	* @return 客户地址
	*/
	public void setCrm_customer_address_list(List<crm_customer_address> crm_customer_address_list) {
		this.crm_customer_address_list = crm_customer_address_list;
 	}

}

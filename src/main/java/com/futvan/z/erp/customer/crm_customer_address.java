package com.futvan.z.erp.customer;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class crm_customer_address extends SuperBean{
	//是否默认地址
	private String is_default;

	//收件人
	private String user_name;

	//手机号
	private String tel;

	//所属地区
	private String areaid;

	//详细地址
	private String area_detail;

	//完整地址
	private String allinfo;

	/**
	* get是否默认地址
	* @return is_default
	*/
	public String getIs_default() {
		return is_default;
  	}

	/**
	* set是否默认地址
	* @return is_default
	*/
	public void setIs_default(String is_default) {
		this.is_default = is_default;
 	}

	/**
	* get收件人
	* @return user_name
	*/
	public String getUser_name() {
		return user_name;
  	}

	/**
	* set收件人
	* @return user_name
	*/
	public void setUser_name(String user_name) {
		this.user_name = user_name;
 	}

	/**
	* get手机号
	* @return tel
	*/
	public String getTel() {
		return tel;
  	}

	/**
	* set手机号
	* @return tel
	*/
	public void setTel(String tel) {
		this.tel = tel;
 	}

	/**
	* get所属地区
	* @return areaid
	*/
	public String getAreaid() {
		return areaid;
  	}

	/**
	* set所属地区
	* @return areaid
	*/
	public void setAreaid(String areaid) {
		this.areaid = areaid;
 	}

	/**
	* get详细地址
	* @return area_detail
	*/
	public String getArea_detail() {
		return area_detail;
  	}

	/**
	* set详细地址
	* @return area_detail
	*/
	public void setArea_detail(String area_detail) {
		this.area_detail = area_detail;
 	}

	/**
	* get完整地址
	* @return allinfo
	*/
	public String getAllinfo() {
		return allinfo;
  	}

	/**
	* set完整地址
	* @return allinfo
	*/
	public void setAllinfo(String allinfo) {
		this.allinfo = allinfo;
 	}

}

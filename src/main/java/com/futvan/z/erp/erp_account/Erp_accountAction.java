package com.futvan.z.erp.erp_account;

import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zuser.z_user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
@Controller
public class Erp_accountAction extends SuperAction{
	@Autowired
	private Erp_accountService erp_accountService;
	
	@RequestMapping(value="/erp_account_list")
	public ModelAndView erp_account_list(@RequestParam HashMap<String,String> bean) throws Exception {
		//从Session中获取用户与组织信息保存到Bean中
		GetSessionInfoToBean(bean,request);
		return erp_accountService.select(bean,"common/form/list");

	}

	/**
	 * 打开充值页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/open_recharge")
	public ModelAndView open_recharge() throws Exception {
		ModelAndView model = new ModelAndView("erp/erp_account/open_recharge");
		if(isMobile()) {
			model.addObject("is_mobile", "1");
		}
		return model;
	}


	/**
	 * 打开退款页面
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/open_refund")
	public ModelAndView open_refund() throws Exception {
		ModelAndView model = new ModelAndView("erp/erp_account/open_refund");
		if(isMobile()) {
			model.addObject("is_mobile", "1");
		}
		return model;
	}
	
	
	/**
	 * 进账
	 * @param userid 客户ID
	 * @param a_type 账户类型
	 * @param amount 进账数量
	 * @param mode 操作模式
	 * @param bizid 业务ID
	 * @return Result.date 明细账ID
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_add")
	public @ResponseBody Result acc_add(String userid,String a_type,String amount,String mode,String bizid) throws Exception {
		return erp_accountService.Add(userid, a_type, amount, mode, bizid);
	}
	
	/**
	 * 出账
	 * @param userid 客户ID
	 * @param a_type 账户类型
	 * @param amount 进账数量
	 * @param mode 操作模式
	 * @param bizid 业务ID
	 * @return Result.date 明细账ID
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_sub")
	public @ResponseBody Result acc_sub(String userid,String a_type,String amount,String mode,String bizid) throws Exception {
		return erp_accountService.Subtract(userid, a_type, amount, mode, bizid);
	}
	
	/**
	 * 解除冻结
	 * @param adid 明细账ID
	 * @return Result.date 明细账ID
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_unfrozen")
	public @ResponseBody Result acc_unfrozen(String adid) throws Exception {
		return erp_accountService.Unfrozen(adid);
	}
	
	/**
	 * 删除明细账
	 * @param adid 明细账ID
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/acc_remove")
	public @ResponseBody Result acc_remove(String adid) throws Exception {
		return erp_accountService.Remove(adid);
	}
	
}

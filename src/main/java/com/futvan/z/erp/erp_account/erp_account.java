package com.futvan.z.erp.erp_account;

import com.futvan.z.framework.core.SuperBean;
public class erp_account extends SuperBean{
	//客户
	private String userid;

	//账户类型
	private String a_type;

	//可用数量
	private String balance;

	//冻结数量
	private String frozen;

	//单位
	private String unit;

	/**
	* get客户
	* @return userid
	*/
	public String getUserid() {
		return userid;
  	}

	/**
	* set客户
	* @return userid
	*/
	public void setUserid(String userid) {
		this.userid = userid;
 	}

	/**
	* get账户类型
	* @return a_type
	*/
	public String getA_type() {
		return a_type;
  	}

	/**
	* set账户类型
	* @return a_type
	*/
	public void setA_type(String a_type) {
		this.a_type = a_type;
 	}

	/**
	* get可用数量
	* @return balance
	*/
	public String getBalance() {
		return balance;
  	}

	/**
	* set可用数量
	* @return balance
	*/
	public void setBalance(String balance) {
		this.balance = balance;
 	}

	/**
	* get冻结数量
	* @return frozen
	*/
	public String getFrozen() {
		return frozen;
  	}

	/**
	* set冻结数量
	* @return frozen
	*/
	public void setFrozen(String frozen) {
		this.frozen = frozen;
 	}

	/**
	* get单位
	* @return unit
	*/
	public String getUnit() {
		return unit;
  	}

	/**
	* set单位
	* @return unit
	*/
	public void setUnit(String unit) {
		this.unit = unit;
 	}

}

package com.futvan.z.erp.erpdocs;

import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
@Controller
public class Erp_docsErp_docs_select_buttonButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/erp_docs_select_button")
	public ModelAndView erp_docs_select_button(@RequestParam HashMap<String,String> bean) throws Exception {
		String UserId = getUserId();
		bean.put("otherwhere", " (create_user='"+UserId+"' or addressee='"+UserId+"') ");
		return commonService.list(bean,"common/form/list",request);
	}
}

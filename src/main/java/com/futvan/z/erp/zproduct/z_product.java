package com.futvan.z.erp.zproduct;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_product extends SuperBean{
	//标题
	private String title;

	//所属品类
	private String type;

	//品牌
	private String brand;

	//首图
	private String img1;

	//是否发布
	private String is_publish;

	//规格
	private String specifications;

	//重量
	private String weight;

	//单位
	private String unit;

	//PV值
	private String pv;

	//零售价
	private String price_retail;

	//成本价格
	private String price_cost;

	//保质期[天]
	private String shelflife;

	//二维码
	private String qrcode;

	//厂家编号
	private String manufacturer_number;

	//说明书图片
	private String img2;

	//产品描述
	private String info;

	//详细描述
	private String info_html;

	//热度值
	private String hot;

	//规格信息
	private String specifications_info;

	//是否直销品
	private String is_direct_sale;

	//产品特点
	private List<z_product_characteristic_detail> z_product_characteristic_detail_list;

	//产品图片
	private List<z_product_img> z_product_img_list;

	/**
	* get标题
	* @return title
	*/
	public String getTitle() {
		return title;
  	}

	/**
	* set标题
	* @return title
	*/
	public void setTitle(String title) {
		this.title = title;
 	}

	/**
	* get所属品类
	* @return type
	*/
	public String getType() {
		return type;
  	}

	/**
	* set所属品类
	* @return type
	*/
	public void setType(String type) {
		this.type = type;
 	}

	/**
	* get品牌
	* @return brand
	*/
	public String getBrand() {
		return brand;
  	}

	/**
	* set品牌
	* @return brand
	*/
	public void setBrand(String brand) {
		this.brand = brand;
 	}

	/**
	* get首图
	* @return img1
	*/
	public String getImg1() {
		return img1;
  	}

	/**
	* set首图
	* @return img1
	*/
	public void setImg1(String img1) {
		this.img1 = img1;
 	}

	/**
	* get是否发布
	* @return is_publish
	*/
	public String getIs_publish() {
		return is_publish;
  	}

	/**
	* set是否发布
	* @return is_publish
	*/
	public void setIs_publish(String is_publish) {
		this.is_publish = is_publish;
 	}

	/**
	* get规格
	* @return specifications
	*/
	public String getSpecifications() {
		return specifications;
  	}

	/**
	* set规格
	* @return specifications
	*/
	public void setSpecifications(String specifications) {
		this.specifications = specifications;
 	}

	/**
	* get重量
	* @return weight
	*/
	public String getWeight() {
		return weight;
  	}

	/**
	* set重量
	* @return weight
	*/
	public void setWeight(String weight) {
		this.weight = weight;
 	}

	/**
	* get单位
	* @return unit
	*/
	public String getUnit() {
		return unit;
  	}

	/**
	* set单位
	* @return unit
	*/
	public void setUnit(String unit) {
		this.unit = unit;
 	}

	/**
	* getPV值
	* @return pv
	*/
	public String getPv() {
		return pv;
  	}

	/**
	* setPV值
	* @return pv
	*/
	public void setPv(String pv) {
		this.pv = pv;
 	}

	/**
	* get零售价
	* @return price_retail
	*/
	public String getPrice_retail() {
		return price_retail;
  	}

	/**
	* set零售价
	* @return price_retail
	*/
	public void setPrice_retail(String price_retail) {
		this.price_retail = price_retail;
 	}

	/**
	* get成本价格
	* @return price_cost
	*/
	public String getPrice_cost() {
		return price_cost;
  	}

	/**
	* set成本价格
	* @return price_cost
	*/
	public void setPrice_cost(String price_cost) {
		this.price_cost = price_cost;
 	}

	/**
	* get保质期[天]
	* @return shelflife
	*/
	public String getShelflife() {
		return shelflife;
  	}

	/**
	* set保质期[天]
	* @return shelflife
	*/
	public void setShelflife(String shelflife) {
		this.shelflife = shelflife;
 	}

	/**
	* get二维码
	* @return qrcode
	*/
	public String getQrcode() {
		return qrcode;
  	}

	/**
	* set二维码
	* @return qrcode
	*/
	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
 	}

	/**
	* get厂家编号
	* @return manufacturer_number
	*/
	public String getManufacturer_number() {
		return manufacturer_number;
  	}

	/**
	* set厂家编号
	* @return manufacturer_number
	*/
	public void setManufacturer_number(String manufacturer_number) {
		this.manufacturer_number = manufacturer_number;
 	}

	/**
	* get说明书图片
	* @return img2
	*/
	public String getImg2() {
		return img2;
  	}

	/**
	* set说明书图片
	* @return img2
	*/
	public void setImg2(String img2) {
		this.img2 = img2;
 	}

	/**
	* get产品描述
	* @return info
	*/
	public String getInfo() {
		return info;
  	}

	/**
	* set产品描述
	* @return info
	*/
	public void setInfo(String info) {
		this.info = info;
 	}

	/**
	* get详细描述
	* @return info_html
	*/
	public String getInfo_html() {
		return info_html;
  	}

	/**
	* set详细描述
	* @return info_html
	*/
	public void setInfo_html(String info_html) {
		this.info_html = info_html;
 	}

	/**
	* get热度值
	* @return hot
	*/
	public String getHot() {
		return hot;
  	}

	/**
	* set热度值
	* @return hot
	*/
	public void setHot(String hot) {
		this.hot = hot;
 	}

	/**
	* get规格信息
	* @return specifications_info
	*/
	public String getSpecifications_info() {
		return specifications_info;
  	}

	/**
	* set规格信息
	* @return specifications_info
	*/
	public void setSpecifications_info(String specifications_info) {
		this.specifications_info = specifications_info;
 	}

	/**
	* get是否直销品
	* @return is_direct_sale
	*/
	public String getIs_direct_sale() {
		return is_direct_sale;
  	}

	/**
	* set是否直销品
	* @return is_direct_sale
	*/
	public void setIs_direct_sale(String is_direct_sale) {
		this.is_direct_sale = is_direct_sale;
 	}

	/**
	* get产品特点
	* @return 产品特点
	*/
	public List<z_product_characteristic_detail> getZ_product_characteristic_detail_list() {
		return z_product_characteristic_detail_list;
  	}

	/**
	* set产品特点
	* @return 产品特点
	*/
	public void setZ_product_characteristic_detail_list(List<z_product_characteristic_detail> z_product_characteristic_detail_list) {
		this.z_product_characteristic_detail_list = z_product_characteristic_detail_list;
 	}

	/**
	* get产品图片
	* @return 产品图片
	*/
	public List<z_product_img> getZ_product_img_list() {
		return z_product_img_list;
  	}

	/**
	* set产品图片
	* @return 产品图片
	*/
	public void setZ_product_img_list(List<z_product_img> z_product_img_list) {
		this.z_product_img_list = z_product_img_list;
 	}

}

package com.futvan.z.erp.zproduct;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_product_characteristic_detail extends SuperBean{
	//特点
	private String characteristic;

	/**
	* get特点
	* @return characteristic
	*/
	public String getCharacteristic() {
		return characteristic;
  	}

	/**
	* set特点
	* @return characteristic
	*/
	public void setCharacteristic(String characteristic) {
		this.characteristic = characteristic;
 	}

}

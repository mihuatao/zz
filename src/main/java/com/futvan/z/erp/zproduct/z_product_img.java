package com.futvan.z.erp.zproduct;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_product_img extends SuperBean{
	//图片
	private String img;

	/**
	* get图片
	* @return img
	*/
	public String getImg() {
		return img;
  	}

	/**
	* set图片
	* @return img
	*/
	public void setImg(String img) {
		this.img = img;
 	}

}

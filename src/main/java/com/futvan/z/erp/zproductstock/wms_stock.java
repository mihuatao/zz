package com.futvan.z.erp.zproductstock;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class wms_stock extends SuperBean{
	//仓库
	private String storehouseid;

	//产品
	private String productid;

	//库存量
	private String amount;

	//批次
	private List<wms_stock_batch> wms_stock_batch_list;

	/**
	* get仓库
	* @return storehouseid
	*/
	public String getStorehouseid() {
		return storehouseid;
  	}

	/**
	* set仓库
	* @return storehouseid
	*/
	public void setStorehouseid(String storehouseid) {
		this.storehouseid = storehouseid;
 	}

	/**
	* get产品
	* @return productid
	*/
	public String getProductid() {
		return productid;
  	}

	/**
	* set产品
	* @return productid
	*/
	public void setProductid(String productid) {
		this.productid = productid;
 	}

	/**
	* get库存量
	* @return amount
	*/
	public String getAmount() {
		return amount;
  	}

	/**
	* set库存量
	* @return amount
	*/
	public void setAmount(String amount) {
		this.amount = amount;
 	}

	/**
	* get批次
	* @return 批次
	*/
	public List<wms_stock_batch> getWms_stock_batch_list() {
		return wms_stock_batch_list;
  	}

	/**
	* set批次
	* @return 批次
	*/
	public void setWms_stock_batch_list(List<wms_stock_batch> wms_stock_batch_list) {
		this.wms_stock_batch_list = wms_stock_batch_list;
 	}

}

package com.futvan.z.erp.zproductstock;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class wms_stock_batch extends SuperBean{
	//入库日期
	private String time_warehousing;

	//入库数量
	private String amount;

	//生产日期
	private String production_date;

	//出库量
	private String out_amount;

	//过期日期
	private String expiration_date;

	//录入人
	private String userid;

	/**
	* get入库日期
	* @return time_warehousing
	*/
	public String getTime_warehousing() {
		return time_warehousing;
  	}

	/**
	* set入库日期
	* @return time_warehousing
	*/
	public void setTime_warehousing(String time_warehousing) {
		this.time_warehousing = time_warehousing;
 	}

	/**
	* get入库数量
	* @return amount
	*/
	public String getAmount() {
		return amount;
  	}

	/**
	* set入库数量
	* @return amount
	*/
	public void setAmount(String amount) {
		this.amount = amount;
 	}

	/**
	* get生产日期
	* @return production_date
	*/
	public String getProduction_date() {
		return production_date;
  	}

	/**
	* set生产日期
	* @return production_date
	*/
	public void setProduction_date(String production_date) {
		this.production_date = production_date;
 	}

	/**
	* get出库量
	* @return out_amount
	*/
	public String getOut_amount() {
		return out_amount;
  	}

	/**
	* set出库量
	* @return out_amount
	*/
	public void setOut_amount(String out_amount) {
		this.out_amount = out_amount;
 	}

	/**
	* get过期日期
	* @return expiration_date
	*/
	public String getExpiration_date() {
		return expiration_date;
  	}

	/**
	* set过期日期
	* @return expiration_date
	*/
	public void setExpiration_date(String expiration_date) {
		this.expiration_date = expiration_date;
 	}

	/**
	* get录入人
	* @return userid
	*/
	public String getUserid() {
		return userid;
  	}

	/**
	* set录入人
	* @return userid
	*/
	public void setUserid(String userid) {
		this.userid = userid;
 	}

}

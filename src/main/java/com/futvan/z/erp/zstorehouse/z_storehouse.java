package com.futvan.z.erp.zstorehouse;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_storehouse extends SuperBean{
	//地址
	private String address;

	//管理员
	private String admin;

	//手机号
	private String tel;

	//配送区域
	private List<z_storehouse_area> z_storehouse_area_list;

	/**
	* get地址
	* @return address
	*/
	public String getAddress() {
		return address;
  	}

	/**
	* set地址
	* @return address
	*/
	public void setAddress(String address) {
		this.address = address;
 	}

	/**
	* get管理员
	* @return admin
	*/
	public String getAdmin() {
		return admin;
  	}

	/**
	* set管理员
	* @return admin
	*/
	public void setAdmin(String admin) {
		this.admin = admin;
 	}

	/**
	* get手机号
	* @return tel
	*/
	public String getTel() {
		return tel;
  	}

	/**
	* set手机号
	* @return tel
	*/
	public void setTel(String tel) {
		this.tel = tel;
 	}

	/**
	* get配送区域
	* @return 配送区域
	*/
	public List<z_storehouse_area> getZ_storehouse_area_list() {
		return z_storehouse_area_list;
  	}

	/**
	* set配送区域
	* @return 配送区域
	*/
	public void setZ_storehouse_area_list(List<z_storehouse_area> z_storehouse_area_list) {
		this.z_storehouse_area_list = z_storehouse_area_list;
 	}

}

package com.futvan.z.erp.zstorehouse;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class z_storehouse_area extends SuperBean{
	//配送区域
	private String areaid;

	/**
	* get配送区域
	* @return areaid
	*/
	public String getAreaid() {
		return areaid;
  	}

	/**
	* set配送区域
	* @return areaid
	*/
	public void setAreaid(String areaid) {
		this.areaid = areaid;
 	}

}

package com.futvan.z.framework.common.bean;
/**
 * 查询条件
 * @author 4223947@qq.com
 * @CreateDate 2018-09-27
 */
public class QueryTerms {
	private String ComparisonColumnId;
	private String ComparisonType;
	private String ComparisonValue;
	private String ConnectorType;
	public String getComparisonColumnId() {
		return ComparisonColumnId;
	}
	public void setComparisonColumnId(String comparisonColumnId) {
		ComparisonColumnId = comparisonColumnId;
	}
	public String getComparisonType() {
		return ComparisonType;
	}
	public void setComparisonType(String comparisonType) {
		ComparisonType = comparisonType;
	}
	public String getComparisonValue() {
		return ComparisonValue;
	}
	public void setComparisonValue(String comparisonValue) {
		ComparisonValue = comparisonValue;
	}
	public String getConnectorType() {
		return ConnectorType;
	}
	public void setConnectorType(String connectorType) {
		ConnectorType = connectorType;
	}
	
}

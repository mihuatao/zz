package com.futvan.z.framework.common.bean;
/**
 * 返回对象
 * @author 4223947@qq.com
 * @param <T>
 * @CreateDate 2018-06-08
 */
public class Result{
	public Code code;//状态码
	public String msg;//信息
	public Object data;//数据

	public void setSuccess() {
		this.code = Code.SUCCESS;
		this.msg = "ok";
	}

	public void setSuccess(String msg,Object data) {
		this.code = Code.SUCCESS;
		this.msg = msg;
		this.data = data;
	}

	public void setSuccess(Object data) {
		this.code = Code.SUCCESS;
		this.msg = "ok";
		this.data = data;
	}

	public void setError(String msg) {
		this.code = Code.ERROR;
		this.msg = msg;
	}

	public void setError(String msg,Object data) {
		this.code = Code.ERROR;
		this.msg = msg;
		this.data = data;
	}

	public static Result success() {
		Result result = new Result();
		result.setCode(Code.SUCCESS);
		return result;
	}

	public static Result success(Object data) {
		Result result = new Result();
		result.setCode(Code.SUCCESS);
		result.setData(data);
		return result;
	}

	public static Result success(String msg,Object data) {
		Result result = new Result();
		result.setCode(Code.SUCCESS);
		result.setMsg(msg);
		result.setData(data);
		return result;
	}

	public static Result error() {
		Result result = new Result();
		result.setCode(Code.ERROR);
		return result;
	}

	public static Result error(String msg) {
		Result result = new Result();
		result.setCode(Code.ERROR);
		result.setMsg(msg);
		return result;
	}

	public static Result error(String msg,Object data) {
		Result result = new Result();
		result.setCode(Code.ERROR);
		result.setMsg(msg);
		result.setData(data);
		return result;
	}

	public boolean isSuccess(){
		if(Code.SUCCESS.equals(this.code)){
			return true;
		}else{
			return false;
		}
	}

	public boolean isError(){
		if(Code.ERROR.equals(this.code)){
			return true;
		}else{
			return false;
		}
	}

	public Code getCode() {
		return code;
	}
	public void setCode(Code code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	//	public T getData() {
	//		return data;
	//	}
	//	public void setData(T data) {
	//		this.data = data;
	//	}
	@Override
	public String toString() {
		return "Result [code=" + code + ", msg=" + msg + ", data=" + data + "]";
	}


}

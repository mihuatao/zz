package com.futvan.z.framework.core;

import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.system.zjob.z_job;
import com.futvan.z.system.zjob.z_job_run;
import com.futvan.z.system.zlog.z_log;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;

public class JobsListener implements JobListener {
	private z_job job;

	public JobsListener(z_job job) {
		this.job = job;
	}
	public String getName() {
		return job.getZid();
	}

	public void jobToBeExecuted(JobExecutionContext context) {
		String key = context.getJobDetail().getKey().toString();
		z_job_run rinfo = new z_job_run();
		rinfo.setZid(z.newZid("z_job_run"));
		rinfo.setPid(job.getZid());

		//设置开始时间
		rinfo.setStart_time(DateUtil.getDateTime());

		//时间表达式
		rinfo.setJobtime(job.getJobtime());

		z.job_run_info.put(key, rinfo);
	}

	public void jobExecutionVetoed(JobExecutionContext context) {
	}

	public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
		z_job_run rinfo = z.job_run_info.get(context.getJobDetail().getKey().toString());
		if(z.isNotNull(rinfo)) {
			//结束时间
			String enttimd = DateUtil.getDateTime();
			rinfo.setEnd_time(enttimd);
			//耗时
			String times = DateUtil.CalcTimeDifference(rinfo.getStart_time(), enttimd);
			rinfo.setTimes(times);
			
			/*
			 * StringBuffer info = new StringBuffer(); info.append(" ").append("\r\n");
			 * info.append("======执行任务====================================").append("\r\n");
			 * info.append("任务名称："+job.getJobname()+"|"+job.getJobclass()).append("\r\n");
			 * info.append("执行开始时间："+rinfo.getStart_time()).append("\r\n");
			 * info.append("执行结束时间："+enttimd).append("\r\n");
			 * info.append("执行耗时："+times+" 秒").append("\r\n");
			 * info.append("=================================================").append(
			 * "\r\n"); z.Log(info);
			 */
			
			//增加日志信息
			if("true".equals(z.sp.get("isInterceptorLog"))) {
				z_log log = new z_log();
				log.setZid(z.newZid("z_log"));
				log.setLogid(z.newNumber());
				log.setCreate_time(DateUtil.getDateTime());
				log.setSystemid(z.sp.get("sid"));
				log.setLogtype("2");//任务日志
				log.setTitle("执行定时任务："+job.getJobname());
				log.setFunctionname(job.getJobclass().substring(job.getJobclass().lastIndexOf(".")+1));
				log.setLoginfo("执行开始时间："+rinfo.getStart_time()+" | 执行结束时间："+enttimd);
				log.setPackagepath(job.getJobclass());
				log.setRuntime(times);
				z.getSqlSession().insert("z_log_insert", log);
			}
			
			//保存信息到数据库
			if("true".equals(z.sp.get("isSaveJobRunInfo"))) {
				int insert_jobrun = z.getSqlSession().insert("z_job_run_insert", rinfo);
				if(insert_jobrun!=1) {
					z.Error("保存任务执行记录出错");
				}else {
					//更新主表最后执行时间
					z_job uj = new z_job();
					uj.setZid(job.getZid());
					uj.setLast_time(enttimd);
					int updatejobLastTime = z.getSqlSession().update("z_job_update_zid",uj);
					if(updatejobLastTime!=1) {
						z.Error("保存任务执行记录出错");
					}else {
						//删除临时记录
						z.job_run_info.remove(context.getJobDetail().getKey().toString());
					}
				}
			}


		}

	}

}

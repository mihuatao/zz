package com.futvan.z.framework.tags;

import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zform.z_form_table;

import javax.servlet.jsp.JspException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EditPage extends SuperTag {
	private String userid;//用户ID
	private String pageType;//页面类型
	private HashMap<String,String> parameter;//所有参数
	private Map<String, List<HashMap>> detail;//所有子表记录
	@Override
	public int doEndTag() throws JspException {
		StringBuffer out_html = new StringBuffer();
		try {
			//获取表对象
			z_form_table table = z.tables.get(parameter.get("tableId"));
			
			out_html.append("<div data-options=\"region:'north'\" class=\"border-top-0 border-right-0 border-left-0\" style=\"height:"+getNorthHeight(parameter.get("is_mobile"))+"px;overflow:hidden;\">").append("\r\n");
			//创建按钮
			CreateFunctionHtml(table.getZ_form_table_button_list(),out_html,pageType,userid,table,parameter);
			out_html.append("</div>").append("\r\n");
			
			//创建表单
			CreateForm(out_html,table,pageType,parameter);
			//如果有下级表，创建下级表列表
			CreateDetailList(detail,out_html,table,pageType);
			//创建JS方法
			CreateJS(out_html,pageType,table,parameter,userid);
			//添加初始HTML代码
			if(z.isNotNull(table.getEdit_ready_html())) {
				out_html.append(table.getEdit_ready_html()).append("\r\n");
			}
		} catch (Exception e) {
			throw new JspException("EditPage:自定义标签构建错误："+e.getMessage());
		}
		//输出HTML
		outHTML(out_html.toString());
		return super.doEndTag();
	}
	
	/**
	 * @return userid
	 */
	public String getUserid() {
		return userid;
	}
	/**
	 * @param userid 要设置的 userid
	 */
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	/**
	 * @return pageType
	 */
	public String getPageType() {
		return pageType;
	}
	/**
	 * @param pageType 要设置的 pageType
	 */
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	/**
	 * @return parameter
	 */
	public HashMap<String, String> getParameter() {
		return parameter;
	}
	/**
	 * @param parameter 要设置的 parameter
	 */
	public void setParameter(HashMap<String, String> parameter) {
		this.parameter = parameter;
	}
	/**
	 * @return detail
	 */
	public Map<String, List<HashMap>> getDetail() {
		return detail;
	}
	/**
	 * @param detail 要设置的 detail
	 */
	public void setDetail(Map<String, List<HashMap>> detail) {
		this.detail = detail;
	}
	
	
	
}

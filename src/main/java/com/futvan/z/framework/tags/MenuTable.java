package com.futvan.z.framework.tags;

import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.z;
import com.futvan.z.system.zmenu.z_menu;

import javax.servlet.jsp.JspException;
import java.util.List;

public class MenuTable extends SuperTag{
	//菜单列表
	private List<z_menu> list;
	//是否是移动端
	private String isMobile;

	@Override
	public int doEndTag() throws JspException {

		StringBuffer out_html = new StringBuffer();
		try {
			if(z.isNotNull(list) && list.size()>0) {
				int i = 0;
				for (z_menu m : list) {
					if(z.isNotNull(m.getNext_menu_list()) && m.getNext_menu_list().size()>0) {
						if(i!=0) {
							out_html.append("<div class=\"col-12\"><hr></div>").append("\r\n");
						}
						out_html.append("<div class=\"col-12 pb-4\"><h2>"+m.getName()+"</h2></div>").append("\r\n");
						out_next_menu(m.getNext_menu_list(),out_html);
					}else {
						String mdvalue = "1".equals(isMobile)?"4":"1";
						out_html.append("	<div class=\"col-"+mdvalue+" text-center text-nowrap\" style=\"cursor:pointer;color: #77778D;\" onclick=\"parent.addTab('"+m.getName()+"','"+m.getUrl()+"','"+m.getMenu_icon()+"','"+m.getZid()+"');\">").append("\r\n");
						out_html.append("		<div class=\"col-12 \"><i class=\""+m.getMenu_icon()+"\" style=\"font-size:50px;\"></i></div>").append("\r\n");
						out_html.append("		<div class=\"col-12 pt-2 pb-5\"><span class=\"\" style=\"font-size: 14px;\">"+m.getName()+"</span></div>").append("\r\n");
						out_html.append("	</div>").append("\r\n");
					}
					i++;
				}
			}else {
				//z.Exception("list is null");
			}
		} catch (Exception e) {
			throw new JspException("MenuTable:自定义标签构建错误："+e.getMessage());
		}
		//输出HTML
		outHTML(out_html.toString());
		return super.doEndTag();
	}
	
	private void out_next_menu(List<z_menu> next_menu_list, StringBuffer out_html) {
		for (z_menu m : next_menu_list) {
			if(z.isNotNull(m.getNext_menu_list()) && m.getNext_menu_list().size()>0) {
				out_html.append("<div class=\"col-12\"><hr></div>").append("\r\n");
				out_html.append("<div class=\"col-12 pl-5 pb-2\"><h4>"+m.getName()+"</h4></div>").append("\r\n");
				out_next_menu(m.getNext_menu_list(),out_html);
			}else {
				String mdvalue = "1".equals(isMobile)?"4":"1";
				out_html.append("<div class=\"col-"+mdvalue+" text-center text-nowrap\" style=\"cursor:pointer;color: #77778D;\" onclick=\"parent.addTab('"+m.getName()+"','"+m.getUrl()+"','"+m.getMenu_icon()+"','"+m.getZid()+"');\">").append("\r\n");
				out_html.append("	<div class=\"col-12 pl-5\"><i class=\""+m.getMenu_icon()+"\" style=\"font-size:50px;\"></i></div>").append("\r\n");
				out_html.append("	<div class=\"col-12 pl-5 pt-3 pb-3\"><span class=\"\" style=\"font-size: 14px;\">"+m.getName()+"</span></div>").append("\r\n");
				out_html.append("</div>").append("\r\n");
			}
		}
	}
	
	public List<z_menu> getList() {
		return list;
	}
	public void setList(List<z_menu> list) {
		this.list = list;
	}

	public String getIsMobile() {
		return isMobile;
	}

	public void setIsMobile(String isMobile) {
		this.isMobile = isMobile;
	}
	
	
}

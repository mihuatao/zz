package com.futvan.z.framework.tags;

import com.futvan.z.framework.core.SuperTag;
import com.futvan.z.framework.core.z;

import javax.servlet.jsp.JspException;

public class form extends SuperTag{
	private String id;
	private String tableId;
	private String zid;
	private String pid;
	private String editType;//insert update
	private String PageType;//edit list
	@Override
	public int doEndTag() throws JspException {
		StringBuffer out_html = new StringBuffer();

		out_html.append("<form id=\"main_form\" style=\"padding:10px;\" enctype=\"multipart/form-data\">").append("\r\n");
		out_html.append("<div class=\"container-fluid\">").append("\r\n");
		out_html.append("<div class=\"row\">").append("\r\n");
		//获取前台标签体信息
		out_html.append(getInfo()).append("\r\n");
		out_html.append("</div>").append("\r\n");
		out_html.append("</div>").append("\r\n");
		if(z.isNotNull(tableId)) {
			out_html.append("<input type=\"hidden\" id=\"tableId_id\" name=\"tableId\" value=\""+tableId+"\" />").append("\r\n");
		}
		if(z.isNotNull(zid)) {
			out_html.append("<input type=\"hidden\" id=\"zid_id\" name=\"zid\" value=\""+zid+"\" />").append("\r\n");
		}
		if(z.isNotNull(pid)) {
			out_html.append("<input type=\"hidden\" id=\"pid_id\" name=\"pid\" value=\""+pid+"\" />").append("\r\n");
		}
		if(z.isNotNull(editType)) {
			out_html.append("<input type=\"hidden\" id=\"editType\" value=\""+editType+"\" />").append("\r\n");
		}
		if(z.isNotNull(PageType)) {
			out_html.append("<input type=\"hidden\" id=\"PageType\" value=\""+PageType+"\" />").append("\r\n");
		}
		out_html.append("</form>").append("\r\n");
		outHTML(out_html.toString());
		return super.doEndTag();
	}
	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id 要设置的 id
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return tableId
	 */
	public String getTableId() {
		return tableId;
	}
	/**
	 * @param tableId 要设置的 tableId
	 */
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	/**
	 * @return zid
	 */
	public String getZid() {
		return zid;
	}
	/**
	 * @param zid 要设置的 zid
	 */
	public void setZid(String zid) {
		this.zid = zid;
	}
	/**
	 * @return pid
	 */
	public String getPid() {
		return pid;
	}
	/**
	 * @param pid 要设置的 pid
	 */
	public void setPid(String pid) {
		this.pid = pid;
	}
	/**
	 * @return editType
	 */
	public String getEditType() {
		return editType;
	}
	/**
	 * @param editType 要设置的 editType
	 */
	public void setEditType(String editType) {
		this.editType = editType;
	}
	/**
	 * @return pageType
	 */
	public String getPageType() {
		return PageType;
	}
	/**
	 * @param pageType 要设置的 pageType
	 */
	public void setPageType(String pageType) {
		PageType = pageType;
	}


}

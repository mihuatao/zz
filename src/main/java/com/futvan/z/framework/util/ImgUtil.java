package com.futvan.z.framework.util;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.z;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import net.coobird.thumbnailator.Thumbnails;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * 图片工具类
 * @author 4223947@qq.com
 *
 */
public class ImgUtil {
	// 二维码尺寸
	private static final int QRCODE_SIZE = 300;
	// LOGO尺寸
	private static final int LOGO_SIZE = 50;


	public static void main(String[] args) throws Exception {
		String d = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gIoSUNDX1BST0ZJTEUAAQEAAAIYAAAAAAIQAABtbnRyUkdCIFhZWiAAAAAAAAAAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAAHRyWFlaAAABZAAAABRnWFlaAAABeAAAABRiWFlaAAABjAAAABRyVFJDAAABoAAAAChnVFJDAAABoAAAAChiVFJDAAABoAAAACh3dHB0AAAByAAAABRjcHJ0AAAB3AAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAFgAAAAcAHMAUgBHAEIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFhZWiAAAAAAAABvogAAOPUAAAOQWFlaIAAAAAAAAGKZAAC3hQAAGNpYWVogAAAAAAAAJKAAAA+EAAC2z3BhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABYWVogAAAAAAAA9tYAAQAAAADTLW1sdWMAAAAAAAAAAQAAAAxlblVTAAAAIAAAABwARwBvAG8AZwBsAGUAIABJAG4AYwAuACAAMgAwADEANv/bAEMAEAsMDgwKEA4NDhIREBMYKBoYFhYYMSMlHSg6Mz08OTM4N0BIXE5ARFdFNzhQbVFXX2JnaGc+TXF5cGR4XGVnY//bAEMBERISGBUYLxoaL2NCOEJjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY//AABEIAcICWAMBIgACEQEDEQH/xAAbAAEBAAMBAQEAAAAAAAAAAAAAAQIDBAUGB//EAEMQAAIBAwMCAwUFBgQEBQUAAAABAgMEEQUhMRJBBhNRIjJhcYEUI0JSkTM0c6GxwRUkNWIlNlNjFkNy4fFEgpLR8P/EABgBAQEBAQEAAAAAAAAAAAAAAAABAgME/8QAHxEBAQEAAwEBAQEBAQAAAAAAAAERAiExQRJRIgMy/9oADAMBAAIRAxEAPwDXU3k2YlZES9kMFSBQpgAvDIgCkAFIUAAgAAA0TsAUCApWBiOwYAAhQJ3BSEAhQUAABO5UCgAAAKRFKBQCIoAwFCgBAhkQKAAJh2IVbAAuAUAAgVFAoAQAKAAAFIioFAoADAACDAwAgMFBcQBQNNY4LgFC6gKMF1EwRozwTAgwBmAa4WXAKY11Qq4AIBSZAFBClEKGCAhgqRQMShgKgAIKRlwTBRAUhEATqI8sqrnBOpEwMAMsm5lgYJBEZkRSgAAighQGCgE0XACLgABgFFASKBAigggBQICkKAKgAwCgIJAoKgikKgoAUInYoKURAvcYAdgUmAA4KAiFQwUqBCgCYBQEEAUogKMbAQFBqI4SAdzi7BQXAEwEUFXDBcAEBDBO5kAItigCDGwyTJBQzENAXIJgpRAUhBMDBQUQFBBCNGRGBYlIi4GiFAwAwVDBUgARQABQAwCgoFAIICgIgKAICgoAAAChBAoKUQoKBMAuAMQBQAAGClCgBAhQAABUUhQBChoBBFCQKAKAICgo4CbAHJ1UEKUOQEi4ICIVF5CokXsCPuwDkQi3MZVaUHiVSOfmBnguDVK5oRxmrEsLilUT6KkX9SDZgYCafDX6jIAhQwICsgAhQBBgpCARlI+SioyJD3lngya3AgKABUQoDAKUAAUCFAAAFAADAiICgBgmDIARAIpQCKAhgFAAFBQBQEQoAAAuCh3AKExECgCArBQwMAoRAUYAhQXGxQAwCoAAJrzi4IU5O5gqHAQABs01riFKDk+xYNvDNNW7o0n7U/0PMur+rJYhLGficHW8tt5ZcZ178tRt4xzls4q2ryk+mlDC9TzHJy5ZYosi9uypfVpxa68J+hypNvL59TLGxYRyayfEIwUnvubVCL+HyMajVKOEsyZFPC3YLXTBuHuyf6m6N3ViuE38zi85JeplKt0JN8vsMlTx6VLUabko1U4N9+x2cpNPKfB4Kq9UPahHH8zrtLt0oqnJrHbJzvH+NS569MmDXCvCWd9jYmmk1ujLS4IyhkEBUGEQj5KQsGUSkijIKhQCAUBFAoKABQEQowAAKCGAwUFEABUAB2IoikXJlgIiLgqBTBAYKghgYAKGAUBAFBRC7gAAUBEBcACAoaABAFQKAigUYCAAAImAUFHnAIpzdwj2WexcGi7q9KUE9+SJWNSq3lLZLl5PJuq/XJqPCOi9rK3teiPvzZ5eTUZG8vLAKuSqJGyCMMlTwanQzb3L1dPBjCnKT2WxvVpUksYeRama5ZNyl1MNs9SGlS6U2jppaP1bt5+BjWpxeEpJdyuWXlvLPpI6JQlHEonPceG223QqY+D4H6a/NeGpMqm08rk2XVldWUmq1J9P5lwaFJPuWMV3W9x7WNj0barLqSx7LPCi8PKOy0qyUll4JR7q9QY031QRkc2kBQBCMyMWgLAyMafczYEKAARUCgEUhUUAigAMApBEUAooAQRAUgAhQAXJkYoyAFAKAACBSFRRQC4CICgAMAFQKQoAhQEQuAChgAAEAUqGAABcAAIAAqPOGdyFRydxvB5l1VSqtyeyPRm9jw9QrLy3BL2pSyIlclerKtVcnxwjWCNmhkU2UbedTDawjtpWcU03ga1jjp0Jz+B107TMllHZTopdjppwXoTWvyxtbSCjujqVKPUsRRlTisG6EEt+5NakZRgmuDop00aFI6KT2yFxujTXoZumhBrBs2wErnqUo1IuNSKlH0Z4974bta6cqLdKf8j3WssvlpoD88vbC40+r01otrtJcGFGeHtvk+71CxjdUJRksnxd1ZStarg1jHDNSufKY9TT5uUGpduDtPN0qopuUHtJfzPSMcvSAAIHYwZn2MGBlDczJDZbFAhQUAECjAKAUUAAAUEEKClEBQIiEMgBjgpQFTuZE7lKgUiKEAClAABKFCKAAAQAKUQoAAAFEKAGQDBQqAoDKYKAaVUAUIxBQEebuEvgUHJ3abhqNJ54PnbuanXeHlJYPorlpUZOWOlep8zOSlOUlw2WeM/WDOi0t/MfXJbdjVSpurUUVx3Z61KmoRUYrZCtyMoQS7G2KwRFTMtt1M3R3Zz0s5e51Ulug1I2w9nBs60jHoytjRJS6ullxXSp5ZupSeDClR23N0YrPpgDpgzYmcUr6jRfTlSkbY3tNrKNYzbHUjcllHJG6i+Iv9DdSuIT23i/RjEt1tcE1g+e8QWfs9fSfSI0XtCNxQlFrfsQs1+ewnK2rxqx5i9/ie/TqKrTjOPDR5WoW3k1JRa7mzSq3S5UnLvsOU+ucemwUGGkMcbmfYx7hGSMiLhFKAAAoLgAAUIgIoRQAAAAoAEKhgoEBQICjBEC4IUoAYKigAUIAFKAAQRRgAohQUiIUAoEwUFRCtAAAAAAAQKkAaQKAAABUeax2KOxyd3JqOFaSTTeeEfNdj6bUZKnazefkfNwTlOK9WWVmTt6VjRUKSfd9zrwSGFFJGTMuiFSGUluzXO4jT7ZY9XZHZThsdNNYwefRvcrDRuVdtpouY1OUepBJh0U5ZNFGusJN7nXCSksojbZtCDfwPOrVK0pYiet5XXTwaXTVJSljL7FnSWa86lZT3nNPLOu3tYqWZSfyJ9rUqUutdE0+BQrLHVKSivWTLebM4PUo0af5Ts+xU5RzjDPLo6nZRl0u4Tl8Nz1KF7RqxXl1FL6ll/pjDyKlJ4ftL1Dz6HampxNVSkluiWE/j5nxHYqdDz4LdcnzND7u6g1um8H317Q861qU/VPB8DOPRLD2lCW4+Mcpj3I8IpjTfVTi/VGZzEfBg89jN8Ea22CMorEUUkdkjIoAAClIVEApQUAAQCgFAoBBCgFEBSEAIoKh3KQoAAFFRSIoQIlgoEAowCoFAKAKgEQoAAMAAQoKiFACAAAFICigAIAAo84IoObtXHqKTtZp84PnqX7SLXqfQ6mn9jm1+h85T5j8BCPag8xM1JRTbNVL3UY1W37KI2xlUlN7inazqvqwbKVNLGTuoShDDlJJfEupOOtMNPqJJrBlKnKltJHfHVLOnHEm3g57u8s7iLdKa6vQl1qSRzOrh7HdY3PV7Odzycm6hLpmmiNx9farqwS/ouFOU0uxq06q3CLO+vN1aE4Y5TNTwvVfKTnmnKq/dTwvizzb+FWpaxqtyazvFPhHrXVL7ujRax0Zb+ZhTt54wmnF+pmHLa4/DELV3VV1Je3heWpep9NO1pVL11bfNKC5a7s4rKyoUn1KnFS9Uj1oOKjjB0/W9M5jfQuOhdMsvHc6PPjPZI4Vu9jZHJD7rdJb5Phdftvsuq1Fj2Ki6kfdJto+e8X2nmWdO5ivapvD+QhzmvOsnm2j8Fg6Dl015tY7nWYrCMiwVmEuAjYioR4RcEAAFFKiYKQUAAACgECgoIFAEAAAFIEABgAUhRBQAigjLBCoAThlGAgUFKICgIIAuCogKQAAUCAoKiAoCICgoAYAApClQABR5xQDi7NVzTU6E0+Wj5WS6Zyi+zPr2spnzWp0fKuXhbSLE8rsovNKL9UMe1k12Uuu3XqtjdJYEbY1KypRyc1LzLrrk2+lcfEtak6s8ZeDoo0lSWIt4GnrypdHViSnF/qdUKcJ006UWvRvk7ZUouXU4rPrgKG4/ROJSUvLXUt0bKaxIYwhD3jDpOn0emN+Wj1Eso8rTWlTiu56sXsakW+uW6tI1N2tzhdB03twe44qcdjirwSzsLCXHHCfS1lHRCo5bGnoyzZTWGGnZTRuhHLNNI7aMe5qdsw8t42Oa9oRr2tWjPicWj0DRXiSpx5frp41potvb2Wa0nhb5OC8tvslWHRPro1PdkfTwjGtbdEl2wePd0G9Oq0mszoSyvkZpZLHlM11N0bc5WTXU5WPUObcuEAALgABFKiFQVQARAAFFAAAoAAAACFIBQAAKQqKgUhQCKEUCFARUCgoEBQUAAggAUIgKAICjARAAVAAFBAFAAAqAAKPPKBg5OqHm6zaeZbeZHmO56mCSipRcWspj6Pm9Ll78dvU9JQycDpfZNU6VtGXB6VN7ErcrW6ajwEjZLGSNBqMSYMgTG8Yt7GVPCW/qYvBouKjprqe0fUuM6+lsOlKOGe9QpQlQzP/wCD4/SrtTisS3R9FUqXUtMm7TDqtbZLxa5eO6NDG9Kf0ZwanKVKPU47dz5qx1XVbC98u8jUcW9+pH1V5Whcac5vho1y/iz+vNpVVL6nVT3aPIUpUcPD6T07OopxTOet2da9GlHg7aaxE5KJ2Rzg6cXLl4yNVf3TYYVuCVjh/wCnPTreTnqj7L3yuxrqwUrmUkswq03kXMlTt6sn+UwspSq2UJSzlbGNdrx+vnY7RwYy95L4mbXTUnB8xk0YP30HBtBSBFKiIoVSkKEAAQAAUUAAUEwUAAAAACAAChVwQyQQAAAoKigUAoFIBiKACoFIUAAAgAAAAKgQoAgKCoAAAEClRAUAcAAObqFJgBXn6pauoo1o46obmmnLKT9TvvK8aMEnhdXGTz4tY2eV8Asbky5yalIvUHSM9iMmQ2RWio3nkt35dTSpYa601sZSimaZ0FnOe/BKbjDTbWanDpk029z66jcVaCp0ptbrlHh6dT+/ikenqc1TlTaftp8FWX49WTjVhipCM/mjCdN1IqDb6F+FLYtnLzraM2mmzoUcGuk/VnTQraDj0uKx6GmFpK3m3DeD7Hd0tFW+zJiTnVt3nB3x4OOFLollHXF7Gp4c/GRrqGw11NyMcPXPUhGcemazFtZRnVj0xUaaSjnsY1F7LfobaThVgpJ/MjtXzOow6NSrx9cP+RyLeojs1OpGpqdZxeUsLJxx/aoz45X1tAKECoiKQUpClQAAApABQCgAAAAAAoAADuAgVAdigAAKikKECgFFBClAoAQAKBBgpAAACAAKgACgAUREBQVDAwABAUAcABcHN2TuXuBgDztat3UoRqJZ6eTgtY9NN7YR9BOKnBwlw0eSqXRKdP4hqMMjJin2KFjLJcmBckblZZJyyNmLmorcqWt9O4jbzjPlo7qF5Rua6k1l47ngVLjrliC6n6I67OnVlLOVDBG+L6eNwsJRlhEWqypSxJxmlz6nHaUYOKU5ymz0aVhRxtQTb9St2T66aGqW1xDCajL0ZjG8h5vSpJmmWjQqLM6cKf8A6XuYR0ilRl1QlPPxY7c7Jr2qM41I5ydEeDy7bqo7Pg9KnJSial6Z5eMzGS2ZkRiOc6rTJHzlzVr0bmtShVlGLfCPpZLc+b1LbUKv0MV15XpxKOG92892ZRWHkMR3kRyZkKAsVAhkgBQAgCkAFBRgYAAFAAAAAAAAABUUAIC4GCgAVEyMhFBMjJdGQyY5DKMkxkxRkAyXJiXARQEUIhSFAhQCoAAoYAKExAB2KABQICgqOAAI5uqgAgYycl7R6ZKrH5M7EYVYqdKUWGo8erHpnlcMxTN9aGI59DQiNKVEKitMZM5akKtR9Mc47tnW1kJdkTUzXPCmqawkd1jCVSWz2RhCg5cs6bWhKjPqjP5olrcdtCrGjLLR7dC5hKHsngVKXVvHnJ1WrqpKOdhrV7ey6zxsWMuo5YKb5OmCwa1mxtSTN8PZ4NEcm+Oy3Ks8bFLcsma20TqGn5ZZPl7yp5l9Wl2zg+ilPq9ldz5ea8u4qwaeVJmWOYxT75G/ZGaQc1AIQVFIUClXBCgACFFKQpAAAFAABgAAQpAKACooRCkVQQqKipDCCKVEwMFKBikZYBSiYGCgIYLgACkKAiAoKIMFAQABQAAQIUFEBQIiAAquApEU5ugCgAUgIrhvF5WXhuL74OFST4PbqLMccnnXtHZVIrjkLrlGTHOSNhtnkiZimZIg3wrYjxubaU5Sktzlib6MsTRGtd7k4000stnVazk0mcyaqI6KCcWlkuLr1aO+GdMI5OWhJdK3OmnUSLjO9umEEt2Jv0Nbq7bGuVTbktrexm5GuVRt9Md2a+qU9lwdNCjhbcmTdSnSwt+Tw9Tp9GoVP9yyfTdKSPA1yPTdwljmOAxyuxwYBC4DmEGQFUpFuUIqKYooFIUACmJQKwCAUpCgAAEAgAACKAACAFQY7gVFIU0gUAAikRQgUhQAACKAQooAAAhSgQpAigjGCiggCBSFKAICYy4UMFKYdkBQUAUEwYtZJ5cWmpLZmYA8K9t5WlR5/ZvhnP1ZPo6lOFam4VI9UWeNdaPWptytZKceel8kXXKpGcZnHUnVoS6a1KUH8UVVk1lPIyrK7etCM2pZRx+cbqVZMNa9WlWTSfB30622UzyKVSDXKOmnFPgjT1adz0r+51QuU0uTy6FJZ5Z6NGljA0dEKspvbKRujFvnLFKnsb1Aasm+JSjuddNYNUY4NsY43LFvjYeZrFt59Dqisyhuviemaq3wFc+P8fCf4xaxeJ9cZJ4awZx1Szm8edj5o9LWvDdK/jKtaJU7hb9K4kfF1qVW1rSp3NOVOXxRZjNlj6mN1bz92tB/U2LEvdafyZ8pTgpJ9LTZnGc6fEpL5Mv4Z/T6kp82tQuaKTVVv4Pc66euVFjrpRmn3WxPzT9PayU8yOt22PbhUi/TGSvWrdf+VVf0JlW16RTynrlLtbzfzeDB67HtbS//ACGU165TxZa7U/DbxXzZrlrl0/dhTj9C/mpr3xhvsfNy1e9kv2kY/JGid/dzftXE8fBj8019W9lu0vmzXK5oR96tBfU+TlVqz9+pN/Ux29Czgn6fVu+tF/8AUQ+jMP8AErNc1l+h8vlLhIKWC/g19P8A4pZf9bP0MXq9ms+3J/Q+a6hkfmH6fTR1eyks+bj5o7KVWnXh1UakZr4M+NT+BttrmpYXCq021Fv2kS8T9PrwYUqsbijCtTfsyRmuTPjSsYACLkpCgCkBUqopBkClIAKAMlQAIBQAUACBFyMkBUUEAFBAUUEDYRQQDRxlIUw6CKQEFABVAAQBlgkpRpw65vpivUK5dWjRlZN12lvsz5udDpScHlGzU72V9cPDxSi8RRbaaq03F+9HsW9JO3NiS7GUZNPg6+hehHBGWmNKruetZyc2sI82nTi5LY9qySikkZrpx13W9N+h6dGnssnNQisI7KbwJFrphFGxGqDNnUitcWaNqeUaEzbTeSxOU+tnY1VeDaaqrFc/+frRjGWjytcjbztZO4pwnhd1uehd3Ebek5SPh9d1aV3U8qEvZzvgkjfKvOzTjUm6UXGOdtzGVQ15wsEydJ04XttUjPb0NUTM1O2avRSzmUM/FM29HVHqhLK9DQ5CM3F+y8FhrJxqNrMMIjpy7GxV01u8P4mXWn3QRyttPDTTI2dUlCS9rBzOMVLEZdQGLZMm+EV6GTpxfK/QlVz5GTKpRcFmLyjBPKEguQRgClIEWDNGxRU4uL7mpG+mVmu/w/d9FR2037Mntnsz31sz5FN07pOOz95fM+ptqyuLeFVPPUt/mY5TGuN+NwIVcGGlAAFBMgIpTEuQKCFAFICooIAKCAIpATgooJkDEVggzuaRRkAAMkYAoIAOVAo4MugACBwTJe4aUVmTUV8WAKjjrapZ0Peq9TXaO55F7rlaunToLy4evdgenqGrUrP2Kft1f5I+fub24u5Zq1H09org0cvLbb9WAQXwOuwisTl34OQ6LOp0VHFvklajuwYtGxBmWmtbM9Czqe0k2cXSjfb7SW5K3On0tvLMUdUHvseZaVPZR6FKYi12RZcmqMzKLyaZ1ugzfTeDngbU32Qb3Y3OSOe4qwo0pVaslGEd8s4tR1qz02LdeopVO0IvLPitY1251afS35dBPaKNY52zjG3W9cqXteUKDxTW2TxeO+X6h4isIx5L4522rkq3ZjgyQgzQlIxbwYSka3IyrmWMjUVMzq42SlkxzjgxyQmjJty2bZspxSNcTNSLBuTwHJ52MMmMpYRWXTB+phO26nmm8P0NEZvJ0wmyy6OWUZQeJrDIdsmqixNJo0TtZLenLK9GFaikknCXTJYYAyjyb6Zzrk303waiVncezGnU/LLH0PW0WtiUqD4ftRPKrRUrea+GTOwr+XOjVzw0pFs1I+pBG87rdPcI4OjIgIEZDJjkZAuS5MS5CMhkmRkopcmJcgXIyQBKuQQFAEyGyilMcjIRQCCIuS5MQUXIICi5BASjQEabi6oWsc1qiXw7ni3mu1auYWq8uP5u5l0e5Wr0aCzVqRj82ck9asot+25fJHzMnKcuqpJyb7tkwiGPcuPEEUum3p7vuzybi7uLmTdWpJ/DJpAXEwigAAAALlp5XKICK77a5jOKjJ4l8TpWGtmeMZRnOPuza+pMXXsG2jyePG7qx5xI3w1BR96m/wBSYsr6K3qYaPTozyj5OGswhj7ps3LxJUisU6CXxYka/UfXweToTUVmclFerZ8HLxDqE89M4018EcVa8urh5rV5z+bNSM3k+8u9f06yT6q3my/LDc+d1HxXd3eYWq8in6rk+f2yMpGskZvJnKTnJyqScpPuzByI2QayclRBkKyBMhssQkzArZDNoAhSKZBCoCo2RiYxjlmc5dEfiakQnJR27mpttk5e5nCOWPRacdzojsjGKSK5I0yzMk2jWpZM4gWvBVqD29qPBxJ5WT0EvQ4Jx6Kso/EtWKjdTNCN1MsSumG6x6mi3WPMpv8AC2b4M0v2LtrtJZNJH0mm1vOsoN+9HZnSeRo9XprTpdprK+Z62TjymVqLnIyQhlVyXJiUCggyVGWRkxKBSkAFyCZBpGWSZIMkFGSDIRQQFFAzsTJRRkmRkqKCACggLiPhZylUk5VJOTfqyAHJ2wBABSFAEKCAUEKFAAEAZLHcdOQaxBl0k6fgXDUKMDBBUy5McFwVDI5AIpgpMkbCDYIUKpGGQVAEBFCkAAyREZRWWBsgsJt8I0yl1Sybaz6YKPqaYo1UjKKyzogkjVHYrnsJ0lZTn2RinuYmSLo2RNqZpizNNmkb4SNF9DFRVFxJI2RZslBVaUoP02A4DbA1JNZi+VsZwLC9uqmzC5WKlKfxwzKmy1/2Lfpua+I20KvlXFKou0t/kfRt53Xfc+Um/umfR2NTzbKjJ89O5z5kb2QrIYaUBDIUKTICKimJSighQgAAAIO5UUAmdyigAgAAIAMFApO4KKCAdo+FABzx3QAACkRQBCgCAYAFBABcjJARGWRkgLoyyMmJQLkZICopAQCkAIoUdgBCAAAAQAABUbKa3NaN0Nlk1ErXXlmpj0REYt5k38SkGXUEYlyDGWUOrcxLFFGyLNqNUcIzUjc8ZbUzbTlhmhSM4zQGF3TxNVFw+TUjsbjODjLg5qlGdNZXtR9UVGdNm5rqi0+5zUn3OqLNSo4688uMEfR6VLNlFejPmmv81JP8x72jz9ipDPxMXuNePSYIDmq5KQAUAFRUCAooAAoIUAAQIoICigAiAAKBSAqAA7AMgIFHwwBGc3dAGDIFAKgUgAoAAgKAMSlAEKTBUBQCAUgAAEAAoKAIwAIyFIQCgAAAUZI2N4gzCKE37JZ0jWitkBlVKiIoRUXJCxjk1BU2ZrISKakRUZowKmVG+G5tgspp9zmhPDR0RknEsTxyRXS2l2Z0w4OePP1N8DURy3ScLnK7rJ6Oj1266WMJ7M4r+O1OXxwZ2MvLhGffqyZ+2LfH02QTnDXcHNplkuTFFAoIAi5KTuUqAAAFIOwFD4IUCAFKgAAA7AACkKVBEAIAALo+HIVkZh2QApFACAUEARSkAFKQpQICgQAEAAgFIAQCgpQQBCoEDBlQgAFBClBFRAuSDNGM2UxZdEABBTJIiRmkXE1VEySMXIx6jWyI2ZGTWnuZFlMZlMEzNNF1FWxsizFcFRUZSim8ozgYJma2NRKl3Hrt36x3MaX7Bdzc49VOSfdHPB9NvH54LnY+htJ9dpTlntg3HFpc828oflZ2nGztqKimJQKCdwh4ayCCAAIhUVFAJwQUEBUUZ2AAFIAKAQIoARQAABAAvSPhyFIzm7oACKpAAAACKCFAoAyBQTKHJQIVsmfQgArTSy4tL5EzsA27jtn+x6MVR0+3o1KlGNWvVXUlPiKPR1uedAsZunCnOtJyajHGxqcWdfPF4Aay0vVkk7VOpeoyfZ6rd6Zpcba3radSqylSUpNLDPH16hY/YbS7s6Hk+c3lZN3hMZ/Twwepp9haVNMqXl5VnBRqdC6fkbFpVlXt61W1vJTdKDk4uJjGteOQLg7rOxjUou6up+Xbx/WXwRDXEk5SSim2+yOylpl1Uj1ziqMPzVX0o2vVVR9myt4Ulx1NZkaaNWteXkXcVJTUfaeXthA7arugra5lSVRT6eWl3NRak3VqTqS5k2zEKpiUgAqIVEGUTJvBjnBGyorZAEBki5IhjJYLkqCRkkaxGUHg2IwSM0axms0jNLYxibI+6WJpFnPcrpo7dpI3w4NN7+x+pupHo6TU+86eMrJ6p4Gm1MVKb+h7xy5NTpSkBlVCIilRSkAAqIAMiMAIFIAKQACghQgACgUgKKQpAAACPiCMyMcNtJct7HJ3CbHvah/h2mVYW7slVqKCcm5Pk5P8WpRf3en0F81k1+c9Tb/Hm4b4T/QyhTqVJdMKcpSXKS3PWttZr1LmlTjSowjKSTSgbvOnQlq1ek+icZRjFrtuX8y+JeVjyJWV1CDnK3qRiuW0YW9vWu6ypUIOc2s4PUt724r6VeyrVZTwklntk06S3RtL65TxKNLpi/iyZDb9Fod4v2jpU13cpo0VrCVG+p2s6sX14fUuNzllKclmU2/mz0bj2dRsn/24Do7/AK2u10e3m4VrqrOSeGoxNsK3hynzb3NT6/8AueXqCxqFf/1s5y7hJvr6vTKuhX17TtaWmtOfeUuD5/VYUaWqXUKEemnGo1FLsd3hTC1qMvywkzitaP2/WVDlVKrb+WTezlEzL067PTbelaK+1KbjTl7lNbOQlrkKXs2VjRpwXDkupmvxBdefqMqUNqVD2IpcbHmGOVzqLJvderDXbyVSMZwpSi3jp6Ea7+3hU110aMUlKUU0uxy2UPMvreGM5mv6nsaZTV34oqVPwwlKX6E49pcjg1ySnqkqUfdppU19D1PFqVKhptuvwUkzxp/5jWH/AL639z1vGsv+MU4doUkdfIjwDdZ0/Ovren+aaX8zSd+gw8zXLWL4U8nPj61XR4uqdetzhnanCMV+g1z7vTtNo7rFPq/U5NaqOtrd1LOc1WkdPiaX+at6f5KMUbt1J8Sr7HhWise/Xb/QaR7OmalP/tqJdS9jw/ptP83VL/8Av1Fm/K8N3k84dSaijmvx59jbO7u6dFcSe+OyN2q3KrV/Jo7UKPsxS4+Zu0r/AC9jd3ndR8uHzZy2VorhTqVp+XRh70viBzbcHTbvy7WvVe3UuiL+J3U6lpCxualCgumK6VOe7bZz3kPK0uyS/HmTJhrg7YAIZaAAUCoiMgAAAoAAyWDLY1jJZUxsLlGrJcmv0mNqkXrwa8jJdTG1VGu4VxOPc1kabFpjohdRUvaWxncx86jiDy1ucip55Z00l08Mst8S4xsp4eHs4vg+lg+qEZeqPAnDEvMjy+T27SXVbRfpsOU6SXtvBAZVSoiKFCkKVAABFwQrIQUAncoFIUIoIAKACiggAFZEGAyDEBHxZ2aNb/atYtqXbrUn8lv/AGOI9fwxXt7bUalW4rRpvy3GDlxlmOPrtfHJrNb7Rq1zUXHXhfJbHEe5Lw/50pSt7+hVby8J7nj06FSrcKhT9qcn0o1zl3U48pjbpqzqNuv96O+b/wAlqrfDqrf6mVlpM7S9p1rivQiqbzJde5plNT0m/qL8VeP9ROktla7TbRbx/wC6KLH7rw7L1r1v5L/4JQ20G5/iRLqP3el2FLbeLm/qT4WOGVLFrGq/xSaO67eK1hN94R/qYXkOjSbP/c5MuotqjYvOWqXP1GG1r1KnP/Eq6jCT9rsjT9luOly+z1MLv0na9cvPw+XF+qjudWmalc3dWtRr1OqMqUsLHcXF/wBSOfw5NrUZb4zTkPDuFqnW/wAMJP8AkadFl5eq0l6txN+kR6NYq0ly1OKRJsSvMqydStOXLlJnqvT7GxjFahVnKtJZ8un2+ZnYaJdQvqVSuoQpxn1PMjkup/adVua83mEJt5+HY1g67O3oUtWrVKGfJoUnNZffB0eE1vf3EuY0Xv8AFnPaJw8P3929pVZKmvkdXh/7nw7qdZ90oo3xhfHlaJBV9ctYy3zVTZ0+Kqvm+ILj0jiP8ieEodfiC3faClJ/ozj1Op52qXU85zUl/UXyn1ynseFY51mMn+CDZ457Phn2at5V/JQkcuPq3x5rbr6m3+er/c6/EsurWKi/LFL+Rz6VHzNWt1zmomZ6vPz9Zr+jn0mkdOv+xa6bR/LQz+uCVPu/DFKPepWb/QeJpf8AEYU1xTpRiNW9mz0+2it+jqa+LJiMLpeR4ftKXEq03UfxXY132bewtrdbOa8yX9jdrMerUbe1hxSpwgkc+szU9QnFPaCUV9EK1Frfc6JQjxKtUc38lsb9Xjix0708o16wnTp2NH8lun+p0aus6Lpk1+VoTxHjEKQw2AEIMkUxMoRlOSjBNt9gGQsyeEm/kj1LbS0sSrvL/Kj3dPs402mqcYxKPkXQrJZdGaXr0swzh4aafxP06hCHSl0xZjd6LYajScatGMZ42nFYaNZqa/MwenreiV9Ir4lmdF+7NI8xGbMUKAUVGRijLYsZCkKXRlF7m6LOYyU2iypY7spwwejpks0XH0Z5FKeeT09NlipJdmsm/Yy9EEyU5rpkpAUUqICIpSAoyIAEAAAKiF4AFICgAFyAKQAUjJncMoZBAXB8WMJgdji749nw9HyqV9dy4p0nFfNnP4fh16qqre1KMqjf0/8Ac6G/svhVL8V1Uz9Ea9LSt9I1C6fMo+VH5s651HO/15dWbq1qlRtvqk2d1NdPh2q/zV0v5Hnrg9K5TpeH7WPDqVJSMfVvjGltoNbC5qpDW/ZuKNJcU6MUb7Kl5mmUafercL9EcmoSd1q9RR3zU6F/Qvwnrq1qHl2FhH/t5NGprFCxxlLyu53eKOmP2WmvwwwcGo/uWn/wn/UtiTxwnfob/wCJQS/FFr+RvU7SysreU7WNWpUjltmMdYjSl1ULKlCXZmMytba5rH2dXpY/6uP5k1CUqeqXDpycH1vHSZaZF1dUpye2JObM3XUJ17xxjKdSbUE/6lzU8rRGncVoudSpJQS3lJs230FZ0IWy9+aU6n17HZRhUr3VnbVHmVaanP5ehxazUdTVbl+k+lfTYudJN16l6vI8H2kFzVqdTMqD8nwTWferWwYeIH5ejaXR7qHU0NQkqXhOxpLmpJyOsZPB/sXtzWfFOg2eFKTnUnJ8ttnt+H/utJ1Wv/2ulP6M8JcGOV/y1J2p7WiLo0rUqv8As6Txex69o/L8M3cvz1FE58fV5eNHh2PVrNDPbL/kzTBO41lY36q39zq8OYV5WqvinRkzVoUfM1eE3xHMmVF1l/aNeqxj+dRX8kdV3FV/ENCgl7NLpj+hx6evtWuRnLvUc39Nzos6vVqN9d/khJosGFGf2rxDOq911yl9FwedXk6tepJ8ymzt0r2KV5cd4Unh/M8+L3i36mVer4k9m+pQ3xGjEzvH5nhizl+Wo4k8T/6jTfZ0YkUuvwu4v8FbJU+PIYAMNhCljFzkoxWWyDKjSnWmowX19D2rS1jbJYw592YW1CNvTwl7T5Z0QbIsjapNPK5O+yqOT9qWTz3E7bLpT9pljXWPTz5fu5yd1vWqdPt4fyODpyspnTbyklhnRmu2vSo3lvKlXgqkHymfG6n4QqUpupYVFOD/AAS2aProNrdGmvVUY5yb6Yr4H/A9RziVFR+LZ0UvDtV71qqj8Ee9d3WHhP6HI679TnbFktc1PQbSK+8c5fXAqaRprWM1Kb9VLJunXk1yaJPL33M3k1OLhlpEFUajcOUO225uhotFrevUTOiLOmi02P0l4uKOg028efVRu/8ADM5w+4uW36TR6lPsdlrlS34ZuJ+Xxt3pV9p76qlJyh6x3Lp93GnXh1PZ7PJ+hJKUcSSkn2Z4WveG6VWhK6sodNSO8oLuaYsaNsJrhhM4NLuvNg6M/fh6ncTEZBEKmQUEyOSjIELkIdyghcFBCgAQoDJSACkAADgBlQZGy9iMRUABUfFh8MA4vS9jWttL0tLjy2Se3hOn8bh5/mAdvsco8d8Hq6vtp2mJceUwDl9adum/u9h/97PJ07fWKOd/vVz8wDU+M/a6/ETbr0cvs/6nPqX7pYfwf7gC+s8fIah+7WX8P+5wr+wBnl66R16btVrNbPypGp+9brt0rb6gF4+JXuWf/NlL4R2+Hss8O83v7j+JL+oBr4kex4n/AGGn/wAH/wDRjrn+j6X/AAwDp8T5GWl/8r6l9DwVwAc+fjXEfB7Ef+VZfxwDHArVo37tqP8AAJoP7xX/AIMgC1n+poX77V/hSJZf6VfPviO/1AIq2H+k6h8o/wBzzvQAVY9nxL+8Wv8ABRz0f9Ar/wAVACHx5oAMtB2aYk60m0tkAFel3NtMAyroj2OilyvmAWEejR9066PABuJXR+E828e0gDbEeNX/AGj+RolyAca6p6gAixO5vo8gEHo0OEd1LkA7Ryruh7p1UN1h7gG54R+eVEo+IqqisLzHsvqeo+QCcvWKvYdwCIoAEFJ3AKjLsAC/AL2AFBBgATsXsAAJ3AArD4AESIGAIMe4AKj/2Q==";
		
		System.out.println(Base64ToImg("d:/bbb.png", d));
	}

	/**
	 * 通过阿里云解析发票服务，解析本地发票文件，获取内容  每次解析0.15元
	 * @param filepath 发票文件路径【绝对路径】
	 * @return
	 * @throws Exception 
	 */
	public static Result analysisInvoice(String filepath) throws Exception {
		Result result = new Result();
		File f = new File(filepath);
		if(z.isNotNull(f) && f.isFile()) {
			String host = "https://ocrapi-mixed-multi-invoice.taobao.com/ocrservice/mixedMultiInvoice";
			//设置头参数
			Map<String, String> headers = new HashMap<String, String>();
			//设置AppCode 中间用空格分隔
			headers.put("Authorization", "APPCODE 9aaeabd5d8e14ed19f3d4775dc882d20");
			headers.put("Content-Type", "application/json; charset=UTF-8");
			Map<String, String> querys = new HashMap<String, String>();
			String imginfo = ImageToBase64(filepath);
			querys.put("img", imginfo);
			String bodys = "{\"img\":\""+imginfo+"\"}";
			
			String response = HttpUtil.doPost2(host, headers, querys, bodys);
			try {
				
				JSONObject jsonObject = new JSONObject(response);
				if(!jsonObject.isNull("sid")) {
					JSONArray subMsgs = jsonObject.getJSONArray("subMsgs");
					JSONObject fpjson = subMsgs.getJSONObject(0).getJSONObject("result").getJSONObject("data");
					HashMap rmap = JsonUtil.getObject(fpjson.toString(), HashMap.class);
					result.setCode(Code.SUCCESS);
					result.setData(rmap);
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("解析发票异常|"+response);
				}
			} catch (Exception e) {
				result.setCode(Code.ERROR);
				result.setMsg("解析发票异常|"+response);
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("发票文件未找到|"+filepath);
		}
		return result;
	}

	public static Result Base64ToImg(String path,String imgdate) {
		Result result = new Result();
		if(z.isNotNull(imgdate)) {
			BASE64Decoder decoder = new BASE64Decoder();
			//去掉base64,头描述信息
			if(imgdate.indexOf(";base64,")>=0) {
				imgdate = imgdate.substring(imgdate.indexOf(";base64,")+8);
			}
	        try {
	            // Base64解码
	            byte[] bytes = decoder.decodeBuffer(imgdate);
	            for (int i = 0; i < bytes.length; ++i) {
	                if (bytes[i] < 0) {// 调整异常数据
	                    bytes[i] += 256;
	                }
	            }
	            // 生成jpeg图片
	            OutputStream out = new FileOutputStream(path);
	            out.write(bytes);
	            out.flush();
	            out.close();
	            result.setCode(Code.SUCCESS);
	            result.setMsg("转换成功");
	        } catch (Exception e) {
	        	result.setCode(Code.ERROR);
	        	result.setMsg(e.getMessage());
	        }
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("imgdate is null");
		}
		return result;
	}


	/**
	 * 本地图片转换Base64的方法
	 *
	 * @param imgPath     
	 */
	private static String ImageToBase64(String imgPath) {
		byte[] data = null;
		// 读取图片字节数组
		try {
			InputStream in = new FileInputStream(imgPath);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			z.Error("ImageToBase64出错", e);
		}
		// 对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		// 返回Base64编码过的字节数组字符串
		return encoder.encode(data);
	}

	public static String getImgZ1Path(String img_url) {
		if(img_url.indexOf("_z5.")>=0){
			return img_url.replace("_z5.", "_z1.");
		}else if(img_url.indexOf("_z1.")>=0) {
			return img_url;
		}else {
			return img_url.replace(".", "_z1.");
		}
	}

	/**
	 * 	判读文件名称是否是图片类型
	 * @param filename
	 * @return
	 */
	public static boolean isImg(String filename) {
		boolean result = false;
		if(z.isNotNull(filename)) {
			if(filename.indexOf(".jpg")>=0){
				result = true;
			}else if(filename.indexOf(".png")>=0) {
				result = true;
			}else if(filename.indexOf(".jpeg")>=0) {
				result = true;
			}else if(filename.indexOf(".bmp")>=0) {
				result = true;
			}else if(filename.indexOf(".gif")>=0) {
				result = true;
			}else if(filename.indexOf(".tiff")>=0) {
				result = true;
			}else if(filename.indexOf(".raw")>=0) {
				result = true;
			}
		}
		return result;
	}

	public static String ImgZip(String img_path) {
		String new_img_path = "";
		if(isImg(img_path)) {
			try {
				//其中的scale是可以指定图片的大小，值在0到1之间，1f就是原图大小，0.5就是原图的一半大小，这里的大小是指图片的长宽。
				//而outputQuality是图片的质量，值也是在0到1，越接近于1质量越好，越接近于0质量越差。
				String temp_img_path_50 = img_path.substring(0, img_path.lastIndexOf(".")) +"_z5." + img_path.substring(img_path.lastIndexOf(".")+1);
				Thumbnails.of(img_path).scale(0.5f).outputQuality(0.5f).toFile(temp_img_path_50);

				String temp_img_path_20 = img_path.substring(0, img_path.lastIndexOf(".")) +"_z1." + img_path.substring(img_path.lastIndexOf(".")+1);
				Thumbnails.of(img_path).scale(0.1f).outputQuality(0.5f).toFile(temp_img_path_20);

				new_img_path = temp_img_path_50;
			} catch (IOException e) {
				z.Error("压缩图片出错|原图片地址："+img_path, e);
			}
		}
		return new_img_path;

	}

	/**
	 * 解析二维码
	 * @param img
	 * @return
	 */
	public static Result AnalysisQRCode(File img) {
		Result result = new Result();
		if(z.isNotNull(img) && img.isFile()) {
			try {
				BufferedImage bufferedImage = ImageIO.read(img);
				LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
				Binarizer binarizer = new HybridBinarizer(source);
				BinaryBitmap bitmap = new BinaryBitmap(binarizer);
				HashMap<DecodeHintType, Object> decodeHints = new HashMap<DecodeHintType, Object>();
				decodeHints.put(DecodeHintType.CHARACTER_SET, "UTF-8");
				com.google.zxing.Result decode_result = new MultiFormatReader().decode(bitmap, decodeHints);
				String info = decode_result.getText();
				if(z.isNotNull(info)) {
					result.setCode(Code.SUCCESS);
					result.setData(info);
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("解析二维码出错：二维码解析后，信息为空");
				}
			} catch (Exception e) {
				result.setCode(Code.ERROR);
				result.setMsg("解析二维码出错："+StringUtil.ExceptionToString(e));
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("解析二维码出错：未找到二维码图片文件");
		}
		return result;
	}

	/**
	 * 	生成二维码【无Logo】
	 * @param content
	 * @return
	 */
	public static BufferedImage CreateQRCode(String content) {
		return CreateQRCode(content,null,QRCODE_SIZE,LOGO_SIZE);
	}

	/**
	 * 	生成二维码【有Logo】
	 * @param content
	 * @param logoUrl
	 * @return
	 */
	public static BufferedImage CreateQRCode(String content,String logoUrl) {
		return CreateQRCode(content,logoUrl,QRCODE_SIZE,LOGO_SIZE);
	}

	/**
	 * 	生成二维码
	 * @param content 生成二维码内容信息
	 * @param width 宽度
	 * @param height  高度
	 * @return BufferedImage
	 */
	public static BufferedImage CreateQRCode(String content,String logoFilePath,int QRCODE_SIZE,int LOGO_SIZE) {
		BufferedImage image = null;

		try {
			//设置二维码纠错级别ＭＡＰ
			Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
			hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
			//创建比特矩阵(位矩阵)的QR码编码的字符串  
			BitMatrix bitMatrix = new MultiFormatWriter().encode(content,BarcodeFormat.QR_CODE, QRCODE_SIZE, QRCODE_SIZE, hints);// 生成矩阵

			// 输出图像
			image = new BufferedImage(QRCODE_SIZE, QRCODE_SIZE, BufferedImage.TYPE_INT_RGB);
			for (int x = 0; x < QRCODE_SIZE; x++) {
				for (int y = 0; y < QRCODE_SIZE; y++) {
					image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
				}
			}

			//判断是否插入logo
			if(z.isNotNull(logoFilePath)) {
				insertImage(image, logoFilePath,QRCODE_SIZE,LOGO_SIZE);
			}

		} catch (Exception e) {
			z.Error("生成二维码出错", e);
		}
		return image;
	}

	/**
	 * 	生成二维码插入Logo
	 * @param image 二维码
	 * @param logoUrl LogoURL
	 * @throws Exception
	 */
	private static void insertImage(BufferedImage image, String logoFilePath,int QRCODE_SIZE,int LOGO_SIZE) throws Exception {
		File file = new File(logoFilePath);
		if(z.isNotNull(logoFilePath) && file.exists()) {
			Image src = ImageIO.read(new File(logoFilePath));

			// 插入LOGO
			Graphics2D graph = image.createGraphics();
			int x = (QRCODE_SIZE - LOGO_SIZE) / 2;
			int y = (QRCODE_SIZE - LOGO_SIZE) / 2;
			graph.drawImage(src, x, y, LOGO_SIZE, LOGO_SIZE, null);
			Shape shape = new RoundRectangle2D.Float(x, y, LOGO_SIZE, LOGO_SIZE, 6, 6);
			graph.setStroke(new BasicStroke(3f));
			graph.draw(shape);
			graph.dispose();

		}else {
			z.Error("生成二维码未找到Logo文件，无法生成带Logo的二维码。当前生成为普通的二维码。");
		}
	}

}

package com.futvan.z.framework.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 直播工具类
 * @author Administrator
 *
 */
public class LiveUtil {
	public static String domain = "merroint.com";
	
	
	/**
	 * 加密方法(可逆加密)
	 * 
	 * @param str
	 * @return
	 */
	public static String jia_old(String str) {
		String returnvalue = "";
		char[] s1 = str.toCharArray();
		String sb1 = "";
		for (int i = s1.length - 1; i >= 0; i--) {
			int srtchar = s1[i] + 3;// 加3
			sb1 = sb1 + (char) srtchar;
		}
		returnvalue = sb1;
		return returnvalue;
	}

	/**
	 * 解密方法
	 * 
	 * @param str
	 * @return
	 */
	public static String jie_old(String str) {
		String returnvalue = "";
		char[] s1 = str.toCharArray();
		String sb1 = "";
		for (int i = s1.length - 1; i >= 0; i--) {
			int srtchar = s1[i] - 3;// 减3
			sb1 = sb1 + (char) srtchar;
		}
		returnvalue = sb1;
		return returnvalue;
	}
	
	/**
	 * 生成直播安全码
	 */
	private static final char[] DIGITS_LOWER =
		{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
	public static String getSafeUrl(String key, String streamName, long txTime) {
		String input = new StringBuilder().
				append(key).
				append(streamName).
				append(Long.toHexString(txTime).toUpperCase()).toString();
		String txSecret = null;
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			txSecret  = byteArrayToHexString(
					messageDigest.digest(input.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return txSecret == null ? "" :
			new StringBuilder().
			append("txSecret=").
			append(txSecret).
			append("&").
			append("txTime=").
			append(Long.toHexString(txTime).toUpperCase()).
			toString();
	}
	private static String byteArrayToHexString(byte[] data) {
		char[] out = new char[data.length << 1];
		for (int i = 0, j = 0; i < data.length; i++) {
			out[j++] = DIGITS_LOWER[(0xF0 & data[i]) >>> 4];
			out[j++] = DIGITS_LOWER[0x0F & data[i]];
		}
		return new String(out);
	}
}

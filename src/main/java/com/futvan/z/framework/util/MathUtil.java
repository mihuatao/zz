package com.futvan.z.framework.util;

import com.futvan.z.framework.core.z;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MathUtil {
	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {
			System.out.println(FormatNumber(getRandom(1,999999999),  "H#000000000"));
		}
		
	}

	/**
	 * 计算耗时
	 * @param beginTime
	 * @return
	 */
	public static String CalculationTime(long beginTime){
		long endTime = System.currentTimeMillis();
		BigDecimal times = (new BigDecimal(endTime).subtract(new BigDecimal(beginTime))).divide(new BigDecimal(1000),4, BigDecimal.ROUND_DOWN);
		return "耗时"+times+"秒";
	}

	/**
	 * 判断字符串是否为数字
	 * @param str
	 * @return
	 */
	public static boolean isNum(String str) {
		Pattern pattern = Pattern.compile("[0-9]*");
		Matcher isNum = pattern.matcher(str);
		if (isNum.matches()) {
			return true;
		} else {
			return false;
		}
	}
	/**
	 * 数字格式化
	 * @param Number 要格式化的数字 不可为空
	 * @param decimal_place 小数点后的位数 不可为空
	 * @return
	 */
	public static String FormatNumber(Object Number,int decimal_place) {
		String result = "";
		if(z.isNotNull(decimal_place)) {
			if(decimal_place>=0) {
				if(decimal_place==0) {
					result = FormatNumber(Number,"#0");
				}else {
					StringBuffer pattern = new StringBuffer();
					pattern.append("#0.");
					for (int i = 0; i < decimal_place; i++) {
						pattern.append("#");
					}
					result = FormatNumber(Number,pattern.toString());
				}
				
			}else {
				z.Log("格式化字符串出错|小数位必须大于等于0");
			}
		}else {
			z.Log("格式化字符串出错|小数位不可为空");
		}
		return result;
	}
	
	/**
	 * 数字格式化
	 * @param Number int double long
	 * @param pattern "#0.00"  格式
	 * @return
	 */
	public static String FormatNumber(Object Number,String pattern){
		String result = "";
		if(z.isNotNull(Number)) {
			if(z.isNotNull(Number)) {
				try {
					BigDecimal n = new BigDecimal(String.valueOf(Number));
					DecimalFormat df = new DecimalFormat(pattern);
					//直接舍位，不进位
					df.setRoundingMode(RoundingMode.DOWN);
					result = df.format(n);
				} catch (Exception e) {
					z.Log("格式化出错："+Number+" 格式："+pattern);
				}
			}else {
				z.Log("格式化字符串出错|pattern格式定义不可为空");
			}
		}else {
			z.Log("格式化字符串出错|字符串不可为空");
		}
		return result;
	}

	/**
	 * 根据数据总数与每页显示数，计算总页数
	 * @param datacount
	 * @param rowcount
	 * @return
	 */
	public static String getPageCount(int datacount,int rowcount) {
		int pagecount = 0;
		if(datacount>0 && rowcount>0) {
			BigDecimal datacount_big = new BigDecimal(datacount);
			pagecount = datacount_big.divide(new BigDecimal(rowcount), BigDecimal.ROUND_UP).intValue();
		}
		return String.valueOf(pagecount);
	}

	/**
	 * 获取两个数这间的随机数
	 * @param min
	 * @param max
	 * @return
	 */
	public static int getRandom(int min,int max) {
		int i = (int)(min+Math.random()*(max-min+1));
		return i;
	}

	/**
	 * 创建数学题
	 */
	private static void CreateMathematical(int amount) {
		List<String> jgList = new ArrayList<String>();

		for (int i = 1; i <= amount; i++) {

			int typeNum = getRandom(1,2);
			int x = 0;
			int y = 0;
			int jg = 0;
			
			
			
			String itext = i+"";
			if(i<10) {
				itext = i+" ";
			}
			
			
			switch (typeNum) {
			case 1:
				//加法
				x = getRandom(1,98);
				y = getRandom(1,98-x);
				
				System.out.print(itext+": "+(x>9?x:x+" ")+" + "+(y>9?y:y+" ")+" =      ");
			    jg = x+y;
				jgList.add(itext+": "+(jg>9?jg:jg+" "));
				break;
			case 2:
				//减法
				x = getRandom(1,98);
				y = getRandom(1,x);
				
				System.out.print(itext+": "+(x>9?x:x+" ")+" - "+(y>9?y:y+" ")+" =      ");
				jg = x-y;
				jgList.add(itext+": "+(jg>9?jg:jg+" "));
				break;
			case 3:

				break;
			case 4:

				break;
			}

			
			if(i>1 && i%3==0) {
				System.out.println();
			}
			

		}
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();
		//打印结果
		for (int i = 0; i < jgList.size(); i++) {
			if(i>1 && i%10==0) {
				System.out.println();
			}
			System.out.print("["+jgList.get(i) + "] ");
			
			
		}
		

	}
}

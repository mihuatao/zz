package com.futvan.z.framework.util;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.z;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PdfUtil {

	public static void main(String[] args) throws IOException {

		PDDocument document = new PDDocument();

		PDPage page = new PDPage();

		//Retrieving the pages of the document
		PDPageContentStream contentStream = new PDPageContentStream(document, page);

		//Begin the Content stream
		contentStream.beginText();
		// contentStream.set
		//Setting the font to the Content stream
		contentStream.setFont(PDType1Font.HELVETICA, 14);

		//Setting the position for the line
		contentStream.newLineAtOffset(25, 700);

		String text = "asdfasdfasd";

		//Adding text in the form of string
		contentStream.showText(text);

		//Ending the content stream
		contentStream.endText();


		//Closing the content stream
		contentStream.close();

		document.addPage(page);

		document.save("d://999.pdf");

		document.close();


		getPdfAllImage("d://999.pdf");


	}



	/*
	 * 生成PDF（根据图片集）
	 * 
	 */
	public static Result CreatePdfForImages(List<String> imgs_path,String PdfPath) {
		Result result = new Result();
		if(z.isNotNull(imgs_path) && imgs_path.size()>0) {
			if(z.isNotNull(PdfPath)) {
				try {
					//检查图片集是否全是有效图片文件
					for (String img : imgs_path) {
						boolean b = FileUtil.isImage(img);
						if(!b) {
							result.setCode(Code.ERROR);
							result.setMsg(img+"|不是有效图片文件");
							break;
						}
					}
					//生成文件
					PDDocument document = new PDDocument();
					//添加图片
					for (String img : imgs_path) {
						//创建单页
						PDPage page = new PDPage();
						//插入单页
						document.addPage(page);
						//获取图片流
						PDImageXObject pdImage = PDImageXObject.createFromFile(img, document);
						//添加到页中
						PDPageContentStream contentStream = new PDPageContentStream(document, page);
						contentStream.drawImage(pdImage, 0, 0,pdImage.getWidth(),pdImage.getHeight());
						contentStream.close();
					}
					//保存文件
					document.save(PdfPath);
					//关闭文件
					document.close();
				} catch (IOException e) {
					result.setCode(Code.ERROR);
					result.setMsg("创建PDF出错|"+StringUtil.ExceptionToString(e));
				}  
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("PDF文件保存路径PdfPath不可为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("imgs_path图片集不可为空");
		}
		return result;
	}

	/**
	 * 获取PDF中所有图片文件
	 * @param pdffilePath
	 */
	public static Result getPdfAllImage(String pdffilePath) {
		Result result = new Result();
		PDDocument pdf = null;
		try {
			File pdffile = new File(pdffilePath);
			if(z.isNotNull(pdffile) && pdffile.isFile()) {
				pdf = PDDocument.load(pdffile);
				PDFRenderer pdfRenderer = new PDFRenderer(pdf);
				PDPageTree pageTree = pdf.getPages();
				int pageCounter = 0;
				List<String> filelist = new ArrayList<String>();
				for(PDPage page : pageTree){
					//获取图片流
					BufferedImage bim = pdfRenderer.renderImageWithDPI(pageCounter, 200);
					//转为图片文件
					String filePath = pdffile.getParent()+pageCounter+++".png";
					ImageIO.write(bim, "png", new File(filePath));
					filelist.add(filePath);
				}
				result.setCode(Code.SUCCESS);
				result.setData(filelist);
			}else {
				result.setCode(Code.ERROR);
				result.setMsg(pdffilePath+"|不是标准PDF文件");
			}
		} catch (IOException e) {
			result.setCode(Code.ERROR);
			result.setMsg(StringUtil.ExceptionToString(e));
		}
		return result;
	}

}

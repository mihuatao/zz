package com.futvan.z.httpservices.erp;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

/**
 * 获取客户拜访次数接口
 */
@Transactional
@Controller
public class GetCustomVisitNumServiceHttpService extends SuperAction {

    @RequestMapping(value = "/getCustomVisitNumService")
    public @ResponseBody
    Result Service(@RequestParam HashMap<String, String> bean) {
        Result result = new Result();
        Result authority = z.isServiceAuthority(bean, request);
        if (Code.SUCCESS.equals(authority.getCode())) {
			//获取参数手机号
            String tel = bean.get("tel");

			//封闭查询数据SQL
            StringBuffer sql = new StringBuffer();
            sql.append(" SELECT ");
            sql.append("   c.custom_name, ");
            sql.append("   MAX(c.orgname) orgname, ");
            sql.append("   MAX(c.tel) tel, ");
			sql.append("   COUNT(d.zid) visit_num, ");
			sql.append("   MAX(d.vdate) vtime  ");
            sql.append(" FROM custom_visit_c c  ");
            sql.append(" LEFT JOIN custom_visit_d d  ON c.zid = d.pid  ");
			sql.append(" where c.tel = '"+tel+"' ");
			sql.append(" GROUP BY c.custom_name  ");

			//执行查询
            List<HashMap<String, String>> list = sqlSession.selectList("select", sql);
			result = Result.success(list.get(0));
        } else {
            result = authority;
        }
        return result;
    }
}

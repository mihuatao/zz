package com.futvan.z.httpservices.system;

import com.futvan.z.erp.customer.crm_customer;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
/**
 * TestService
 */
@Transactional
@RestController
public class TestHttpService extends SuperAction{

	@RequestMapping(value="/test")
	public @ResponseBody Result Service(@RequestParam HashMap<String, String> bean) {
		Result result = new Result();
 		Result authority = z.isServiceAuthority(bean,request);
 		if(Code.SUCCESS.equals(authority.getCode())){


// 			int n1 = sqlSession.insert("insert", "INSERT INTO test1(zid)VALUES (UUID());");
//
// 			System.out.println("N1:"+n1);
//
// 			int n2 = sqlSession.insert("insert", "INSERT INTO test2(zid)VALUES (UUID());");
//
// 			System.out.println("N2:"+n2);
 		}else {
 			result = authority;
 		}
		return result;
	}
}

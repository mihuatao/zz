package com.futvan.z.job;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.BaiduUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import java.util.HashMap;
import java.util.List;

/**
 * 任务程序：清理超时未付款订单（超过2小时）
 */
public class ClearOvertimeOrderJob implements StatefulJob{
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		//超时2小时，订单状态为未支付的，变更为90已取消
		int n = z.getSqlSession().update("update","UPDATE s_order SET order_status = '90' WHERE order_status = '0' AND  NOW() >SUBDATE(create_time,INTERVAL -2 HOUR)");
		z.Log("执行任务：清理超时未付款订单|清理订单数量："+n+"单");
	}
}

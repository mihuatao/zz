package com.futvan.z.job;
import com.futvan.z.framework.core.z;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
/**
 * 任务程序：处理已收货订单变更为已完成
 */
public class Handle30OrderJob implements StatefulJob{
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		int n = z.getSqlSession().update("update","UPDATE s_order SET order_status = '100' WHERE order_status = '30'");
		z.Log("执行任务：处理已收货订单变更为已完成|处理订单数量："+n+"单");
	}
}

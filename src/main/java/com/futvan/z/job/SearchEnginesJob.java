package com.futvan.z.job;

import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.BaiduUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import java.util.HashMap;
import java.util.List;
/**
 * 舆情信息采集任务-每小时0点自动执行
 * @author 42239
 *
 */
public class SearchEnginesJob  implements StatefulJob {

	public void execute(JobExecutionContext context) throws JobExecutionException {
		//获取所有舆情采集模板
		List<HashMap<String,String>> list = z.getSqlSession().selectList("select", "select * from erp_search_engines");
		for (HashMap<String, String> se : list) {
			String keyinfo = se.get("keyinfo");
			//执行采集任务
			try {
				BaiduUtil.getList2(keyinfo,5,null,0,se.get("zid"));
			} catch (Exception e) {
				throw new JobExecutionException("舆情信息采集任务-每小时0点自动执行出错"+e.getMessage());
			}
		}
	}

}

package com.futvan.z.job;

import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.DateUtil;
import com.futvan.z.framework.util.SystemUtil;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

public class zJob implements StatefulJob{

	public void execute(JobExecutionContext context) throws JobExecutionException {
		SystemUtil.Sleep(5000);
		z.Log("自动任务|zJob|执行时间："+DateUtil.getDateTime());
	}
}

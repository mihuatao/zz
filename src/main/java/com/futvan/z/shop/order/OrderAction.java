package com.futvan.z.shop.order;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.shop.order.OrderService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class OrderAction extends SuperAction{
	@Autowired
	private OrderService orderService;

	@RequestMapping(value="/OrderSendGoods")
	public @ResponseBody
	Result Service(@RequestParam HashMap<String, String> bean) throws Exception {
		Result result = new Result();

		String zids = bean.get("zids");
			if (z.isNotNull(zids)){
				List<s_order> list = sqlSession.selectList("s_order_select_sql","select * from s_order where zid in ("+ StringUtil.ListToSqlInArray(zids)+")");
				boolean isok = true;
				String info = "";
				for (s_order o : list) {
					if(!"10".equals(o.getOrder_status())){
						isok = false;
						info = "订单状态必须为已付款";
						break;
					}
					if(z.isNull(o.getCourier_company())){
						isok = false;
						info = "快递公司不可为空";
						break;
					}
					if(z.isNull(o.getCourier_number())){
						isok = false;
						info = "快递单号不可为空";
						break;
					}
				}

				if(isok){
					//开始执行
					int n = sqlSession.update("update","update s_order set order_status = 20 where zid in ("+StringUtil.ListToSqlInArray(zids)+")");
					if(n!=list.size()){
						z.Exception("订单执行发货出错");
					}else{
						result.setSuccess();
					}
				}else{
					result.setError(info);
				}
			}else{
				result.setError("zids is null");
			}
		return result;
	}
}

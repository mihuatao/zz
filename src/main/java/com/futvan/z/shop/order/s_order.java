package com.futvan.z.shop.order;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class s_order extends SuperBean{
	//购买者ID
	private String buyer_id;

	//购买者名称
	private String buyer_name;

	//销售者ID
	private String seller_id;

	//销售者名称
	private String seller_name;

	//所属小组
	private String group_num;

	//下单人
	private String create_order_customer;

	//订单状态
	private String order_status;

	//获得PV值
	private String pv_total;

	//合计金额
	private String total;

	//产品总额
	private String product_total;

	//运费
	private String freight;

	//发货仓库
	private String start_storehouse;

	//快递公司
	private String courier_company;

	//快递单号
	private String courier_number;

	//客户地址
	private String custom_address;

	//客户完整地址
	private String custom_eaddress_complete;

	//客服电话
	private String customer_service_tel;

	//买家手机号
	private String buyer_tel;

	//付款时间
	private String pay_time;

	//发货单ID
	private String invoiceid;

	//产品明细
	private List<s_order_products> s_order_products_list;

	//日志
	private List<s_order_log> s_order_log_list;

	//支付信息
	private List<s_order_pay> s_order_pay_list;

	/**
	* get购买者ID
	* @return buyer_id
	*/
	public String getBuyer_id() {
		return buyer_id;
  	}

	/**
	* set购买者ID
	* @return buyer_id
	*/
	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
 	}

	/**
	* get购买者名称
	* @return buyer_name
	*/
	public String getBuyer_name() {
		return buyer_name;
  	}

	/**
	* set购买者名称
	* @return buyer_name
	*/
	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
 	}

	/**
	* get销售者ID
	* @return seller_id
	*/
	public String getSeller_id() {
		return seller_id;
  	}

	/**
	* set销售者ID
	* @return seller_id
	*/
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
 	}

	/**
	* get销售者名称
	* @return seller_name
	*/
	public String getSeller_name() {
		return seller_name;
  	}

	/**
	* set销售者名称
	* @return seller_name
	*/
	public void setSeller_name(String seller_name) {
		this.seller_name = seller_name;
 	}

	/**
	* get所属小组
	* @return group_num
	*/
	public String getGroup_num() {
		return group_num;
  	}

	/**
	* set所属小组
	* @return group_num
	*/
	public void setGroup_num(String group_num) {
		this.group_num = group_num;
 	}

	/**
	* get下单人
	* @return create_order_customer
	*/
	public String getCreate_order_customer() {
		return create_order_customer;
  	}

	/**
	* set下单人
	* @return create_order_customer
	*/
	public void setCreate_order_customer(String create_order_customer) {
		this.create_order_customer = create_order_customer;
 	}

	/**
	* get订单状态
	* @return order_status
	*/
	public String getOrder_status() {
		return order_status;
  	}

	/**
	* set订单状态
	* @return order_status
	*/
	public void setOrder_status(String order_status) {
		this.order_status = order_status;
 	}

	/**
	* get获得PV值
	* @return pv_total
	*/
	public String getPv_total() {
		return pv_total;
  	}

	/**
	* set获得PV值
	* @return pv_total
	*/
	public void setPv_total(String pv_total) {
		this.pv_total = pv_total;
 	}

	/**
	* get合计金额
	* @return total
	*/
	public String getTotal() {
		return total;
  	}

	/**
	* set合计金额
	* @return total
	*/
	public void setTotal(String total) {
		this.total = total;
 	}

	/**
	* get产品总额
	* @return product_total
	*/
	public String getProduct_total() {
		return product_total;
  	}

	/**
	* set产品总额
	* @return product_total
	*/
	public void setProduct_total(String product_total) {
		this.product_total = product_total;
 	}

	/**
	* get运费
	* @return freight
	*/
	public String getFreight() {
		return freight;
  	}

	/**
	* set运费
	* @return freight
	*/
	public void setFreight(String freight) {
		this.freight = freight;
 	}

	/**
	* get发货仓库
	* @return start_storehouse
	*/
	public String getStart_storehouse() {
		return start_storehouse;
  	}

	/**
	* set发货仓库
	* @return start_storehouse
	*/
	public void setStart_storehouse(String start_storehouse) {
		this.start_storehouse = start_storehouse;
 	}

	/**
	* get快递公司
	* @return courier_company
	*/
	public String getCourier_company() {
		return courier_company;
  	}

	/**
	* set快递公司
	* @return courier_company
	*/
	public void setCourier_company(String courier_company) {
		this.courier_company = courier_company;
 	}

	/**
	* get快递单号
	* @return courier_number
	*/
	public String getCourier_number() {
		return courier_number;
  	}

	/**
	* set快递单号
	* @return courier_number
	*/
	public void setCourier_number(String courier_number) {
		this.courier_number = courier_number;
 	}

	/**
	* get客户地址
	* @return custom_address
	*/
	public String getCustom_address() {
		return custom_address;
  	}

	/**
	* set客户地址
	* @return custom_address
	*/
	public void setCustom_address(String custom_address) {
		this.custom_address = custom_address;
 	}

	/**
	* get客户完整地址
	* @return custom_eaddress_complete
	*/
	public String getCustom_eaddress_complete() {
		return custom_eaddress_complete;
  	}

	/**
	* set客户完整地址
	* @return custom_eaddress_complete
	*/
	public void setCustom_eaddress_complete(String custom_eaddress_complete) {
		this.custom_eaddress_complete = custom_eaddress_complete;
 	}

	/**
	* get客服电话
	* @return customer_service_tel
	*/
	public String getCustomer_service_tel() {
		return customer_service_tel;
  	}

	/**
	* set客服电话
	* @return customer_service_tel
	*/
	public void setCustomer_service_tel(String customer_service_tel) {
		this.customer_service_tel = customer_service_tel;
 	}

	/**
	* get买家手机号
	* @return buyer_tel
	*/
	public String getBuyer_tel() {
		return buyer_tel;
  	}

	/**
	* set买家手机号
	* @return buyer_tel
	*/
	public void setBuyer_tel(String buyer_tel) {
		this.buyer_tel = buyer_tel;
 	}

	/**
	* get付款时间
	* @return pay_time
	*/
	public String getPay_time() {
		return pay_time;
  	}

	/**
	* set付款时间
	* @return pay_time
	*/
	public void setPay_time(String pay_time) {
		this.pay_time = pay_time;
 	}

	/**
	* get发货单ID
	* @return invoiceid
	*/
	public String getInvoiceid() {
		return invoiceid;
  	}

	/**
	* set发货单ID
	* @return invoiceid
	*/
	public void setInvoiceid(String invoiceid) {
		this.invoiceid = invoiceid;
 	}

	/**
	* get产品明细
	* @return 产品明细
	*/
	public List<s_order_products> getS_order_products_list() {
		return s_order_products_list;
  	}

	/**
	* set产品明细
	* @return 产品明细
	*/
	public void setS_order_products_list(List<s_order_products> s_order_products_list) {
		this.s_order_products_list = s_order_products_list;
 	}

	/**
	* get日志
	* @return 日志
	*/
	public List<s_order_log> getS_order_log_list() {
		return s_order_log_list;
  	}

	/**
	* set日志
	* @return 日志
	*/
	public void setS_order_log_list(List<s_order_log> s_order_log_list) {
		this.s_order_log_list = s_order_log_list;
 	}

	/**
	* get支付信息
	* @return 支付信息
	*/
	public List<s_order_pay> getS_order_pay_list() {
		return s_order_pay_list;
  	}

	/**
	* set支付信息
	* @return 支付信息
	*/
	public void setS_order_pay_list(List<s_order_pay> s_order_pay_list) {
		this.s_order_pay_list = s_order_pay_list;
 	}

}

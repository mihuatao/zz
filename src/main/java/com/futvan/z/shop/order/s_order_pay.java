package com.futvan.z.shop.order;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class s_order_pay extends SuperBean{
	//金额
	private String amount;

	/**
	* get金额
	* @return amount
	*/
	public String getAmount() {
		return amount;
  	}

	/**
	* set金额
	* @return amount
	*/
	public void setAmount(String amount) {
		this.amount = amount;
 	}

}

package com.futvan.z.shop.order;
import com.futvan.z.framework.core.SuperBean;
import java.util.List;
public class s_order_products extends SuperBean{
	//产品
	private String product;

	//是否赠品
	private String is_gift;

	//数量
	private String amount;

	//单价
	private String price;

	//合计金额
	private String total;

	//批次
	private String batchid;

	/**
	* get产品
	* @return product
	*/
	public String getProduct() {
		return product;
  	}

	/**
	* set产品
	* @return product
	*/
	public void setProduct(String product) {
		this.product = product;
 	}

	/**
	* get是否赠品
	* @return is_gift
	*/
	public String getIs_gift() {
		return is_gift;
  	}

	/**
	* set是否赠品
	* @return is_gift
	*/
	public void setIs_gift(String is_gift) {
		this.is_gift = is_gift;
 	}

	/**
	* get数量
	* @return amount
	*/
	public String getAmount() {
		return amount;
  	}

	/**
	* set数量
	* @return amount
	*/
	public void setAmount(String amount) {
		this.amount = amount;
 	}

	/**
	* get单价
	* @return price
	*/
	public String getPrice() {
		return price;
  	}

	/**
	* set单价
	* @return price
	*/
	public void setPrice(String price) {
		this.price = price;
 	}

	/**
	* get合计金额
	* @return total
	*/
	public String getTotal() {
		return total;
  	}

	/**
	* set合计金额
	* @return total
	*/
	public void setTotal(String total) {
		this.total = total;
 	}

	/**
	* get批次
	* @return batchid
	*/
	public String getBatchid() {
		return batchid;
  	}

	/**
	* set批次
	* @return batchid
	*/
	public void setBatchid(String batchid) {
		this.batchid = batchid;
 	}

}

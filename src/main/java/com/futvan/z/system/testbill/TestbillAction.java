package com.futvan.z.system.testbill;

import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestbillAction extends SuperAction{
	@Autowired
	private TestbillService testbillService;


	@RequestMapping(value="/testbill_test")
	public @ResponseBody Result testbill_test() throws Exception {
		return Result.success();
	}
}

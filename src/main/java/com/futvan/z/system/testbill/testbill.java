package com.futvan.z.system.testbill;

import com.futvan.z.framework.core.SuperBean;

import java.util.List;
public class testbill {
	private String zid;
	//文件字段
	private String t_text;

	//多行文本框
	private String z_textarea;

	//数字
	private String t_number;

	//文件
	private String z_file;

	//图片
	private String t_img;

	//多选框
	private String t_dx;

	//单选
	private String t_danx;

	//下拉框
	private String t_xlk;

	//Z5
	private String t_z5;

	//日期
	private String t_date;

	//日期时间
	private String t_datetime;

	//HTML
	private String t_html;

	//源码
	private String t_code;

	//测试表单明细
	private List<testbilldetail> testbilldetail_list;

	//测试表单明细2
	private List<testbilldetail2> testbilldetail2_list;

	/**
	* get文件字段
	* @return t_text
	*/
	public String getT_text() {
		return t_text;
  	}

	/**
	* set文件字段
	* @return t_text
	*/
	public void setT_text(String t_text) {
		this.t_text = t_text;
 	}

	/**
	* get多行文本框
	* @return z_textarea
	*/
	public String getZ_textarea() {
		return z_textarea;
  	}

	/**
	* set多行文本框
	* @return z_textarea
	*/
	public void setZ_textarea(String z_textarea) {
		this.z_textarea = z_textarea;
 	}

	/**
	* get数字
	* @return t_number
	*/
	public String getT_number() {
		return t_number;
  	}

	/**
	* set数字
	* @return t_number
	*/
	public void setT_number(String t_number) {
		this.t_number = t_number;
 	}

	/**
	* get文件
	* @return z_file
	*/
	public String getZ_file() {
		return z_file;
  	}

	/**
	* set文件
	* @return z_file
	*/
	public void setZ_file(String z_file) {
		this.z_file = z_file;
 	}

	/**
	* get图片
	* @return t_img
	*/
	public String getT_img() {
		return t_img;
  	}

	/**
	* set图片
	* @return t_img
	*/
	public void setT_img(String t_img) {
		this.t_img = t_img;
 	}

	/**
	* get多选框
	* @return t_dx
	*/
	public String getT_dx() {
		return t_dx;
  	}

	/**
	* set多选框
	* @return t_dx
	*/
	public void setT_dx(String t_dx) {
		this.t_dx = t_dx;
 	}

	/**
	* get单选
	* @return t_danx
	*/
	public String getT_danx() {
		return t_danx;
  	}

	/**
	* set单选
	* @return t_danx
	*/
	public void setT_danx(String t_danx) {
		this.t_danx = t_danx;
 	}

	/**
	* get下拉框
	* @return t_xlk
	*/
	public String getT_xlk() {
		return t_xlk;
  	}

	/**
	* set下拉框
	* @return t_xlk
	*/
	public void setT_xlk(String t_xlk) {
		this.t_xlk = t_xlk;
 	}

	/**
	* getZ5
	* @return t_z5
	*/
	public String getT_z5() {
		return t_z5;
  	}

	/**
	* setZ5
	* @return t_z5
	*/
	public void setT_z5(String t_z5) {
		this.t_z5 = t_z5;
 	}

	/**
	* get日期
	* @return t_date
	*/
	public String getT_date() {
		return t_date;
  	}

	/**
	* set日期
	* @return t_date
	*/
	public void setT_date(String t_date) {
		this.t_date = t_date;
 	}

	/**
	* get日期时间
	* @return t_datetime
	*/
	public String getT_datetime() {
		return t_datetime;
  	}

	/**
	* set日期时间
	* @return t_datetime
	*/
	public void setT_datetime(String t_datetime) {
		this.t_datetime = t_datetime;
 	}

	/**
	* getHTML
	* @return t_html
	*/
	public String getT_html() {
		return t_html;
  	}

	/**
	* setHTML
	* @return t_html
	*/
	public void setT_html(String t_html) {
		this.t_html = t_html;
 	}

	/**
	* get源码
	* @return t_code
	*/
	public String getT_code() {
		return t_code;
  	}

	/**
	* set源码
	* @return t_code
	*/
	public void setT_code(String t_code) {
		this.t_code = t_code;
 	}

	/**
	* get测试表单明细
	* @return 测试表单明细
	*/
	public List<testbilldetail> getTestbilldetail_list() {
		return testbilldetail_list;
  	}

	/**
	* set测试表单明细
	* @return 测试表单明细
	*/
	public void setTestbilldetail_list(List<testbilldetail> testbilldetail_list) {
		this.testbilldetail_list = testbilldetail_list;
 	}

	/**
	* get测试表单明细2
	* @return 测试表单明细2
	*/
	public List<testbilldetail2> getTestbilldetail2_list() {
		return testbilldetail2_list;
  	}

	/**
	* set测试表单明细2
	* @return 测试表单明细2
	*/
	public void setTestbilldetail2_list(List<testbilldetail2> testbilldetail2_list) {
		this.testbilldetail2_list = testbilldetail2_list;
 	}

	public String getZid() {
		return zid;
	}

	public void setZid(String zid) {
		this.zid = zid;
	}
}

package com.futvan.z.system.testbill;

import com.futvan.z.framework.core.SuperBean;
public class testbilldetail extends SuperBean{
	//文本
	private String td_text;

	//多行文本
	private String td_texttextarea;

	//数字
	private String td_number;

	//文件
	private String td_file;

	//图片
	private String td_img;

	//多选
	private String td_dx;

	//单选
	private String td_danx;

	//下拉框
	private String td_xlk;

	//Z5
	private String td_z5;

	//日期
	private String td_date;

	//日期时间
	private String td_datetime;

	//HTML
	private String td_html;

	//源码
	private String td_code;

	/**
	* get文本
	* @return td_text
	*/
	public String getTd_text() {
		return td_text;
  	}

	/**
	* set文本
	* @return td_text
	*/
	public void setTd_text(String td_text) {
		this.td_text = td_text;
 	}

	/**
	* get多行文本
	* @return td_texttextarea
	*/
	public String getTd_texttextarea() {
		return td_texttextarea;
  	}

	/**
	* set多行文本
	* @return td_texttextarea
	*/
	public void setTd_texttextarea(String td_texttextarea) {
		this.td_texttextarea = td_texttextarea;
 	}

	/**
	* get数字
	* @return td_number
	*/
	public String getTd_number() {
		return td_number;
  	}

	/**
	* set数字
	* @return td_number
	*/
	public void setTd_number(String td_number) {
		this.td_number = td_number;
 	}

	/**
	* get文件
	* @return td_file
	*/
	public String getTd_file() {
		return td_file;
  	}

	/**
	* set文件
	* @return td_file
	*/
	public void setTd_file(String td_file) {
		this.td_file = td_file;
 	}

	/**
	* get图片
	* @return td_img
	*/
	public String getTd_img() {
		return td_img;
  	}

	/**
	* set图片
	* @return td_img
	*/
	public void setTd_img(String td_img) {
		this.td_img = td_img;
 	}

	/**
	* get多选
	* @return td_dx
	*/
	public String getTd_dx() {
		return td_dx;
  	}

	/**
	* set多选
	* @return td_dx
	*/
	public void setTd_dx(String td_dx) {
		this.td_dx = td_dx;
 	}

	/**
	* get单选
	* @return td_danx
	*/
	public String getTd_danx() {
		return td_danx;
  	}

	/**
	* set单选
	* @return td_danx
	*/
	public void setTd_danx(String td_danx) {
		this.td_danx = td_danx;
 	}

	/**
	* get下拉框
	* @return td_xlk
	*/
	public String getTd_xlk() {
		return td_xlk;
  	}

	/**
	* set下拉框
	* @return td_xlk
	*/
	public void setTd_xlk(String td_xlk) {
		this.td_xlk = td_xlk;
 	}

	/**
	* getZ5
	* @return td_z5
	*/
	public String getTd_z5() {
		return td_z5;
  	}

	/**
	* setZ5
	* @return td_z5
	*/
	public void setTd_z5(String td_z5) {
		this.td_z5 = td_z5;
 	}

	/**
	* get日期
	* @return td_date
	*/
	public String getTd_date() {
		return td_date;
  	}

	/**
	* set日期
	* @return td_date
	*/
	public void setTd_date(String td_date) {
		this.td_date = td_date;
 	}

	/**
	* get日期时间
	* @return td_datetime
	*/
	public String getTd_datetime() {
		return td_datetime;
  	}

	/**
	* set日期时间
	* @return td_datetime
	*/
	public void setTd_datetime(String td_datetime) {
		this.td_datetime = td_datetime;
 	}

	/**
	* getHTML
	* @return td_html
	*/
	public String getTd_html() {
		return td_html;
  	}

	/**
	* setHTML
	* @return td_html
	*/
	public void setTd_html(String td_html) {
		this.td_html = td_html;
 	}

	/**
	* get源码
	* @return td_code
	*/
	public String getTd_code() {
		return td_code;
  	}

	/**
	* set源码
	* @return td_code
	*/
	public void setTd_code(String td_code) {
		this.td_code = td_code;
 	}

}

package com.futvan.z.system.testbill;

import com.futvan.z.framework.core.SuperBean;
public class testbilldetail2 extends SuperBean{
	//文本
	private String td2_text;

	//数字
	private String td2_number;

	/**
	* get文本
	* @return td2_text
	*/
	public String getTd2_text() {
		return td2_text;
  	}

	/**
	* set文本
	* @return td2_text
	*/
	public void setTd2_text(String td2_text) {
		this.td2_text = td2_text;
 	}

	/**
	* get数字
	* @return td2_number
	*/
	public String getTd2_number() {
		return td2_number;
  	}

	/**
	* set数字
	* @return td2_number
	*/
	public void setTd2_number(String td2_number) {
		this.td2_number = td2_number;
 	}

}

package com.futvan.z.system.zarea;

import java.util.List;
public class z_area_simple{
	//行政编号
	private String zid;

	//名称
	private String area_name;
	
	private List<z_area_simple> detail_list;//明细列表

	public String getZid() {
		return zid;
	}

	public void setZid(String zid) {
		this.zid = zid;
	}

	public String getArea_name() {
		return area_name;
	}

	public void setArea_name(String area_name) {
		this.area_name = area_name;
	}

	public List<z_area_simple> getDetail_list() {
		return detail_list;
	}

	public void setDetail_list(List<z_area_simple> detail_list) {
		this.detail_list = detail_list;
	}


}

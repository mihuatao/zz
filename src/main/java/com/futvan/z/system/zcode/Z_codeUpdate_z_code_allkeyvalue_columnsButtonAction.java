package com.futvan.z.system.zcode;

import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
@Controller
public class Z_codeUpdate_z_code_allkeyvalue_columnsButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;
	
	@Autowired
	private ZcodeService zcodeService;

	@RequestMapping(value="/update_z_code_allkeyvalue_columns")
	public @ResponseBody Result update_z_code_allkeyvalue_columns(@RequestParam HashMap<String,String> bean) throws Exception {
		return zcodeService.update_z_code_allkeyvalue_columns();
	}

	
}

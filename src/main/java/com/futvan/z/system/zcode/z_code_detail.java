package com.futvan.z.system.zcode;

import com.futvan.z.framework.core.SuperBean;
public class z_code_detail extends SuperBean{
	//存储值
	private String z_key;

	//显示值
	private String z_value;

	//显示颜色
	private String display_color;

	/**
	* get存储值
	* @return z_key
	*/
	public String getZ_key() {
		return z_key;
  	}

	/**
	* set存储值
	* @return z_key
	*/
	public void setZ_key(String z_key) {
		this.z_key = z_key;
 	}

	/**
	* get显示值
	* @return z_value
	*/
	public String getZ_value() {
		return z_value;
  	}

	/**
	* set显示值
	* @return z_value
	*/
	public void setZ_value(String z_value) {
		this.z_value = z_value;
 	}

	/**
	* get显示颜色
	* @return display_color
	*/
	public String getDisplay_color() {
		return display_color;
  	}

	/**
	* set显示颜色
	* @return display_color
	*/
	public void setDisplay_color(String display_color) {
		this.display_color = display_color;
 	}

}

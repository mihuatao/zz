package com.futvan.z.system.zcontroller;

import com.futvan.z.framework.core.SuperBean;

import java.util.List;
public class z_controller extends SuperBean{
	//方法名称
	private String controller;

	//方法类型
	private String controller_type;

	//返回值类型
	private String return_type;

	//是否监控
	private String is_monitor;

	//参数描述
	private List<z_controller_parameter> z_controller_parameter_list;

	/**
	* get方法名称
	* @return controller
	*/
	public String getController() {
		return controller;
  	}

	/**
	* set方法名称
	* @return controller
	*/
	public void setController(String controller) {
		this.controller = controller;
 	}

	/**
	* get方法类型
	* @return controller_type
	*/
	public String getController_type() {
		return controller_type;
  	}

	/**
	* set方法类型
	* @return controller_type
	*/
	public void setController_type(String controller_type) {
		this.controller_type = controller_type;
 	}

	/**
	* get返回值类型
	* @return return_type
	*/
	public String getReturn_type() {
		return return_type;
  	}

	/**
	* set返回值类型
	* @return return_type
	*/
	public void setReturn_type(String return_type) {
		this.return_type = return_type;
 	}

	/**
	* get是否监控
	* @return is_monitor
	*/
	public String getIs_monitor() {
		return is_monitor;
  	}

	/**
	* set是否监控
	* @return is_monitor
	*/
	public void setIs_monitor(String is_monitor) {
		this.is_monitor = is_monitor;
 	}

	/**
	* get参数描述
	* @return 参数描述
	*/
	public List<z_controller_parameter> getZ_controller_parameter_list() {
		return z_controller_parameter_list;
  	}

	/**
	* set参数描述
	* @return 参数描述
	*/
	public void setZ_controller_parameter_list(List<z_controller_parameter> z_controller_parameter_list) {
		this.z_controller_parameter_list = z_controller_parameter_list;
 	}

}

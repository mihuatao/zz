package com.futvan.z.system.zcontroller;

import com.futvan.z.framework.core.SuperBean;
public class z_controller_parameter extends SuperBean{
	//参数类型
	private String parameter_types;

	//参数
	private String parameter;

	/**
	* get参数类型
	* @return parameter_types
	*/
	public String getParameter_types() {
		return parameter_types;
  	}

	/**
	* set参数类型
	* @return parameter_types
	*/
	public void setParameter_types(String parameter_types) {
		this.parameter_types = parameter_types;
 	}

	/**
	* get参数
	* @return parameter
	*/
	public String getParameter() {
		return parameter;
  	}

	/**
	* set参数
	* @return parameter
	*/
	public void setParameter(String parameter) {
		this.parameter = parameter;
 	}

}

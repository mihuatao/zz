package com.futvan.z.system.zdb;

import com.futvan.z.framework.core.SuperBean;
public class z_db_table_column extends SuperBean{
	//字段ID
	private String column_id;

	//字段名称
	private String column_name;

	//字段类型
	private String column_type;

	//字段长度
	private String column_length;

	//是否非空
	private String is_null;

	/**
	* get字段ID
	* @return column_id
	*/
	public String getColumn_id() {
		return column_id;
  	}

	/**
	* set字段ID
	* @return column_id
	*/
	public void setColumn_id(String column_id) {
		this.column_id = column_id;
 	}

	/**
	* get字段名称
	* @return column_name
	*/
	public String getColumn_name() {
		return column_name;
  	}

	/**
	* set字段名称
	* @return column_name
	*/
	public void setColumn_name(String column_name) {
		this.column_name = column_name;
 	}

	/**
	* get字段类型
	* @return column_type
	*/
	public String getColumn_type() {
		return column_type;
  	}

	/**
	* set字段类型
	* @return column_type
	*/
	public void setColumn_type(String column_type) {
		this.column_type = column_type;
 	}

	/**
	* get字段长度
	* @return column_length
	*/
	public String getColumn_length() {
		return column_length;
  	}

	/**
	* set字段长度
	* @return column_length
	*/
	public void setColumn_length(String column_length) {
		this.column_length = column_length;
 	}

	/**
	* get是否非空
	* @return is_null
	*/
	public String getIs_null() {
		return is_null;
  	}

	/**
	* set是否非空
	* @return is_null
	*/
	public void setIs_null(String is_null) {
		this.is_null = is_null;
 	}

}

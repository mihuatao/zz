package com.futvan.z.system.zetlin;

import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.core.SuperAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class ZetlinAction extends SuperAction{
	@Autowired
	private ZetlinService zetlinService;
	
	/**
	 * 根据表名获取表ZID
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/getZDBTableZidForTelZid")
	public @ResponseBody Result z_db_save_and_add_button(@RequestParam String zid) throws Exception {
		return zetlinService.getZDBTableZidForTelZid(zid);
	}
}

package com.futvan.z.system.zhttpservices;
import java.util.HashMap;
import java.util.List;

import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
@Controller
public class Z_http_servicesCopy_httpserviceButtonAction extends SuperAction{

	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/copy_httpservice")
	public @ResponseBody Result copy_httpservice(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();


		String zids = bean.get("zids");

		if(z.isNotNull(zids)){
			String[] zids_array = zids.split(",");
			for (String zid : zids_array) {
				z_http_services s = z.httpservices_zid.get(zid);

				//生成新的zid
				String newzid = z.newZid("z_http_services");

				//保存参数
				List<z_http_services_parameter> splist = s.getZ_http_services_parameter_list();
				for (z_http_services_parameter sp : splist) {
					sp.setZid(z.newZid("z_http_services_parameter"));
					sp.setPid(newzid);
					sqlSession.insert("z_http_services_parameter_insert",sp);
				}

				//保存主表
				s.setZid(newzid);
				String old_serviceid = s.getServiceid();
				s.setServiceid(old_serviceid+"_"+z.newZcode());
				sqlSession.insert("z_http_services_insert",s);
			}

			result.setSuccess(zids_array.length);
		}else{
			result.setError("zids is null");
		}

		return result;
	}
}

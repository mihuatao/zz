package com.futvan.z.system.zjob;

import com.futvan.z.framework.core.SuperBean;

import java.util.List;
public class z_job extends SuperBean{
	//任务类型
	private String jobtype;

	//任务
	private String jobname;

	//是否启动
	private String isstart;

	//执行时间
	private String jobtime;

	//任务执行文件
	private String jobclass;

	//数据抽取计划
	private String etlid;

	//最后执行时间
	private String last_time;

	//执行记录
	private List<z_job_run> z_job_run_list;

	/**
	* get任务类型
	* @return jobtype
	*/
	public String getJobtype() {
		return jobtype;
  	}

	/**
	* set任务类型
	* @return jobtype
	*/
	public void setJobtype(String jobtype) {
		this.jobtype = jobtype;
 	}

	/**
	* get任务
	* @return jobname
	*/
	public String getJobname() {
		return jobname;
  	}

	/**
	* set任务
	* @return jobname
	*/
	public void setJobname(String jobname) {
		this.jobname = jobname;
 	}

	/**
	* get是否启动
	* @return isstart
	*/
	public String getIsstart() {
		return isstart;
  	}

	/**
	* set是否启动
	* @return isstart
	*/
	public void setIsstart(String isstart) {
		this.isstart = isstart;
 	}

	/**
	* get执行时间
	* @return jobtime
	*/
	public String getJobtime() {
		return jobtime;
  	}

	/**
	* set执行时间
	* @return jobtime
	*/
	public void setJobtime(String jobtime) {
		this.jobtime = jobtime;
 	}

	/**
	* get任务执行文件
	* @return jobclass
	*/
	public String getJobclass() {
		return jobclass;
  	}

	/**
	* set任务执行文件
	* @return jobclass
	*/
	public void setJobclass(String jobclass) {
		this.jobclass = jobclass;
 	}

	/**
	* get数据抽取计划
	* @return etlid
	*/
	public String getEtlid() {
		return etlid;
  	}

	/**
	* set数据抽取计划
	* @return etlid
	*/
	public void setEtlid(String etlid) {
		this.etlid = etlid;
 	}

	/**
	* get最后执行时间
	* @return last_time
	*/
	public String getLast_time() {
		return last_time;
  	}

	/**
	* set最后执行时间
	* @return last_time
	*/
	public void setLast_time(String last_time) {
		this.last_time = last_time;
 	}

	/**
	* get执行记录
	* @return 执行记录
	*/
	public List<z_job_run> getZ_job_run_list() {
		return z_job_run_list;
  	}

	/**
	* set执行记录
	* @return 执行记录
	*/
	public void setZ_job_run_list(List<z_job_run> z_job_run_list) {
		this.z_job_run_list = z_job_run_list;
 	}

}

package com.futvan.z.system.zrole;

import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
@Service
public class ZroleService extends SuperService{

	public List<Map<String,String>> getUserList(String roleid) throws Exception{
		String sql = "SELECT uc.* FROM z_user uc LEFT JOIN z_role_user ud ON uc.zid = ud.userid WHERE (ud.pid != '"+roleid+"' OR ud.pid IS NULL) and uc.is_start = 1 ";
		List<Map<String,String>> userList = selectList(sql);
		return userList;
	}

	public void ImportUser(String roleid, String userIds) throws Exception{
		String [] userArray = userIds.split(",");
		for (int i = 0; i < userArray.length; i++) {
			//导入用户到组织下面
			String sql = "INSERT INTO z_role_user(zid,pid,userid) VALUES ('"+z.newZid("z_role_user")+"','"+roleid+"','"+userArray[i]+"')";
			insert(sql);
		}
	}

	public List<Map<String,String>> getMenuList(String roleid) {
		StringBuffer sql = new StringBuffer();
		sql.append(" SELECT DISTINCT  ");
		sql.append("  uc.zid, ");
		sql.append("  uc.seq,");
		sql.append("  uc.parentid,");
		sql.append("  uc.name,");
		sql.append("  uc.url,");
		sql.append("  'true' open,");
		sql.append("  uc.formid,");
		sql.append("  IF(ud.pid = '"+roleid+"', 'true' ,'false' ) checked ");
		sql.append("  FROM z_menu uc ");
		sql.append("  LEFT JOIN z_role_menu ud  ON uc.zid = ud.menuid  AND ud.pid = '"+roleid+"'");
		List<Map<String,String>> menuList = selectList(sql.toString());
		return menuList;
	}

	public void ImportMenu(String roleid, String menuIds) {
		//删除现在菜单
		String deletesql = "delete from z_role_menu where pid ='"+roleid+"'";
		delete(deletesql);
		
		//保存新的菜单
		String [] menuArray = menuIds.split(",");
		for (int i = 0; i < menuArray.length; i++) {
			//导入用户到组织下面
			String sql = "INSERT INTO z_role_menu(zid,pid,menuid) VALUES ('"+z.newZid("z_role_menu")+"','"+roleid+"','"+menuArray[i]+"')";
			insert(sql);
		}
	}
}

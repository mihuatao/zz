package com.futvan.z.system.zsequence;

import com.futvan.z.framework.core.SuperBean;
public class z_sequence extends SuperBean{
	//前缀
	private String prefix;

	//最大长度
	private String max_with;

	//当前数
	private String now_with;

	//步长
	private String increment;

	//数值格式
	private String pattern;

	/**
	* get前缀
	* @return prefix
	*/
	public String getPrefix() {
		return prefix;
  	}

	/**
	* set前缀
	* @return prefix
	*/
	public void setPrefix(String prefix) {
		this.prefix = prefix;
 	}

	/**
	* get最大长度
	* @return max_with
	*/
	public String getMax_with() {
		return max_with;
  	}

	/**
	* set最大长度
	* @return max_with
	*/
	public void setMax_with(String max_with) {
		this.max_with = max_with;
 	}

	/**
	* get当前数
	* @return now_with
	*/
	public String getNow_with() {
		return now_with;
  	}

	/**
	* set当前数
	* @return now_with
	*/
	public void setNow_with(String now_with) {
		this.now_with = now_with;
 	}

	/**
	* get步长
	* @return increment
	*/
	public String getIncrement() {
		return increment;
  	}

	/**
	* set步长
	* @return increment
	*/
	public void setIncrement(String increment) {
		this.increment = increment;
 	}

	/**
	* get数值格式
	* @return pattern
	*/
	public String getPattern() {
		return pattern;
  	}

	/**
	* set数值格式
	* @return pattern
	*/
	public void setPattern(String pattern) {
		this.pattern = pattern;
 	}

}

package com.futvan.z.system.zuser;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import com.futvan.z.framework.util.StringUtil;
import com.futvan.z.system.zusersp.z_user_sp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
@Controller
public class ZuserAction extends SuperAction{
	@Autowired
	private ZuserService zuserService;

	@Autowired
	private CommonService commonService;
	
	/**
	 * 锁屏
	 * @param bean
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/user_locking")
	public @ResponseBody Result user_locking(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		String zid = bean.get("zid");
		if(z.isNotNull(zid)) {
			int num = sqlSession.update("update", "update z_user_sp set spvalue = '1' where uid = '"+zid+"' and spid = 'isLock'");
			if(num==1) {
				z_user_sp usp = sqlSession.selectOne("z_user_sp_select_sql", "select * from z_user_sp where uid = '"+zid+"' and spid = 'isLock' and spvalue = '1'");
				if(z.isNotNull(usp)) {
					z.users_sp.put(zid+usp.getSpid(), usp);
					result.setCode(Code.SUCCESS);
					result.setMsg("锁屏成功");
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("获取用户锁屏状态信息异常");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("修改用户锁屏状态信息异常");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("zid is null");
		}
		return result;
	}
	
	
	/**
	 * 解锁
	 * @param bean zid:用户zid    password:当前输入的密码
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/user_unlock")
	public @ResponseBody Result user_unlock(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		String zid = bean.get("zid");
		if(z.isNotNull(zid)) {
			z_user u = z.users.get(zid);
			if(z.isNotNull(u)) {
				if(z.isNotNull(u.getLogin_password())) {
					String password = bean.get("password");
					if(z.isNotNull(password)) {
						String jm_password = StringUtil.CreatePassword(password);
						if(jm_password.equals(u.getLogin_password())) {
							int num = sqlSession.update("update", "update z_user_sp set spvalue = '0' where uid = '"+zid+"' and spid = 'isLock'");
							if(num==1) {
								z_user_sp usp = sqlSession.selectOne("z_user_sp_select_sql", "select * from z_user_sp where uid = '"+zid+"' and spid = 'isLock' and spvalue = '0'");
								if(z.isNotNull(usp)) {
									z.users_sp.put(zid+usp.getSpid(), usp);
									result.setCode(Code.SUCCESS);
									result.setMsg("解锁成功");
								}else {
									result.setCode(Code.ERROR);
									result.setMsg("获取用户锁屏状态信息异常");
								}
							}else {
								result.setCode(Code.ERROR);
								result.setMsg("修改用户锁屏状态信息异常");
							}
							
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("密码错误");
						}
					}else {
						result.setCode(Code.ERROR);
						result.setMsg("登录密码不能为空");
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("用户未定义登录密码");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("根据ZID未找到用户信息");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("zid is null");
		}
		return result;
	}
	
	
	@RequestMapping(value="/UserDelete")
	public @ResponseBody Result UserDelete(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();
		try {
			String zids = bean.get("zids");

			if(z.isNotNull(zids)) {
				String [] zids_array = zids.split(",");
				for (int i = 0; i < zids_array.length; i++) {
					String zid = zids_array[i];
					//删除组织
					commonService.DeleteForSQL("DELETE FROM z_org_user WHERE userid = '"+zid+"'");
					
					//删除角色
					commonService.DeleteForSQL("DELETE FROM z_role_user WHERE userid = '"+zid+"'");
				}
			}
			
			//删除用户
			result = commonService.delete(bean,request);
		} catch (Exception e) {
			result.setCode(Code.ERROR);
			result.setMsg("删除用户失败");
			result.setData(StringUtil.ExceptionToString(e));
		}
		return result;
	}

	@RequestMapping(value="/UserInsert")
	public @ResponseBody Result UserInsert(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();

		if(z.isNull(bean.get("zid"))) {
			bean.put("zid",z.newZid("z_user"));
		}

		//生成密码
		String password = StringUtil.CreatePassword("123456");
		bean.put("login_password", password);

		//判读是否选择组织
		String user_orgid = bean.get("user_orgid");
		if(z.isNotNull(user_orgid)) {
			//判读是否选择角色
			String user_role = bean.get("user_role");
			if(z.isNotNull(user_role)) {
				//插入用户
				Result ur = commonService.insert(bean,request);
				if(Code.SUCCESS.equals(ur.getCode())) {
					//插入用户所属组织
					HashMap<String,String> userOrgbean = new HashMap<String, String>();
					userOrgbean.put("tableId", "z_org_user");
					userOrgbean.put("zid", z.newZid("z_org_user"));
					userOrgbean.put("pid", user_orgid);
					userOrgbean.put("userid", bean.get("zid"));
					Result uor = commonService.insert(userOrgbean,request);
					if(Code.SUCCESS.equals(uor.getCode())) {
						//插入角色
						HashMap<String,String> userRolebean = new HashMap<String, String>();
						userRolebean.put("tableId", "z_role_user");
						userRolebean.put("zid", z.newZid("z_role_user"));
						userRolebean.put("pid", user_role);
						userRolebean.put("userid", bean.get("zid"));
						Result urr = commonService.insert(userRolebean,request);
						if(Code.SUCCESS.equals(uor.getCode())) {
							result.setCode(Code.SUCCESS);
							result.setMsg("新增用户成功");
							
							//重新加载系统参数
							RLoadParameter();
						}else {
							result = urr;
						}
					}else {
						result = uor;
					}
				}else {
					result = ur;
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("所属组织不可为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("所属组织不可为空");
		}
		return result;
	}

	@RequestMapping(value="/UserUpdate")
	public @ResponseBody Result UserUpdate(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();

		z_user old_user = commonService.selectBean("z_user_select_zid", bean.get("zid"));
		
		//删除旧所属组织
		if(z.isNotNull(old_user.getUser_orgid())) {
			commonService.DeleteForSQL("DELETE FROM z_org_user WHERE userid = '"+old_user.getZid()+"' AND pid = '"+old_user.getUser_orgid()+"'");
		}
		
		//删除旧的所属角色
		if(z.isNotNull(old_user.getUser_role())) {
			commonService.DeleteForSQL("DELETE FROM z_role_user WHERE userid = '"+old_user.getZid()+"' AND pid = '"+old_user.getUser_role()+"'");
		}

		//判读是否选择组织
		String user_orgid = bean.get("user_orgid");
		if(z.isNotNull(user_orgid)) {
			//判读是否选择角色
			String user_role = bean.get("user_role");
			if(z.isNotNull(user_role)) {
				//修改用户
				Result ur = commonService.update(bean,request);
				if(Code.SUCCESS.equals(ur.getCode())) {
					//插入用户所属组织
					HashMap<String,String> userOrgbean = new HashMap<String, String>();
					userOrgbean.put("tableId", "z_org_user");
					userOrgbean.put("zid", z.newZid("z_org_user"));
					userOrgbean.put("pid", user_orgid);
					userOrgbean.put("userid", bean.get("zid"));
					Result uor = commonService.insert(userOrgbean,request);
					if(Code.SUCCESS.equals(uor.getCode())) {
						//插入角色
						HashMap<String,String> userRolebean = new HashMap<String, String>();
						userRolebean.put("tableId", "z_role_user");
						userRolebean.put("zid", z.newZid("z_role_user"));
						userRolebean.put("pid", user_role);
						userRolebean.put("userid", bean.get("zid"));
						Result urr = commonService.insert(userRolebean,request);
						if(Code.SUCCESS.equals(uor.getCode())) {
							result.setCode(Code.SUCCESS);
							result.setMsg("修改用户成功");
							
							//重新加载系统参数
							RLoadParameter();
						}else {
							result = urr;
						}
					}else {
						result = uor;
					}
				}else {
					result = ur;
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("所属组织不可为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("所属组织不可为空");
		}

		return result;
	}

	@RequestMapping(value="/user_settings")
	public ModelAndView user_settings(@RequestParam HashMap<String,String> bean) throws Exception {
		ModelAndView model = new ModelAndView("system/zuser/user_settings");
		//更新系统缓存用户
		initUser();
		
		//获取用户
		z_user user = GetSessionUser(request);
		if(z.isNull(user)) {
			String userid = GetSessionUserId(request);
			user = z.users.get(userid);
			
		}
		request.getSession().setAttribute("zuser", user);
		model.addObject("user", user);
		if(isMobile()) {
			model.addObject("is_mobile", "1");
		}
		return model;
	}

	@RequestMapping(value="/user_settings_password")
	public ModelAndView user_settings_password() throws Exception {
		ModelAndView model = new ModelAndView("system/zuser/user_settings_password");
		if(isMobile()) {
			model.addObject("is_mobile", "1");
		}
		return model;
	}

	@RequestMapping(value="/SaveUserPassword")
	public @ResponseBody Result SaveUserPassword(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = new Result();

		String login_password = bean.get("login_password");
		String new_login_password1 = bean.get("new_login_password1");
		String new_login_password2 = bean.get("new_login_password2");

		if(z.isNotNull(login_password)) {
			if(z.isNotNull(new_login_password1)) {
				if(z.isNotNull(new_login_password2)) {
					if(new_login_password1.equals(new_login_password2)) {
						if(z.isNotNull(request.getSession().getAttribute("zuser")) &&  request.getSession().getAttribute("zuser") instanceof z_user) {
							//获取原用户信息
							z_user user = (z_user) request.getSession().getAttribute("zuser");
							String db_login_password = user.getLogin_password();
							if(db_login_password.equals(StringUtil.CreatePassword(login_password))) {
								//保存新密码
								zuserService.updateLoginPassword(user.getZid(),StringUtil.CreatePassword(new_login_password1));

								//更新Session中的用户登录密码
								user.setLogin_password(StringUtil.CreatePassword(new_login_password1));
								request.getSession().setAttribute("zuser", user);

								//设置返回结果
								result.setCode(Code.SUCCESS);
								result.setMsg("密码修改成功");

							}else {
								result.setCode(Code.ERROR);
								result.setMsg("原密码输入错误，请重新输入。");
							}
						}else {
							result.setCode(Code.ERROR);
							result.setMsg("获取用户信息出错，请退出后重新登录操作。");
						}

					}else {
						result.setCode(Code.ERROR);
						result.setMsg("新密码两次输入不相同，请重新输入新密码。");
					}
				}else {
					result.setCode(Code.ERROR);
					result.setMsg("新密码确认不能为空");
				}
			}else {
				result.setCode(Code.ERROR);
				result.setMsg("新密码不能为空");
			}
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("原密码不能为空");
		}

		return result;
	}


}

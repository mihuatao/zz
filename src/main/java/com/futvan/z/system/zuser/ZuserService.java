package com.futvan.z.system.zuser;

import com.futvan.z.framework.core.SuperService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
@Service
public class ZuserService extends SuperService{

	/**
	 * 修改登录密码
	 * @param zid
	 * @param new_login_password1
	 * @throws Exception 
	 */
	public void updateLoginPassword(String zid, String login_password) throws Exception {
		HashMap<String,String> bean = new HashMap<String, String>();
		bean.put("tableId", "z_user");
		bean.put("zid",zid);
		bean.put("login_password", login_password);
		update(bean, null);
	}

}

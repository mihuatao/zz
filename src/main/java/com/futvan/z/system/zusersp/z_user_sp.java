package com.futvan.z.system.zusersp;

import com.futvan.z.framework.core.SuperBean;
public class z_user_sp extends SuperBean{
	//用户
	private String uid;

	//参数标识
	private String spid;

	//参数名称
	private String spname;

	//参数内容
	private String spvalue;

	/**
	* get用户
	* @return uid
	*/
	public String getUid() {
		return uid;
  	}

	/**
	* set用户
	* @return uid
	*/
	public void setUid(String uid) {
		this.uid = uid;
 	}

	/**
	* get参数标识
	* @return spid
	*/
	public String getSpid() {
		return spid;
  	}

	/**
	* set参数标识
	* @return spid
	*/
	public void setSpid(String spid) {
		this.spid = spid;
 	}

	/**
	* get参数名称
	* @return spname
	*/
	public String getSpname() {
		return spname;
  	}

	/**
	* set参数名称
	* @return spname
	*/
	public void setSpname(String spname) {
		this.spname = spname;
 	}

	/**
	* get参数内容
	* @return spvalue
	*/
	public String getSpvalue() {
		return spvalue;
  	}

	/**
	* set参数内容
	* @return spvalue
	*/
	public void setSpvalue(String spvalue) {
		this.spvalue = spvalue;
 	}

}

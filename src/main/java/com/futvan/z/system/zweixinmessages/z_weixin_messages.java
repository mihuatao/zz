package com.futvan.z.system.zweixinmessages;

import com.futvan.z.framework.core.SuperBean;
public class z_weixin_messages extends SuperBean{
	//接收者
	private String ToUserName;

	//发送者
	private String FromUserName;

	//发送时间
	private String CreateTime;

	//消息类型
	private String MsgType;

	//信息内容
	private String Content;

	//消息ID
	private String MsgId;

	//事件
	private String Event;

	//消息个数
	private String ArticleCount;

	//消息媒体id
	private String MediaId;

	//图片链接
	private String PicUrl;

	//格式
	private String Format;

	//语音识别结果
	private String Recognition;

	//缩略图的媒体id
	private String ThumbMediaId;

	//地理位置纬度
	private String Location_X;

	//地理位置经度
	private String Location_Y;

	//地理位置纬度
	private String Latitude;

	//地理位置经度
	private String Longitude;

	//地图缩放大小
	private String Scale;

	//地理位置信息
	private String Label;

	//消息标题
	private String Title;

	//消息描述
	private String Description;

	//消息链接
	private String Url;

	//事件KEY值
	private String EventKey;

	//二维码的ticket
	private String Ticket;

	/**
	* get接收者
	* @return ToUserName
	*/
	public String getToUserName() {
		return ToUserName;
  	}

	/**
	* set接收者
	* @return ToUserName
	*/
	public void setToUserName(String ToUserName) {
		this.ToUserName = ToUserName;
 	}

	/**
	* get发送者
	* @return FromUserName
	*/
	public String getFromUserName() {
		return FromUserName;
  	}

	/**
	* set发送者
	* @return FromUserName
	*/
	public void setFromUserName(String FromUserName) {
		this.FromUserName = FromUserName;
 	}

	/**
	* get发送时间
	* @return CreateTime
	*/
	public String getCreateTime() {
		return CreateTime;
  	}

	/**
	* set发送时间
	* @return CreateTime
	*/
	public void setCreateTime(String CreateTime) {
		this.CreateTime = CreateTime;
 	}

	/**
	* get消息类型
	* @return MsgType
	*/
	public String getMsgType() {
		return MsgType;
  	}

	/**
	* set消息类型
	* @return MsgType
	*/
	public void setMsgType(String MsgType) {
		this.MsgType = MsgType;
 	}

	/**
	* get信息内容
	* @return Content
	*/
	public String getContent() {
		return Content;
  	}

	/**
	* set信息内容
	* @return Content
	*/
	public void setContent(String Content) {
		this.Content = Content;
 	}

	/**
	* get消息ID
	* @return MsgId
	*/
	public String getMsgId() {
		return MsgId;
  	}

	/**
	* set消息ID
	* @return MsgId
	*/
	public void setMsgId(String MsgId) {
		this.MsgId = MsgId;
 	}

	/**
	* get事件
	* @return Event
	*/
	public String getEvent() {
		return Event;
  	}

	/**
	* set事件
	* @return Event
	*/
	public void setEvent(String Event) {
		this.Event = Event;
 	}

	/**
	* get消息个数
	* @return ArticleCount
	*/
	public String getArticleCount() {
		return ArticleCount;
  	}

	/**
	* set消息个数
	* @return ArticleCount
	*/
	public void setArticleCount(String ArticleCount) {
		this.ArticleCount = ArticleCount;
 	}

	/**
	* get消息媒体id
	* @return MediaId
	*/
	public String getMediaId() {
		return MediaId;
  	}

	/**
	* set消息媒体id
	* @return MediaId
	*/
	public void setMediaId(String MediaId) {
		this.MediaId = MediaId;
 	}

	/**
	* get图片链接
	* @return PicUrl
	*/
	public String getPicUrl() {
		return PicUrl;
  	}

	/**
	* set图片链接
	* @return PicUrl
	*/
	public void setPicUrl(String PicUrl) {
		this.PicUrl = PicUrl;
 	}

	/**
	* get格式
	* @return Format
	*/
	public String getFormat() {
		return Format;
  	}

	/**
	* set格式
	* @return Format
	*/
	public void setFormat(String Format) {
		this.Format = Format;
 	}

	/**
	* get语音识别结果
	* @return Recognition
	*/
	public String getRecognition() {
		return Recognition;
  	}

	/**
	* set语音识别结果
	* @return Recognition
	*/
	public void setRecognition(String Recognition) {
		this.Recognition = Recognition;
 	}

	/**
	* get缩略图的媒体id
	* @return ThumbMediaId
	*/
	public String getThumbMediaId() {
		return ThumbMediaId;
  	}

	/**
	* set缩略图的媒体id
	* @return ThumbMediaId
	*/
	public void setThumbMediaId(String ThumbMediaId) {
		this.ThumbMediaId = ThumbMediaId;
 	}

	/**
	* get地理位置纬度
	* @return Location_X
	*/
	public String getLocation_X() {
		return Location_X;
  	}

	/**
	* set地理位置纬度
	* @return Location_X
	*/
	public void setLocation_X(String Location_X) {
		this.Location_X = Location_X;
 	}

	/**
	* get地理位置经度
	* @return Location_Y
	*/
	public String getLocation_Y() {
		return Location_Y;
  	}

	/**
	* set地理位置经度
	* @return Location_Y
	*/
	public void setLocation_Y(String Location_Y) {
		this.Location_Y = Location_Y;
 	}

	/**
	* get地理位置纬度
	* @return Latitude
	*/
	public String getLatitude() {
		return Latitude;
  	}

	/**
	* set地理位置纬度
	* @return Latitude
	*/
	public void setLatitude(String Latitude) {
		this.Latitude = Latitude;
 	}

	/**
	* get地理位置经度
	* @return Longitude
	*/
	public String getLongitude() {
		return Longitude;
  	}

	/**
	* set地理位置经度
	* @return Longitude
	*/
	public void setLongitude(String Longitude) {
		this.Longitude = Longitude;
 	}

	/**
	* get地图缩放大小
	* @return Scale
	*/
	public String getScale() {
		return Scale;
  	}

	/**
	* set地图缩放大小
	* @return Scale
	*/
	public void setScale(String Scale) {
		this.Scale = Scale;
 	}

	/**
	* get地理位置信息
	* @return Label
	*/
	public String getLabel() {
		return Label;
  	}

	/**
	* set地理位置信息
	* @return Label
	*/
	public void setLabel(String Label) {
		this.Label = Label;
 	}

	/**
	* get消息标题
	* @return Title
	*/
	public String getTitle() {
		return Title;
  	}

	/**
	* set消息标题
	* @return Title
	*/
	public void setTitle(String Title) {
		this.Title = Title;
 	}

	/**
	* get消息描述
	* @return Description
	*/
	public String getDescription() {
		return Description;
  	}

	/**
	* set消息描述
	* @return Description
	*/
	public void setDescription(String Description) {
		this.Description = Description;
 	}

	/**
	* get消息链接
	* @return Url
	*/
	public String getUrl() {
		return Url;
  	}

	/**
	* set消息链接
	* @return Url
	*/
	public void setUrl(String Url) {
		this.Url = Url;
 	}

	/**
	* get事件KEY值
	* @return EventKey
	*/
	public String getEventKey() {
		return EventKey;
  	}

	/**
	* set事件KEY值
	* @return EventKey
	*/
	public void setEventKey(String EventKey) {
		this.EventKey = EventKey;
 	}

	/**
	* get二维码的ticket
	* @return Ticket
	*/
	public String getTicket() {
		return Ticket;
  	}

	/**
	* set二维码的ticket
	* @return Ticket
	*/
	public void setTicket(String Ticket) {
		this.Ticket = Ticket;
 	}

}

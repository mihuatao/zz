package com.futvan.z.system.zworkflow;

import com.futvan.z.framework.common.bean.Code;
import com.futvan.z.framework.common.bean.Result;
import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import com.futvan.z.framework.core.z;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
@Controller
public class ZworkflowAction extends SuperAction{
	@Autowired
	private ZworkflowService zworkflowService;
	@Autowired
	private CommonService commonService;

	//根据节点ID绑定表单主表ID
	@RequestMapping(value="/getCTableZidForNodeId")
	public @ResponseBody Result getCTableZidForNodeId(String zid) throws Exception {
		Result result = new Result();
		if(z.isNotNull(zid)) {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.zid ");
			sql.append("FROM z_workflow wfc ");
			sql.append("INNER JOIN z_workflow_node wfd ON wfc.zid = wfd.pid ");
			sql.append("LEFT JOIN z_form_table t ON wfc.w_form = t.pid  ");
			sql.append("WHERE t.parent_table_id IS NULL AND  wfd.zid = '"+zid+"' ");
			String ctableId = sqlSession.selectOne("selectone", sql);

			//设置返回信息
			result.setCode(Code.SUCCESS);
			result.setData(ctableId);
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("getCTableZidForNodeId:NodeId is null");
		}
		return result;
	}

	//根据节点ID获取流程ID
	@RequestMapping(value="/getWidForNodeId")
	public @ResponseBody Result getWidForNodeId(String zid) throws Exception {
		Result result = new Result();
		if(z.isNotNull(zid)) {
			String wid = sqlSession.selectOne("selectone", "SELECT pid FROM z_workflow_node WHERE zid = '"+zid+"'");

			//设置返回信息
			result.setCode(Code.SUCCESS);
			result.setData(wid);
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("getWidForNodeId:NodeId is null");
		}
		return result;
	}

	@RequestMapping(value="/getCTableZidForWid")
	public @ResponseBody Result getCTableZidForWid(String zid) throws Exception {
		Result result = new Result();
		if(z.isNotNull(zid)) {
			//根据表ID获取绑定流程
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT t.zid ");
			sql.append("FROM z_workflow wfc ");
			sql.append("LEFT JOIN z_form_table t ON wfc.w_form = t.pid  ");
			sql.append("WHERE t.parent_table_id IS NULL AND wfc.zid = '"+zid+"' ");
			String ctableId = sqlSession.selectOne("selectone", sql);

			//设置返回信息
			result.setCode(Code.SUCCESS);
			result.setData(ctableId);
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("getCTableColumnList:formid is null");
		}
		return result;
	}

	@RequestMapping(value="/getOaWid")
	public @ResponseBody Result getOaWid(String tableId) throws Exception {
		Result result = new Result();
		if(z.isNotNull(tableId)) {
			//根据表ID获取绑定流程
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT zw.* ");
			sql.append("FROM z_workflow zw ");
			sql.append("INNER JOIN z_form zf ON  zw.w_form = zf.zid ");
			sql.append("INNER JOIN z_form_table zft ON zf.zid = zft.pid ");
			sql.append("WHERE zw.is_start = '1' AND  zft.table_id = '"+tableId+"' ");
			List<HashMap<String,String>> list = commonService.SelectForSQL(sql.toString());

			//设置返回信息
			result.setCode(Code.SUCCESS);
			result.setData(list);
		}else {
			result.setCode(Code.ERROR);
			result.setMsg("getOaWid:tableId is null");
		}
		return result;
	}

	@RequestMapping(value="/z_workflow_insert")
	public @ResponseBody Result z_workflow_insert(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = commonService.insert(bean,request);
		if(Code.SUCCESS.equals(result.getCode())) {
			//添加开始结点与结束结点
			zworkflowService.CreateInitNode(bean,request);
		}
		return result;
	}

	@RequestMapping(value="/z_workflow_update")
	public @ResponseBody Result z_workflow_update(@RequestParam HashMap<String,String> bean) throws Exception {
		Result result = commonService.update(bean,request);
		if(Code.SUCCESS.equals(result.getCode())) {
			//添加开始结点与结束结点
			zworkflowService.CreateInitNode(bean,request);
		}
		return result;
	}



}

package com.futvan.z.system.zworkflow;

import com.futvan.z.framework.core.SuperService;
import com.futvan.z.framework.core.z;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
@Service
public class ZworkflowService extends SuperService{

	/**
	 * 创建开始与结束节点
	 * @param bean
	 * @param request 
	 * @throws Exception 
	 */
	public void CreateInitNode(HashMap<String, String> bean, HttpServletRequest request) throws Exception {
		//判读是否有开始结点
		int count0 = selectInt("SELECT COUNT(*) FROM z_workflow_node WHERE node_type = 0 AND pid = '"+bean.get("zid")+"' ");
		if(count0==0) {
			//创建
			HashMap<String, String> NodeBean = new HashMap<String, String>();
			NodeBean.put("zid", z.newZid("z_workflow_node"));
			NodeBean.put("node_title", "开始");
			NodeBean.put("node_type", "0");
			NodeBean.put("pid", bean.get("zid"));
			NodeBean.put("tableId", "z_workflow_node");
			insert(NodeBean, request);
		}
	
	
		//判读是否有结束结点
		int count1 = selectInt("SELECT COUNT(*) FROM z_workflow_node WHERE node_type = 9 AND pid = '"+bean.get("zid")+"' ");
		if(count1==0) {
			//创建
			HashMap<String, String> NodeBean = new HashMap<String, String>();
			NodeBean.put("zid", z.newZid("z_workflow_node"));
			NodeBean.put("node_title", "结束");
			NodeBean.put("node_type", "9");
			NodeBean.put("pid", bean.get("zid"));
			NodeBean.put("tableId", "z_workflow_node");
			insert(NodeBean, request);
		}
	}
	

}

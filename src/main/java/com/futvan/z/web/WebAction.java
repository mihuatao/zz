package com.futvan.z.web;

import com.futvan.z.framework.common.service.CommonService;
import com.futvan.z.framework.core.SuperAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
public class WebAction extends SuperAction {
	@Autowired
	private WebService webService;
	@Autowired
	private CommonService commonService;

	@RequestMapping(value="/index")
	public ModelAndView index(String cityId) throws Exception {
		ModelAndView  model = new ModelAndView("web/index");
		return model;
	}


	@RequestMapping(value="/bi001")
	public ModelAndView bi001() throws Exception {
		ModelAndView  model = new ModelAndView("web/bi001");

		//参数一
		List<String> p1 = new ArrayList<String>();
		p1.add("001");
		p1.add("002");
		p1.add("003");
		p1.add("004");
		p1.add("005");
		model.addObject("p1",p1);

		//参数二
		List<String> p2 = new ArrayList<String>();
		p2.add("0201");
		p2.add("0202");
		p2.add("0203");
		p2.add("0204");
		p2.add("0205");
		model.addObject("p2",p2);

		return model;
	}
	
	


}

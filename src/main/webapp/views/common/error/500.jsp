<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<!-- 系统ICO图标 -->
		<link href="<%=request.getContextPath()%>/img/zico.ico" rel="shortcut icon" type="image/x-icon"  media="screen,print" />
		<!-- 添加BootStrap -->
		<link  href="<%=request.getContextPath()%>/js/bootstrap-4.6.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen,print"/>
		<script src="<%=request.getContextPath()%>/js/bootstrap-4.6.0/js/bootstrap.min.js" type="text/javascript"></script>
		<title>系统错误</title>
	</head>
	<body>
		<h3>程序执行出错</h3>
		<p><button onclick="history.go(-1)" class="btn btn-outline-info btn-lg">返回</button></p>
		<h5>${requestScope.ex}</h5>
	</body>
</html>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>拍照</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
//初始化方法
$(document).ready(function() {
	Webcam.set({
		width: 600,//宽度
		height: 450,//高度
		image_format: 'jpeg',//图片格式
		flip_horiz: true,//水平反转
		jpeg_quality: 100//图片质量0~100
	});
	Webcam.attach('#z_camera');//开始摄像头
});


//拍照
function take_snapshot() {
	openLoading();
	Webcam.snap(function(data_uri) {
		data_uri = encodeURIComponent(data_uri);
		var ColumnId = $("#ColumnId").val();
		var PageType = $("#PageType").val();
		var zid = $("#zid").val();
		var tableId = $("#tableId").val();
		$.ajax({
			type : "post",
			url : "upload_imgdate",
			data:{imgdate:data_uri,ColumnId:ColumnId,PageType:PageType,zid:zid,tableId:tableId},
			success : function(data) {
				closedLoading();
				if(data.code=='SUCCESS'){
					if(PageType=='edit'){
		            	if(isNotNull(ColumnId)){
		            		window.opener.OpenFileUploadReturn(ColumnId,data.data);
		            	}
					}
					if(PageType=='list'){
						window.opener.RefreshList();
					}
	                window.close();
				}else{
					alert(data.msg);
				}
			},
			error: function (data) {
				closedLoading();
				alert("ajax错误："+JSON.stringify(data));
			}
		});
	});
}


</script>
</head>
<body>
	<input type="hidden" id="tableId" value="${bean.tableId}" />
	<input type="hidden" id="ColumnId" value="${bean.ColunmId}" />
	<input type="hidden" id="zid" value="${bean.zid}" />
	<input type="hidden" id="PageType" value="${bean.PageType}" />
	<div class="container-fluid">
		<div class="row" >
			<div class="col-12 pt-2 d-flex justify-content-center">
				<div id="z_camera"></div>
			</div>
			<div class="col-12 pt-2 pb-2">
				<button class="btn btn-primary btn-lg btn-block" onClick="take_snapshot()"><i class="fa fa-camera"></i> 拍照</button><div id="z_camera"></div>
			</div>
		</div>
	</div>
	<%@include file="/views/common/body.jsp"%>
</body>
</html>
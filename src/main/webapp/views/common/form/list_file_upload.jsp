<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>文件上传</title>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
$(function() {
	
	var zid = $("#zid").val();
	if(isNull(zid)){
		$("#alertInfo").text("zid is null|参数异常，无法上传文件");
	}
	
	var tableId = $("#tableId").val();
	if(isNull(tableId)){
		$("#alertInfo").text("tableId is null|参数异常，无法上传文件");
	}
	
	var ColumnId = $("#ColumnId").val();
	if(isNull(ColumnId)){
		$("#alertInfo").text("ColumnId is null|参数异常，无法上传文件");
	}
	
	var return_url_head = '${sp.fileserverurl}';
	if(isNull(return_url_head)){
		$("#alertInfo").text("sp.fileserverurl is null|参数异常，无法上传文件");
	}
	
	var upload_url = return_url_head+'/upload?filepath='+$("#zid").val()+'&return_url_head='+encodeURIComponent(return_url_head);
	$("#uploadify").uploadifive({
		uploadScript:upload_url,
		auto: true,
		fileDataName:'fileData',
		fileObjName:'fileData',
		buttonText: '请选择要上传的文件',
		queueID: 'queue',
		height: 50,
		width : 220,
		multi: false,
		onFallback:function(){    
            $("#alertInfo").text("您的浏览器不支持HTML5文件上传控件，请使用支持HTML5的浏览器后使用。如：Chrome")
        },onUploadComplete : function(file, data){  
        	var obj = JSON.parse(data);
            if (obj.code == "SUCCESS") {
            	//上传成功后，保存到指定表字段中
            	$.ajax({
					type : "get",
					url : "update",
					data:'zids='+zid+'&tableId='+tableId+'&'+ColumnId+'='+obj.data,
					success : function(data) {
						if("list"==$("#PageType").val()){
							window.opener.RefreshList();
						}
						if("edit"==$("#PageType").val()){
							window.opener.RefreshEdit();
						}
						//关闭当前页面
						window.close();
					},
					error: function (data) {
						$("#alertInfo").text("ajax错误："+JSON.stringify(data));
					}
				});
            } else {
            	 $("#alertInfo").text(obj.msg);
            }
        	
        },onAddQueueItem:function(file){
        	/* if(/.*[\u4e00-\u9fa5]+.*$/.test(file.name)) { 
        		$("#alertInfo").text("文件名,不能包含中文!")
        		$("#uploadify").uploadifive('cancel',$('.uploadifive-queue-item').first().data('file')); 
        		$("#uploadify").uploadifive('clearQueue');
        	} */
        }
	});
});
</script>
</head>
<body>
<div class="container-fluid">
	<div class="row-fluid" >
		<input type="hidden" id="tableId" value="${bean.tableId}" />
		<input type="hidden" id="ColumnId" value="${bean.ColunmId}" />
		<input type="hidden" id="zid" value="${bean.zid}" />
		<input type="hidden" id="PageType" value="${bean.PageType}" />
		<input id="uploadify" name="fileData" type="file"/>
	</div>
	<div class="row-fluid" >
		<span id="alertInfo"></span>
		<div id="queue"></div>
	</div>
</div>
<%@include file="/views/common/body.jsp"%>
</body>
</html>
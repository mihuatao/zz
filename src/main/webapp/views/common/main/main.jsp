<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/main/main_head.jsp"%>
</head>
<body class="easyui-layout" id="zmain_easyui_layout">
	<!-- 菜单区域 -->
	<div id="menu_title_div" data-options="region:'west',title:'功 能 列 表 ',hideCollapsedContent:false,border:true,collapsed:true,split:true" style="width: 15%;">
		<div class="easyui-layout" data-options="fit:true"> 
				<div data-options="region:'north',border:true" style="height:100px;"> 
					<!-- 用户信息 -->
					<div class="row" style="padding: 5px;margin:0px;"> 
		  				<div class="col-md-5" style="padding: 0px;margin:0px;width: 80px;height: 80px">
		  					<c:if test='${not empty zuser.photo}'>
		  						<img class="img-thumbnail" width="80px" height="80px" src="${zuser.photo}" />
							</c:if>
							<c:if test="${empty zuser.photo}" >
		  						<img class="img-thumbnail" width="80px" height="80px" src="./img/system/touxiang.jpg" /> 
							</c:if>
						</div>
		  				<div class="col-md-7" >
		  					<div class="row" >
		  						<h5>${zuser.user_name}</h5>
		  					</div>
		  					<div class="row">
		  						<h6>${zorg.org_name}</h6>
		  					</div>
						</div>
						
		  			</div>
				</div>
				
				<div data-options="region:'center',border:true">
					
					<!-- 菜单搜索框 -->
					<div class="row pt-3 pl-2 pr-2 m-0" style="float:left"> 
						<div class="col-12">
							<input type="text" class="form-control" id="search_menu_input" placeholder="快速搜索功能" oninput="search_menu()">
						</div>
						<div class="col-12">
							<ul id="search_menu_list" class="list-group d-none">
							</ul>
						</div>
						
						<script type="text/javascript">
							function search_menu(){
								var x = $("#search_menu_input").val();
								if(isNotNull(x)){
									
									//搜索相关功能
									var list = '${user_all_menu_list}';
									var listjson = $.parseJSON(list);
									$('#search_menu_list').empty();
									for (var i = 0; i < listjson.length; i++) {
										var m = listjson[i];
										if(m.name.indexOf(x)>=0 || m.number.indexOf(x)>=0 ||m.parentid.indexOf(x)>=0){
											$('#search_menu_list').append('<li class="list-group-item" onclick="addTab(\''+m.name+'\',\''+m.url+'\',\''+m.menu_icon+'\',\''+m.zid+'\')"><i class="'+m.menu_icon+' pr-1"></i>'+m.name+'</li>');
										}
									}
									
									$("#search_menu_list").removeAttr("class", "d-none");
									$("#search_menu_list").addClass("list-group");
								}else{
									$("#search_menu_list").addClass("d-none");
								}
							}
						</script>
						
  					</div>
  					
						
					<!-- 菜单目录 -->
					<div class="row" style="padding: 10px;margin:0px;overflow-x:hidden;">
  						<div id="SideMenuTree" class="easyui-sidemenu" data-options="data:menuTreeList,border:false,onSelect:MenuTreeSelect,multiple:false" style="width: 100%" ></div>
  					</div>
				</div>
				
	<%--        <div data-options="region:'south',border:false" style="overflow-x: hidden;overflow-y: hidden;padding: 10px;">
				</div>  --%>
				
		</div>
	</div>
	<!-- 主页面区域 -->
	<div data-options="region:'center',border:false">
		<div id="myTab" class="easyui-tabs"  data-options="tools:'#myTab-tools'"  style="height: 100%">
			<c:if test='${sp.is_show_index==true}'>
				<div title=" 功 能 权 限 "  iconCls="fa fa-home">
					<iframe src="main_index" id="index_iframe" name="MainIndex" scrolling="auto" frameborder="0"   style="width:100%;height:100%;"></iframe>
				</div>
			</c:if>
		</div>
	</div>
	<%@include file="/views/common/main/main_body.jsp"%>	
</body>
</html>
<%@ page pageEncoding="utf-8"%>
<div id="myTab-tools">
	<!-- 超级管理员功能 -->
 	<c:if test='${zuser.zid==sp.super_user}'>
		<button type="button" class="easyui-menubutton" data-options="menu:'#kaifa'"><i class="fa fa-gears"></i> 开发管理</button>
	</c:if>
	<button type="button" class="easyui-linkbutton" data-options="plain:true" onclick="addTab('待办任务','rlist?zid=2a3810a816b847o2rp4arzd21a2e125178tfc219731e172317241514163008','fa fa-tags','2a3810a816b847o2rp4arzd21a2e125178tfc219731e172317241514163008');"><i class="fa fa-tags"></i> 待办任务</button>
	<button type="button" class="easyui-linkbutton" data-options="plain:true" onclick="addTab('设置','user_settings','fa fa-sliders','user_settings');"><i class="fa fa-sliders"></i> 设置</button>
	<button type="button" class="easyui-linkbutton" data-options="plain:true" onclick="locking();"><i class="fa fa-unlock-alt"></i> 锁屏</button>
	<button type="button" class="easyui-linkbutton" data-options="plain:true" onclick="UserExit();"><i class="fa fa-power-off"></i> 退出</button>
</div>

<div id="kaifa" style="width:100px;">
	<div data-options="iconCls:'fa fa-window-restore'" onclick="addTab('表单管理','list?tableId=z_form','fa fa-window-restore','from_system')">表单管理</div>
	<div data-options="iconCls:'fa fa-server'" onclick="addTab('HTTP接口','list?tableId=z_http_services','fa fa-server','http_service_system')">HTTP接口</div>
	<div data-options="iconCls:'fa fa-bar-chart'" onclick="addTab('报表管理','list?tableId=z_report','fa fa-bar-chart','report_system')">报表管理</div>
	<div data-options="iconCls:'fa fa-usb'" onclick="addTab('流程定义','list?tableId=z_workflow','fa fa-usb','workflow_system')">流程定义</div>
	<div data-options="iconCls:'fa fa-braille'" onclick="addTab('字典管理','list?tableId=z_code','fa fa-braille','code_system')">字典管理</div>
	<div class="menu-sep"></div>
	<div data-options="iconCls:'fa fa-map-pin'" onclick="addTab('任务管理','list?tableId=z_job','fa fa-map-pin','job_system')">任务管理</div>
	<div class="menu-sep"></div>
	<div data-options="iconCls:'fa fa-power-off'" onclick="RestartTomcat()">重启Tomcat</div>
	<div class="menu-sep"></div>
	<div data-options="iconCls:'fa fa-gears'" onclick="addTab('设置系统参数','list?tableId=z_sp','fa fa-gears','SetSystemParameter')">系统参数设置</div>
	<div data-options="iconCls:'fa fa-refresh'" onclick="LoadParameter()">更新缓存数据</div>
</div>
	
<div id="UserMessagesList" data-options="region:'east',hideCollapsedContent:false,collapsible:true,collapsed:true,split:true" title="系统用户：${user_messages_list.size()}人" style="width:200px;">
	<c:forEach items="${user_messages_list}" var="users_message">
		<c:if test='${users_message.zid!= zuser.zid}'>
			<c:if test='${users_message.isonline==0}'>
				<button id="message_user_${users_message.zid}" type="button" class="btn btn-success btn-block" onclick="OpenMessageWindows('${users_message.zid}','${users_message.user_name}')">${users_message.user_name} <c:if test='${users_message.messagecount>0}'>[${users_message.messagecount}]</c:if></button>
			</c:if>
			<c:if test='${users_message.isonline==1}'>
				<button id="message_user_${users_message.zid}" type="button" class="btn btn-light btn-block" onclick="OpenMessageWindows('${users_message.zid}','${users_message.user_name}')">${users_message.user_name} <c:if test='${users_message.messagecount>0}'>[${users_message.messagecount}]</c:if></button>
			</c:if>
		</c:if> 
	</c:forEach>
</div>
	
<!-- 信息提示框 -->
<div id="zalert" class="modal fade" tabindex="-1" aria-labelledby="zalert" aria-hidden="true">
  <div id="zalert_head" class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" id="zalert_title"></div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="zalert_info">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-dismiss="modal">关闭</button>
      </div>
    </div>
  </div>
</div>

<!-- 锁屏框 -->
<div id="lockdialog" class="modal bg-secondary fade " data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lockdialog" aria-hidden="true" >
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
		<div class="modal-content" >
	      <div class="modal-header">
	        <div class="modal-title">解除锁屏</div>
	      </div>
	      <div class="modal-body" >
	      	<input id="lockdialog_info" class="form-control form-control-lg" type="password" placeholder="请输入解锁密码">
	      </div>
	      <div class="modal-footer d-flex justify-content-between">
	      	<span id="lockdialog_span" class="text-danger"></span>
	        <button type="button" class="btn btn-secondary" onclick="unlock()"><i class="fa fa-unlock"></i> 解锁</button>
	      </div>
	    </div>
    </div>
</div>

<!-- 聊天窗口 -->
<div id="message_windows" class="easyui-window" title="系统管理员" style="width:800px;height:500px" data-options="closed:true,shadow:false,minimizable:false,cache:false">
	<div id="MessageTab" class="easyui-tabs"  border="false" fit="true" tabPosition="left"></div>
</div>
	
	
<!-- myTab右键菜单 -->
<div id="my_tab_menu" class="easyui-menu" style="width:120px;">
	<div data-options="name:'tab_refresh',iconCls:'fa fa-refresh'">刷新</div>
	<div class="menu-sep"></div>
	<div data-options="name:'tab_close_other_all',iconCls:'fa fa-window-close-o'">关闭其它功能</div>
    <div data-options="name:'tab_close_all',iconCls:'fa fa-window-close'">关闭全部功能</div>
</div>
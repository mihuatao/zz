<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
</script>
<style type="text/css">
</style>
</head>
<body>
<div class="container-fluid">
  <div class="row p-1">
  	<z:MenuTable list="${menu_list}"/>
  </div>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<%@include file="/views/common/common.jsp"%>
	<script type="text/javascript">
		function RunRecharge(){
			var userid = $("#userid_id").val();
			var a_type = $("#a_type_id").val();
			var amount = $("#amount_id").val();
			var bizid = $("#bizid_id").val();
			console.log(userid+' '+a_type+' '+amount);
			if(isNotNull(userid)){
				if(isNotNull(a_type)){
					if(amount>0){
						$.messager.confirm('信息提示','您确定要给'+userid+'执行充值吗？',function(r){
							if (r){
								$.ajax({
									type : "get",
									url : 'acc_add?userid='+userid+'&a_type='+a_type+'&amount='+amount+'&bizid='+bizid,
									success : function(data) {
										if(data.code=='SUCCESS'){
											parent.alertMessager('充值成功');
											location.reload();
										}else{
											parent.alertErrorMessager(''+data.msg);
										}
									},
									error: function (data) {
										parent.alertErrorMessager('ajax错误：'+JSON.stringify(data));
									}
								});
							}
						});
					}else{
						parent.alertErrorMessager('充值金额必须大于0');
					}
				}else{
					parent.alertErrorMessager('账户不能为空');
				}
			}else{
				parent.alertErrorMessager('客户不能为空');
			}


		}

	</script>
</head>
<body class="easyui-layout">
<div data-options="region:'north'" class="border-top-0 border-right-0 border-left-0" style="height:38px;overflow:hidden">
	<div id="ButtonHR" class="btn-group" role="group">
		<c:if test="${is_mobile=='1'}" >
			<button id="return_button" type="button" class="btn btn-light" onclick="window.location.href='user_center_mobile';"><i class="fa fa-hand-o-left"></i> 返回</button>
		</c:if>
		<button id="RunRechargeButton" type="button" class="btn btn-light" onclick="RunRecharge();"><i class="fa fa-floppy-o"></i> 执行充值</button>
	</div>

</div>
<div data-options="region:'center',border:false">
	<z:column column_size="4" column_type="8" column_id="userid" column_name="客户" column_help="客户" tableId="erp_account" z5_table="crm_customer" z5_key="zid" z5_value="name" />
	<z:column column_size="4" column_type="8" column_id="a_type" column_name="账户" column_help="账户"  tableId="erp_account" z5_table="erp_account_type" z5_key="zid" z5_value="name" />
	<z:column column_size="4" column_type="2" column_id="amount" column_name="金额" 	column_help="金额" />
	<z:column column_size="4" column_type="1" column_id="bizid" column_name="银行充值记录号" 	column_help="银行充值记录号" />
</div>

<%@include file="/views/common/body.jsp"%>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/common.jsp"%>
<script type="text/javascript">
	//上传文件
	function upload(){
		var width = 400;
		var height = 250;
		var iTop = (window.screen.availHeight-30-height)/2;//获得窗口的垂直位置;
		var iLeft = (window.screen.availWidth-10-width)/2;//获得窗口的水平位置;
		var directory = $("#directory").val();
		var url = 'openUploadFile?zid='+directory;
		var winObj = window.open(url, '文件上传', 'width='+width+',height='+height+',location=no,menubar=no,status=no,toolbar=no, top='+iTop+', left='+iLeft);

		var loop = setInterval(function(){
		　　if(winObj.closed){
				clearInterval(loop);
				reload();
		　　}

		 },1);
	}
	
	//创建文件夹
	function createDir(){
		var dirname = $("#dirname").val();
		var directory = $("#directory").val(); 
		if(isNotNull(dirname)){
			$.ajax({
				type : "get",
				url : 'CreateServerDir',
				data : {
					dirname : dirname,
					directory : directory
				},
				success : function(data) {
					if (data.code == 'SUCCESS') {
						alertMessagerAutoOff('' + data.msg);
						reload();
					} else {
						alertErrorMessager('' + data.msg);
					}
				},
				error : function(data) {
					alertErrorMessager('ajax错误：' + JSON.stringify(data));
				}
			});
		}else{
			alertMessagerAutoOff('请输入文件夹名称');
			$("#dirname").focus(); 
		}
	}
	
	//重命名
	function updateName(){
		var zids = getTableColumn('MainTable','zid');
		if(zids==''){
			alertMessager('请选择要操作的记录');
		}else{
			var zids_array = zids.split(",");
			if(zids_array.length==1 && zids_array[0]!='' && zids_array[0]!=null){
				$('#UpdateFileNameBox').modal('show');
			}else{
				alertMessager('只能选择一条记录操作');
			}
		}
	}
	
	//执行重命名
	function UpdateFileName(){
		var rfilename = $("#rfilename").val();
		var zid = getTableColumn('MainTable','zid');
		if(isNotNull(rfilename)){
			$.ajax({
				type : "get",
				url : 'UpdateServerFileName',
				data : {
					rfilename : rfilename,
					filepath : zid
				},
				success : function(data) {
					if (data.code == 'SUCCESS') {
						alertMessagerAutoOff('' + data.msg);
						reload();
					} else {
						alertErrorMessager('' + data.msg);
					}
				},
				error : function(data) {
					alertErrorMessager('ajax错误：' + JSON.stringify(data));
				}
			});
		}else{
			alertMessager('请输入文件名称');
			$("#rfilename").focus(); 
		}
	}
	
	
	
	//下载文件
	function download(){
		var directory = $("#directory").val(); 
		var zids = getTableColumn('MainTable','zid');
		if(zids==''){
			alertMessager('请选择要下载的记录');
		}else{
			
			$.messager.confirm('信息提示','您确定下载这些文件吗？',function(r){
				if (r){
					
					var $form = $('<form method="GET"></form>');
					$form.attr('action', 'downloadServerFiles');
					var zids_input = $('<input type="hidden" name="zids" value="'+zids+'" />');  
					$form.append(zids_input);
					var directory_input = $('<input type="hidden" name="directory" value="'+directory+'" />');  
					$form.append(directory_input);
					$form.appendTo($('body'));
					$form.submit();
					
				}
			});
		}
	}
	
	//删除文件
	function remove(){
		var zids = getTableColumn('MainTable','zid');
		if(zids==''){
			alertMessager('请选择要删除的记录');
		}else{
			$.messager.confirm('信息提示', '您确定删除这些文件吗？', function(r) {
				if (r) {
					$.ajax({
						type : "get",
						url : 'deleteLocalFile',
						data : {
							zids : zids
						},
						success : function(data) {
							if (data.code == 'SUCCESS') {
								alertMessagerAutoOff('' + data.msg);
								reload();
							} else {
								alertErrorMessager('' + data.msg);
							}
						},
						error : function(data) {
							alertErrorMessager('ajax错误：' + JSON.stringify(data));
						}
					});
				}
			});
			
		}
		
	}
	
</script>
</head>
<body class="easyui-layout layout panel-noscroll">
	<input type="hidden" id="directory" value="${directory}">
	<!-- <input type="file" id="fileinput"> -->
	<div data-options="region:'north'"
		class="border-top-0 border-right-0 border-left-0"
		style="height: 85px; overflow: hidden;">
		<div id="ButtonHR" class="row">
			<div class="col-sm border-bottom">
				<button type="button" class="btn btn-light" onclick="upload();">
					<i class="fa fa-upload"></i> 上传文件
				</button>
				<button type="button" class="btn btn-light" onclick="download();">
					<i class="fa fa-download"></i> 下载文件
				</button>
				<button type="button" class="btn btn-light" data-toggle="modal" data-target="#CreateDirBox">
					<i class="fa fa-plus-circle"></i> 创建文件夹
				</button>
				<button type="button" class="btn btn-light" onclick="updateName()">
					<i class="fa fa-pencil"></i> 重命名
				</button>
				<button type="button" class="btn btn-light" onclick="remove();">
					<i class="fa fa-trash-o"></i> 删除文件
				</button>
				<button type="button" class="btn btn-light" onclick="reload();">
					<i class="fa fa fa-refresh"></i> 刷新
				</button>
			</div>
		</div>

		<div class="row ">
			<div class="col-12">
				<nav aria-label="breadcrumb" class="p-0 m-0">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="all_file_list" class="text-dark">根目录</a></li>
					<c:forEach items="${pathlist}" var="f">
						<li class="breadcrumb-item"><a class="text-dark" href="all_file_list?directory=${f.filepath}">${f.name}</a></li>
					</c:forEach>
				</ol>
				</nav>
			</div>
		</div>
	</div>

	<div data-options="region:'center',border:false">
		<table id="MainTable" class="easyui-datagrid" data-options="fit:true,ctrlSelect:true,nowrap:false,onDblClickCell:DblClickList">
			<thead data-options="frozen:true">
				<tr>
					<th data-options="field:'zid',checkbox:true">主键</th>
					<th data-options="field:'name',width:500">名称</th>
					<th data-options="field:'file_type',width:150">类型</th>
					<th data-options="field:'size',width:150">大小</th>
					<th data-options="field:'create_time',width:300">创建时间</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${filelist}" var="f">
					<tr>
						<td class="border-right">${f.zid}</td>
						<c:if test='${f.file_type=="目录"}'>
							<td><a class="text-dark" href="all_file_list?directory=${f.filepath}">${f.name}</a></td>
						</c:if>
						<c:if test='${f.file_type!="目录"}'>
							<td><a class="text-dark" href="${f.filepath}" target="_blank">${f.name}</a></td>
						</c:if>
						<td class="border-right">${f.file_type}</td>
						<td class="border-right">${f.size}${filelist.size()}</td>
						<td class="border-right">${f.create_time}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>



	<div class="modal fade" id="CreateDirBox" data-backdrop="static"
		data-keyboard="false" tabindex="-1"
		aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">请输入文件夹名称</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="text" id="dirname" class="form-control" value/>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="createDir()">创建文件夹</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="UpdateFileNameBox" data-backdrop="static"
		data-keyboard="false" tabindex="-1"
		aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">请输入文件名称</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<input type="text" id="rfilename" class="form-control" value/>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="UpdateFileName();">更新文件名</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
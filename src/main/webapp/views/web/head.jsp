<%@ page pageEncoding="utf-8"%>
<div class="row">
	<div class="col-6 pt-4 pl-4" onclick="">
		<a href="/"><img src="img/web/logo_w300.png" class="img-fluid" alt="Z平台 Logo"></a>
	</div>
	<div class="col-6 pt-4 text-right">
		<nav class="navbar navbar-expand-lg navbar-light bg-light bg-white float-right">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a href="/" class="btn btn-light">首页</a></li>
					<li class="nav-item"><a href="u_s_l" class="btn btn-light" target="_blank">演示系统</a></li>
					<li class="nav-item"><a href="#download" class="btn btn-light">下载中心</a></li>
					<li class="nav-item"><a href="https://gitee.com/zj1983/zz/tree/master/docs" class="btn btn-light">使用教程</a></li>
					<!-- <li class="nav-item"><a href="index#about_us" class="btn btn-light">关于作者</a></li> -->
				</ul>
			</div>
		</nav>
	</div>
</div>
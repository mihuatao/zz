<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="z" uri="zz"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@include file="/views/common/common.jsp"%>
</head>
<body>
<div class="container border-bottom">
	<!--Head-->
	<%@include file="head.jsp"%>

	<!--index-->
	<!-- 说明信息 -->
	<div class="row">
		<div class="col-12">
			<div class="jumbotron bg-white">
				<ul class="nav nav-tabs nav-justified " role="tablist">
					<li class="nav-item" role="presentation"><a class="nav-link text-dark active" data-toggle="tab" href="#jiangjie_1">平台简述</a></li>
					<li class="nav-item" role="presentation"><a class="nav-link text-dark" data-toggle="tab" href="#jiangjie_2">平台价值</a></li>
					<li class="nav-item" role="presentation"><a class="nav-link text-dark" data-toggle="tab" href="#jiangjie_3">平台特点</a></li>
					<li class="nav-item" role="presentation"><a class="nav-link text-dark" data-toggle="tab" href="#jiangjie_4">适用用户</a></li>
				</ul>

				<div class="tab-content mt-4">
					<div class="tab-pane fade show active" id="jiangjie_1"
						 role="tabpanel">
						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-6">
										<p class="h4 pb-3" style="color:#4032AE"><i class="fa fa-hand-o-right"></i>Z平台能做什么？</p>
										<p class="pb-4 font-weight-normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Z平台是开源免费的Java Web快速开发平台。通过Z平台集成开发环境，以零编码、动态配置的方式能够快速开发出各类Web系统软件。Z平台框架组成所用到的各种功能组件与框架，都是开源免费的，不涉及到版权问题，商业与非商业项目都可以使用。</p>

										<p class="h4 pb-3" style="color:#4032AE"><i class="fa fa-hand-o-right"></i>使用Z平台有没有费用？</p>
										<p class="pb-3 font-weight-normal">Z平台<span class="text-danger">永久免费</span></p>

										<p class="h4 pb-3" style="color:#4032AE"><i class="fa fa-hand-o-right"></i>技术问题咨询</p>
										<p class="pb-3 font-weight-normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;您在使用Z平台过程中，如果遇到技术问题，可以给我发邮件，在邮件中详细描述遇到的问题，并将错误日志、错误截图、计算机环境信息一并写在邮件中，我将通过邮件的方式为您解答。</p>
										<p class="h5 pb-1 font-weight-normal">邮箱：<span class="text-danger">zframeworks@qq.com</span></p>
										<p class="h5 pb-3 font-weight-normal">QQ群：<a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=2id9ln2I4Tmn7i9FaltfanEb-nqPNo6P&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Z平台" title="Z平台"></a></p>
									</div>
									<div class="col-md-6">
										<p class="h4 pb-3" style="color:#4032AE"><i class="fa fa-hand-o-right"></i>适合开发什么项目？</p>
										<p class="font-weight-normal"><i class="fa fa-arrow-right" style="color:#4032AE"></i>企业ERP系统</p>
										<p class="font-weight-normal"><i class="fa fa-arrow-right" style="color:#4032AE"></i>APP项目后端接口服务</p>
										<p class="font-weight-normal"><i class="fa fa-arrow-right" style="color:#4032AE"></i>各类网站</p>
										<p class="font-weight-normal"><i class="fa fa-arrow-right" style="color:#4032AE"></i>统计报表平台</p>
										<p class="pb-3 font-weight-normal"><i class="fa fa-arrow-right" style="color:#4032AE"></i>自动任务平台</p>

										<p class="h4 pb-3" style="color:#4032AE"><i class="fa fa-hand-o-right"></i>与同类产品对比</p>
										<p class="pb-1"><span class="font-weight-bold">相同点：</span></p>
										<p class="pb-2 font-weight-normal">开发模式相似，平台包含模块大致相同，都包含表单、报表、接口、工作流、任务等开发功能。</p>
										<p class="pb-1"><span class="font-weight-bold">不同点：</span></p>
										<p class="pb-5 font-weight-normal">其它同类产品收费，Z平台永久免费。</p>
									</div>
								</div>

							</div>


						</div>
					</div>

					<div class="tab-pane fade" id="jiangjie_2" role="tabpanel">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;第一、提升软件开发速度，缩短软件开发周期。</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;第二、降低软件开发BUG率，缩短软件测试周期。</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;第三、降低项目所需高级开发人员比例，减少项目用工成本支出。</p>
					</div>

					<div class="tab-pane fade" id="jiangjie_3" role="tabpanel">
						<p class="font-weight-bold">永久开源免费</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;Z平台为开源免费项目，可以应用在所有商业或非商业项目中进行使用。</p>
						<p class="font-weight-bold">学习成本低</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;Z平台所使用的框架都是热门的开源技术框架。学习资料丰富。核心框架为Spring + SpringMVC + Mybatis组成。</p>
						<p class="font-weight-bold">技术成熟稳定</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;Z平台所应用的基础框架都是经过长时间沉淀成熟稳定的开源框架。在稳定性方面值得信赖。</p>
					</div>

					<div class="tab-pane fade" id="jiangjie_4" role="tabpanel">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th scope="col">Z平台功能</th>
									<th scope="col">非专业人士</th>
									<th scope="col">企业信息管理人员</th>
									<th scope="col">程序员</th>
								</tr>
								</thead>
								<tbody>
								<tr>
									<td>安装部署</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
								</tr>
								<tr>
									<td>平台功能</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
								</tr>
								<tr>
									<td>表单开发</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
								</tr>
								<tr>
									<td>报表开发</td>
									<td class="text-danger">NO</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
								</tr>
								<tr>
									<td>工作流程配置</td>
									<td class="text-danger">NO</td>
									<td class="text-success">YES</td>
									<td class="text-success">YES</td>
								</tr>
								<tr>
									<td>自动任务配置</td>
									<td class="text-danger">NO</td>
									<td class="text-danger">NO</td>
									<td class="text-success">YES</td>
								</tr>
								<tr>
									<td>接口开发</td>
									<td class="text-danger">NO</td>
									<td class="text-danger">NO</td>
									<td class="text-success">YES</td>
								</tr>
								<tr>
									<td>框架修改</td>
									<td class="text-danger">NO</td>
									<td class="text-danger">NO</td>
									<td class="text-success">YES</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron bg-white">
				<h3>搭建环境服务</h3>
				<div class="tab-content mt-4">
					<div class="row pt-4 border-top">
						<div class="col-md-10">
							<p class="pb-3 font-weight-normal">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;如果您想使用Z平台建设项目，但对开发或运行环境搭建不太擅长，您可以通过微信与我联系，购买服务，我将通过QQ远程方式，辅助您快速完成开发与运行环境搭建工作。</p>
							<p class="pb-3 font-weight-normal">服务费：<span class="text-danger">100元/次</span></p>
						</div>
						<div class="col-md-2">
							<p class="pb-3"><img src="img/web/wxewm.jpg" class="img-fluid"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- 下载中心 -->
	<div class="row" id="download">
		<div class="col-12">
			<div class="jumbotron bg-white">
				<div class="col-md-3"><h3>下载中心</h3></div>
				<div class="row pt-4">

					<div class="col-md-12 pt-3">
						<div class="row">
							<div class="col-md-3">
								<div class="card mb-2">
									<div class="card-header">Z平台-集成版</div>
									<div class="card-body">
										<button class="btn btn-outline-secondary btn-block" onclick="alertMessager('Z平台')">下 载</button>
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<p class="font-weight-bold">说明:</p>
								<p>&nbsp;&nbsp;&nbsp;&nbsp;该版本包含MySQL、Tomcat、JDK等所有运行环境应用到的组件。并集成了控制台程序。方便对于Java环境配置过程不熟的同学，能够快速使用与体验Z平台。减少在环境配置方面浪费时间。</p>
							</div>
						</div>
					</div>

					<div class="col-md-12 pt-3">
						<div class="row">
							<div class="col-md-3">
								<div class="card mb-2">
									<div class="card-header">Z平台源码</div>
									<div class="card-body">
										<a href="https://gitee.com/zj1983/zz" class="btn btn-outline-secondary btn-block" target="_blank">
											<img alt="Z平台码云仓库" src="img/web/gitee.png" class="img-fluid">
										</a>
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<p class="font-weight-bold">说明:</p>
								<p>&nbsp;&nbsp;&nbsp;&nbsp;该版本需程序员自行编译，方便有开发能力的同学学习源码与修改源码。编译环境为JDK8 + MySQL8.0 + Tomcat9，所有环境均为64位版本。</p>
								<p class="font-weight-bold"><a href="https://gitee.com/zj1983/zz" class="clearA" target="_blank">https://gitee.com/zj1983/zz</a></p>
							</div>
						</div>
					</div>

					<div class="col-md-12 pt-3">
						<div class="row">
							<div class="col-md-3">
								<div class="card mb-2">
									<div class="card-header">文件服务组件</div>
									<div class="card-body">
										<button class="btn btn-outline-secondary btn-block" onclick="alertMessager('文件服务组件')">下 载</button>
									</div>
								</div>
							</div>
							<div class="col-md-9">
								<p class="font-weight-bold">说明:</p>
								<p>&nbsp;&nbsp;&nbsp;&nbsp;与Z平台配套的文件管理服务组件，管理所有上传到Z平台中的文件，与Z平台Web工程的文件分别管理。</p>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>

	<!-- 应用场景 -->
	<div class="row" id="application">
		<div class="col-12">
			<div class="jumbotron bg-white">
				<h3>应用场景</h3>
				<ul class="nav nav-tabs nav-justified pt-4" role="tablist">
					<li class="nav-item" role="presentation"><a class="nav-link text-dark active" data-toggle="tab" href="#jiangjie_erp">ERP管理软件开发</a></li>
					<li class="nav-item" role="presentation"><a class="nav-link text-dark" data-toggle="tab" href="#jiangjie_app">APP、小程序项目开发</a></li>
					<li class="nav-item" role="presentation"><a class="nav-link text-dark" data-toggle="tab" href="#jiangjie_web">WEB网站项目开发</a></li>
				</ul>
				<div class="tab-content mt-4">
					<div class="tab-pane fade show active" id="jiangjie_erp" role="tabpanel" aria-labelledby="home-tab">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;Z平台研发初衷就是为了适应ERP类软件快速开发而制作的。通过Z平台开发配置相关功能，能够快速实现ERP软件常用表单、报表等需求的快速实现，通过工作流完成业务流程的审核，通过任务管理完成定时执行程序的需求。大家可以很方便在的Z平台上快速构建出需要的业务系统。</p>
						<img src="img/web/jiangjie_erp.png" class="img-fluid">
					</div>
					<div class="tab-pane fade" id="jiangjie_app" role="tabpanel" aria-labelledby="profile-tab">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;APP、小程序等项目主要特点就是【前端客户端+后端接口服务】的集成项目，Z平台的主要特点是能够快速构建后端管理系统，在此基础上，我又对接口权限管理功能增加了一些控制能力，使Z平台在能够快速创建对外服务型接口之外。还对这些接口有一定管理能力。所以，大家可以应用Z平台这一特点，在APP、小程序等集成项目中，通过Z平台强大后端能力。快速为这种集成项目构建需要服务接口。</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;这类集成项目，不限于APP、小程序等，大家在做与其实第三方系统集成时也考虑应用。</p>
						<img src="img/web/jiangjie_app.png" class="img-fluid">
					</div>
					<div class="tab-pane fade" id="jiangjie_web" role="tabpanel" aria-labelledby="contact-tab">
						<p>&nbsp;&nbsp;&nbsp;&nbsp;Z平台的主要组成框架是【SpringMVC + Mybatis】，对这两大框架了解的同学一定知道，这是做WEB项目最常用的框架组合，所以，Z平台是完全可以做普通WEB项目的，并且结合Z平台本身提供的一些通用方法，大家在做普通WEB项目时，还是可以减少很大的一部分开发时间。</p>
						<img src="img/web/jiangjie_web.png" class="img-fluid">
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--foot-->
	<%@include file="foot.jsp"%>


	<!-- 信息提示框 -->
	<div id="zalert" class="modal fade" tabindex="-1" aria-labelledby="zalert" aria-hidden="true">
		<div id="zalert_head" class="modal-dialog modal-dialog-scrollable">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" id="zalert_title"></div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body" id="zalert_info">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-light" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		function alertMessager(msg){
			$("#zalert_title").html("下载说明");
			var htmlinfo = "<p>下载【"+msg+"】，请到Z平台交流QQ群,共享文件夹中下载</p>";
			htmlinfo = htmlinfo + '<p>Z平台交流QQ群：685040231</p>';
			htmlinfo = htmlinfo + '<p><a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=2id9ln2I4Tmn7i9FaltfanEb-nqPNo6P&jump_from=webapi"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="Z平台" title="Z平台"></a></p>';
			$("#zalert_info").html(htmlinfo);
			$("#zalert").modal("show");
		}
	</script>
</div>
</body>

</html>
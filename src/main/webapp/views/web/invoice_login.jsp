<%@ page language="java" contentType="text/html; charset=utf-8"
		 pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>登陆</title>
	<%@include file="/views/common/common.jsp"%>
	<style type="text/css">
		.text2 {
			color: #fff;
			font-size: 20px;
			font-family: 微软雅黑;
			font-weight: 500;
		}

		.text3 {
			color: #BAB9B8;
			font-size: 20px;
			font-family: 微软雅黑;
		}

		@media only screen and (min-width: 10px) and (max-width: 768px) {
			.test{
				width:100%;
			}
		}
		@media only screen and (min-width: 769px) and (max-width: 2000px) {
			.test{
				width:33.3%;
				margin:0 auto;
			}
		}

	</style>
</head>
<body>
<div class="container-fluid">
	<div class="row" style="padding-top:5%;padding-bottom:5%; background-color: #213477; text-align: center">
		<div class="col-md-12">
			<samp class="text2">发票提交</samp>
		</div>
	</div>
	<div id="MainDiv" class="row" style="margin-top:5%">
		<div class="col-md-12">
			<form action="qwer1" method="get" enctype="multipart/form-data">
				<div class="col-md-12 test" style="margin:0 auto">
					<samp style="color: #6d6c67; font-size: 20px; font-family: 微软雅黑;">用户名</samp>
				</div>
				<div class="col-md-12 pt-2 test">
					<input class="form-control form-control-lg" type="text" id="userid" name="user_id" value="${user_id}" placeholder="用户名"  maxlength="16" autofocus="autofocus" style="font-size: 20px; outline: none;margin:0 auto" />
				</div>

				<div class="col-md-12 pt-4 test" style=" margin:0 auto">
					<samp style="color: #6d6c67; font-size: 20px; font-family: 微软雅黑;">密 码</samp>
				</div>
				<div class="col-md-12 pt-2 test">
					<input class="form-control form-control-lg " type="password" id="passwordid" name="password" placeholder="用户身份证后6位"  style=" font-size: 20px; outline: none; margin:0 auto" />
				</div>
				<div class="col-md-12 pt-5 d-flex justify-content-center " >
					<button type="submit"  class="btn btn-secondary btn-lg  " style="background-color: #213477; color: white; width: 31.5%">登录</button>
				</div>
			</form>
		</div>
		<div class="col-md-12">
			<label style="color: red;">${msg}</label>
		</div>
		
	</div>
</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=utf-8"
		 pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>发票提交</title>

	<%@include file="/views/common/common.jsp"%>
	<script type="text/javascript">
	/* function fp_uploadFile(){
		var zid = newid();
		uploadFile("fp_bill_fp_file",zid);
	} */
	
	
	$(function() {
		var return_url_head = '${sp.fileserverurl}';
		var zid = newid();
		$("#uploadify").uploadifive({
			uploadScript:return_url_head+'/upload?filepath='+zid+'&return_url_head='+encodeURIComponent(return_url_head),
			auto: true,
			fileDataName:'fileData',
			fileObjName:'fileData',
			buttonText: '请选择要上传的文件',
			queueID: 'queue',
			height: 50,
			width : 220,
			multi: false,
			onFallback:function(){    
	            $("#alertInfo").text("您的浏览器不支持HTML5文件上传控件，请使用支持HTML5的浏览器后使用。如：Chrome")
	        },onUploadComplete : function(file, data){  
	        	var obj = JSON.parse(data);
	        	var user_id = $("#user_id").val();
	            if (obj.code == "SUCCESS" && isNotNull(obj.data)) {
	            	$.ajax({
	            		type : "get",
	            		url : "save_fp?filepath="+obj.data+"&user_id="+user_id,
	            		success : function(data) {
	            			if(data.code=='SUCCESS'){
	            				alert(data.data.amount);
	            			}else{
	            				alertErrorMessager(''+data.msg);
	            			}
	            		},
	            		error: function (data) {
	            			alertErrorMessager('ajax错误：'+JSON.stringify(data));
	            		}
	            	});
	            } else {
	            	 $("#alertInfo").text(data.msg + " | "+obj.data)
	            }
	        	
	        },onAddQueueItem:function(file){
	        	if(/.*[\u4e00-\u9fa5]+.*$/.test(file.name)) { 
	        		$("#alertInfo").text("文件名,不能包含中文!")
	        		$("#uploadify").uploadifive('cancel',$('.uploadifive-queue-item').first().data('file')); 
	        		$("#uploadify").uploadifive('clearQueue');
	        	}
	        }
		});
	});
	</script>
	<style type="text/css">
.fptj{
	position: relative;
}
.exit{
	position: absolute;
}

		@media only screen and (min-width: 10px) and (max-width: 768px) {
			.test{
				width:100%;
				margin:0 auto;
			}
			.exit{
				right:0%;
				bottom: -25%;
			}


		}
		@media only screen and (min-width: 769px) and (max-width: 2000px) {
			.test{
				width:33.3%;
				margin:0 auto;
			}
			.exit{
				right:1%;
				bottom: -100%;
			}
			.upimg{
				border: 1px solid grey;
			}

		}

	</style>
</head>
<body>
<div class="container-fluid" style="background-color: #213477;height: 120px;">
	<dir class="row">
		<div class="col-md-4">
		</div>
		<div class="col-md-4 d-flex justify-content-center ">
			<samp class="text-light">发票提交</samp>
		</div>
		<div class="col-md-4 d-flex justify-content-center">
			<a href="" style="color: white" class="exit">退出</a>
		</div>
	</dir>
</div>


<div class="container">
	<dir class="row">
		<div class="col-md-12 d-flex justify-content-center">
		<input type="hidden" id="user_id" value="${user_id}" />
			<!-- <button type="button" class="btn btn-outline-primary btn-lg" onclick="fp_uploadFile()">上传文件</button> -->
			<input id="uploadify" name="fileData" type="file"/>
		</div>
	</dir>
	
	<div class="row-fluid" >
		<span id="alertInfo"></span>
		<div id="queue"></div>
	</div>
</div>


</body>
</html>